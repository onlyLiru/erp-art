import React, {
    Component
} from 'react';
import {
    Select,
    Spin
} from 'antd';
import {
    fpost
} from './io.js';
const Option = Select.Option;

export default class SelectSchool extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fetching: false,
            // schoolAreas: store.session('userInfo') && store.session('userInfo').schoolAreas || [],
            schoolAreas: []
        }
        this._handleChange = this._handleChange.bind(this);
    }
    onSelect(value) {
        console.log('onSelect', value);
    }
    render() {
        const {
            fetching,
            schoolAreas
        } = this.state;
        let {
            initialValueData,
            disabled = false,
            setDefault
        } = this.props;
        // console.log(initialValueData);

        let width = this.props.width ? this.props.width : '200px';
        let mode = this.props.mode || '';

        if (!schoolAreas.length) {
            return <div>暂无数据</div>;
        }

        if (setDefault) {
            return (
                <Select
                    allowClear
                    size="large"
                    mode={mode}
                    disabled={disabled}
                    placeholder="请选择校区"
                    style={{ minWidth: '60px',width:width }}
                    onChange={ this._handleChange.bind(this) }
                    defaultValue= { schoolAreas[0].id }
                >
                    {
                        schoolAreas.map((d,i)=> {
                            return (<Option key={i} value={d.id}>{d.name}</Option>);
                        })
                    }
                </Select>
            );
        }

        return (
            <Select
                allowClear
                size="large"
                mode={mode}
                disabled={disabled}
                placeholder="请选择校区"
                style={{ minWidth: '60px',width:width }}
                onChange={ this._handleChange.bind(this) }
                defaultValue= { initialValueData }
            >
                {
                    schoolAreas.map((d,i)=> {
                        return (<Option key={i} value={d.id}>{d.name}</Option>);
                    })
                }
            </Select>
        );
    }
    componentDidMount() {
        this._getData();
    }
    _handleChange(v) {
        this.props.onSelect(v);
    }
    _getData() {
        let {
            url,
            initialValueData,
            setDefault
        } = this.props;
        let {
            schoolAreas,
        } = this.state;

        url = url ? url : '/api/system/schoolarea/findUserSchoolAreaInfo'; //url默认是当前登陆用户的校区，传入所有校区则获取所有校区数据

        fpost(url)
            .then((res) => res.json())
            .then((res) => {
                if (!res.success || !res.result) {
                    throw new Error(res.message || '系统错误');
                };
                return (res.result);
            })
            .then((result) => {
                // console.log(result);
                if (result.userSchoolArea) {
                    schoolAreas = result.userSchoolArea;
                } else if (result) {
                    schoolAreas = result;
                }

                this.setState({
                    schoolAreas: schoolAreas
                }, () => {
                    if ((schoolAreas.length == 1 && !initialValueData) || setDefault) {
                        this._handleChange(schoolAreas && schoolAreas[0] && schoolAreas[0].id || '');
                    }
                });
            })
            .catch((err) => {
                // console.log(err);
            });
    }
}