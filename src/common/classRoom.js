import React, {
    Component
} from 'react';
import {
    Select,
    Spin
} from 'antd';
import {
    fpost
} from './io.js';
const Option = Select.Option;

export default class ClassRooms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result: []
        }
        this._handleChange = this._handleChange.bind(this);
    }
    onSelect(value) {
        // console.log('onSelect', value);
    }
    render() {
        const {
            result
        } = this.state;
        let {
            initialValueData,
            disabled = false
        } = this.props;
        // console.log(initialValueData);

        let width = this.props.width ? this.props.width : '100%';
        let mode = this.props.mode || '';

        if (!result.length) {
            return <div>暂无数据</div>;
        }

        return (
            <Select
                allowClear
                size="large"
                mode={mode}
                disabled={disabled}
                placeholder="请选择教室"
                style={{ minWidth: '60px',width:width }}
                onChange={ this._handleChange.bind(this) }
                defaultValue= { initialValueData }
            >
                {
                    result.map((d,i)=> {
                        return (<Option key={i} value={d.id}>{d.name}</Option>);
                    })
                }
            </Select>
        );
    }
    componentDidMount() {
        this._getData();
    }
    _handleChange(v) {
        this.props.onSelect(v);
    }
    _getData() {
        let {
            url,
            initialValueData,
            setDefault
        } = this.props;
        let {
            result,
        } = this.state;

        url = url ? url : '/api/system/schoolarea/listClassRoom';

        fpost(url)
            .then((res) => res.json())
            .then((res) => {
                if (!res.success || !res.result) {
                    throw new Error(res.message || '系统错误');
                };
                return (res.result);
            })
            .then((result) => {

                this.setState({
                    result
                }, () => {
                    // if ((result.length == 1 && !initialValueData) || setDefault) {
                    //     this._handleChange(result[0].id);
                    // }
                });
            })
            .catch((err) => {
                // console.log(err);
            });
    }
}