import React, {
	Component
} from 'react';
import {
	Form
} from 'antd';

import {
	default as TopForm
} from './form.js';
import {
	default as EditClassTimes
} from './editClassTimes.js';

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			
		}
	}
	render() {
		return (<Form>
			<TopForm 
				form ={this.props.form}
				schoolAreaName ={this.props.schoolAreaName}
				defaultTimes={ this.props.defaultTimes }
			/>
			<EditClassTimes 
				defaultTimes={ this.props.defaultTimes }
				getData={this.props.getTimes.bind(this)} 
				form ={this.props.form}
			/>
		</Form>);
	}
	componentDidMount() {

	}
}

const MyForm = Form.create()(Main);

export default MyForm;