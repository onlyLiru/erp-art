import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Select,
	InputNumber,
	Icon,
	Form
} from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

let uuId = 0;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			defaultTimes:this.props.defaultTimes,
			defaultKey:[uuId],
			ruleList: [{
				
			}]
		}
	}
	render() {
		let {
			ruleList
		} = this.state;
		const {
			getFieldDecorator,
			getFieldValue
		} = this.props.form;
		let {
			defaultTimes,
			defaultKey
		} = this.state;
		// console.log(defaultTimes);

		if(defaultTimes && defaultTimes.length && store.session.get('ifFirstEditTimes') == '1') {
			defaultKey=[];
			store.session.set('ifFirstEditTimes', '2');
			defaultTimes.map((d,i)=> {
				defaultKey.push(i);
				uuId=i;
			});
			this.setState({
				defaultKey
			});
		}

		getFieldDecorator('keys', {
			initialValue: defaultKey
		});
		const keys = getFieldValue('keys');

		return (<div className="f-ml5">
			{ 
				keys.map((k,index)=> {
					return(<div className="f-clear" style={{marginBottom:'10px'}} key={k}>
			            <FormItem className="f-left">
			            	{
			            		getFieldDecorator(`weekday-${k}`, {
			            			rules: [{  
			            				required: true,
			            				message: '请选择星期' 
			            			}],
			            			initialValue:defaultTimes && defaultTimes[k] && (defaultTimes[k].weekday).split(',') || []
			            		})(
			            			<Select
			    						size="large"
			    						mode="multiple"
			    						style={{ width: 200, marginRight:'20px', float:'left'}} 
			    						onChange={
			    							(v)=> {
			    								
			    							}
			    						}
			    						placeholder="请选择星期"
			    					>
			    				    	<Option value="1">周一</Option>
			    				    	<Option value="2">周二</Option>
			    				    	<Option value="3">周三</Option>
			    				    	<Option value="4">周四</Option>
			    				    	<Option value="5">周五</Option>
			    				    	<Option value="6">周六</Option>
			    				    	<Option value="7">周日</Option>
			    				    </Select>
			            		)
			            	}
			            </FormItem>
	                    <FormItem className="f-left">
	                    	{
	                    		getFieldDecorator(`startHour-${k}`, {
	                    			rules: [{  
	                    				required: true,
	                    				message: '请输入' 
	                    			}],
	                    			initialValue:defaultTimes && defaultTimes[k] && (defaultTimes[k].startTime).split(':')[0] || ''
	                    		})(
	                    			<InputNumber 
								    	size="large" 
								    	min={0} 
								    	max={24} 
								    	style={{ float:'left'}} 
								    	formatter={
								    		(value) => {
								    			if(value=='0') {
								    				value = '00';
								    			}else if(value.length==1) {
								    				value = '0' + value;
								    			}
								    			return value
								    		}
								    	}
							    	/>
	                    		)
	                    	}
	                    </FormItem>
				    	<span style={{float:'left',margin:'0 10px',lineHeight:'36px'}}>:</span>
	                    <FormItem className="f-left">
	                    	{
	                    		getFieldDecorator(`startMinutes-${k}`, {
	                    			rules: [{  
	                    				required: true,
	                    				message: '请输入' 
	                    			}],
	                    			initialValue:defaultTimes && defaultTimes[k] && (defaultTimes[k].startTime).split(':')[1] || ''
	                    		})(
	                    			<InputNumber 
								    	size="large" 
								    	min={0} 
								    	max={60} 
								    	style={{ float:'left'}} 
								    	formatter={
								    		(value) => {
								    			if(value=='0') {
								    				value = '00';
								    			}else if(value.length==1) {
								    				value = '0' + value;
								    			}
								    			return value
								    		}
								    	}
							    	/>
	                    		)
	                    	}
	                    </FormItem>
				    	<span style={{float:'left',margin:'0 10px',lineHeight:'36px'}}>至</span>
	                    <FormItem className="f-left">
	                    	{
	                    		getFieldDecorator(`endHour-${k}`, {
	                    			rules: [{  
	                    				required: true,
	                    				message: '请输入' 
	                    			}],
	                    			initialValue:defaultTimes && defaultTimes[k] && (defaultTimes[k].endTime).split(':')[0] || ''
	                    		})(
	                    			<InputNumber 
								    	size="large" 
								    	min={0} 
								    	max={24} 
								    	style={{ float:'left'}}
								    	formatter={
								    		(value) => {
								    			if(value=='0') {
								    				value = '00';
								    			}else if(value.length==1) {
								    				value = '0' + value;
								    			}
								    			return value
								    		}
								    	}
							    	/>
	                    		)
	                    	}
	                    </FormItem>
				    	<span style={{float:'left',margin:'0 10px',lineHeight:'36px'}}>:</span>
	                    <FormItem className="f-left">
	                    	{
	                    		getFieldDecorator(`endMinutes-${k}`, {
	                    			rules: [{  
	                    				required: true,
	                    				message: '请输入' 
	                    			}],
	                    			initialValue:defaultTimes && defaultTimes[k] && (defaultTimes[k].endTime).split(':')[1] || ''
	                    		})(
	                    			<InputNumber 
								    	size="large" 
								    	min={0} 
								    	max={60} 
								    	style={{ float:'left'}}
								    	formatter={
								    		(value) => {
								    			if(value=='0') {
								    				value = '00';
								    			}else if(value.length==1) {
								    				value = '0' + value;
								    			}
								    			return value
								    		}
								    	}
							    	/>
	                    		)
	                    	}
	                    </FormItem>
				    	{
				    		keys.length > 1 ?
						    	<Icon 
						    		type="minus-square-o"
						    		style={{ fontSize: 38,float:'left', color: '#FC3F40',margin:'0 -8px 0 10px' }}
					    			onClick={
					    				()=> {
					    					this._rmItem(k);
					    				}
					    			}
					    		/>
					    	: null
				    	}
				    	<Icon 
				    		type="plus-square-o" 
				    		style={{ fontSize: 38,float:'left', color: '#64C4FC',marginLeft:'10px' }}
				    		onClick={this._add.bind(this)}
			    		/>
		    		</div>);
				})
			}
			
		</div>);
	}
	componentDidMount() {
		
	}
	componentWillUnmount() {

	}
	_add() {
		uuId++;
		const {
			form
		} = this.props;
		// can use data-binding to get
		const keys = form.getFieldValue('keys');
		const nextKeys = keys.concat(uuId);
		// can use data-binding to set
		// important! notify form to detect changes
		this.setState({
			defaultKey:nextKeys
		});
		form.setFieldsValue({
			keys: nextKeys,
		});
	}
	_rmItem(k) {
		const {
			form
		} = this.props;
		// can use data-binding to get
		const keys = form.getFieldValue('keys');
		// We need at least one passenger
		if (keys.length === 1) {
			return;
		}

		// can use data-binding to set
		form.setFieldsValue({
			keys: keys.filter(key => key !== k),
		});
		this.setState({
			defaultKey:keys.filter(key => key !== k)
		});
	}
}