import React, {
	Component
} from 'react';
import {
	Input,
	Button,
	Icon,
	Col,
	Row,
	Form,
	DatePicker,
	Select
} from 'antd';
import {
	default as SelectPerson
} from '../selectPerson.js';
import SelectCourses from '../selectCourses.js';
import SelectSchool from '../selectSchool.js';
import ClassRooms from '../classRoom.js';
import {
	undefinedToEmpty
} from '../g.js';
import {
	status
} from '../staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isShow: true
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			isShow
		} = this.state;

		let {
			defaultTimes
		} = this.props;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		if (!isShow) {
			return null;
		}

		return (
			<div>
				<Row gutter={ 40 }>
    				<Col span={12}>
						<FormItem
							label="校区"
							{...formItemLayout}
						>
							{this.props.schoolAreaName}
						</FormItem>
					</Col>
				</Row>
				<Row gutter={ 40 }>
    				<Col span={ 12 }>
						<FormItem
							label="上课老师"
							{...formItemLayout}
						>
							{
								getFieldDecorator('teacherId', {
									rules: [{  
										required:true,
										message: '请选择上课老师' 
									}],
									initialValue:(defaultTimes && defaultTimes[0].teacherId) || '' 
								})(
									<SelectPerson
										url='/api/hr/staff/listStaffByCondForDropDown'
										data={
											{
												type:6
											}
										}
										onSelect={ 
											(v)=> {
												v = v ? v : null;
												this.props.form.setFieldsValue({
													teacherId: v
												});
											} 
										}
										initialValueData={ (defaultTimes && defaultTimes[0].teacherId) || '' }
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={ 12 }>
						<FormItem
							label="教室"
							{...formItemLayout}
						>
							{
								getFieldDecorator('roomId', {
									rules: [{ 
										required:true, 
										message: '请选择教室' 
									}],
									initialValue:defaultTimes && defaultTimes[0].roomId || null
								})(
									<ClassRooms
										onSelect={ 
											(v)=> {
												v = v ? v : null;
												this.props.form.setFieldsValue({
													roomId: v
												});
											} 
										}
										initialValueData={ (defaultTimes && defaultTimes[0].roomId) || '' }
									/>
								)
							}
						</FormItem>
					</Col>
    	        </Row>
	      	</div>);
	}
	componentDidMount() {

	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			isShow: false
		}, () => {
			this.setState({
				isShow: true
			}, () => {
				this._search();
			})
		});
	}
}