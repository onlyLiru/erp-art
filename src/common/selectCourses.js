import React, {
    Component
} from 'react';
import {
    Select,
    Spin
} from 'antd';
import {
    fpost
} from './io.js';
const Option = Select.Option;

export default class SelectCourses extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fetching: false,
            result: []
        }
    }
    render() {
        const {
            fetching,
            result
        } = this.state;

        let {

            mode = "", //如果是多选择mode的值要传入"multiple"
                initialValueData = [],
                width = '100%'
        } = this.props;

        if (!result.length) {
            return <div>暂无数据</div>;
        }

        return (
            <Select
                size="large"
                placeholder="请选择课程"
                mode={mode}
                allowClear
                optionFilterProp="children"
                style={{ width: width }}
                onChange={ this._handleChange.bind(this) }
                defaultValue= { initialValueData }
            >
                {
                    result.map((d,i)=> {
                        return (<Option key={i} value={d.id}>{d.name}</Option>);
                    })
                }
            </Select>
        );
    }
    componentDidMount() {
        this._getData();
    }
    _handleChange(v) {
        let {
            result
        } = this.state;
        let myName;
        let curData;

        result.forEach((d, i) => {
            let {
                id,
                name
            } = d;

            if (v == id) {
                myName = name;
                curData= d;
            }
        });

        // console.log(v);
        // console.log(myName);
        this.props.onSelect(v, myName,curData);
    }
    _getData() {
        let {
            courseTypeId = ''
        } = this.props;

        this.setState({
            fetching: true
        });

        fpost('/api/system/course/listCourse', {
                subjectId: courseTypeId
            })
            .then((res) => res.json())
            .then((res) => {
                if (!res.success || !res.result) {
                    this.setState({
                        fetching: false
                    });
                    // message.error(res.message || '系统错误');
                    throw new Error(res.message || '系统错误');
                };
                return (res.result);
            })
            .then((result) => {
                this.setState({
                    fetching: false,
                    result
                });
            })
            .catch((err) => {
                this.setState({
                    fetching: false
                });
                console.log(err);
            });
    }
}