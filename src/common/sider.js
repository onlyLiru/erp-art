import React, {
    Component
} from 'react';
import store from 'store2';
import {
    Menu,
    Spin,
    Icon
} from 'antd';
import $ from 'jquery';
const MenuItemGroup = Menu.ItemGroup;
let pathname = window.location.pathname;

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            /*头部主菜单的选中项的key*/
            tMenuKey: store.session('tMenu') && store.session('tMenu').selectedKeys,
            /*左边菜单选中项的key*/
            selectedKeys: store.session('lMenu.selectedKeys') || "",

            rules: null,
        }
    }
    render() {
        let {
            selectedKeys,
            rules,
        } = this.state;

        if (!rules) {
            return (<div className="f-align-center f-pd5"><Spin /></div>);
        }
        return (<div>
            <Menu  
                theme="light"
                selectedKeys={[selectedKeys]}
                onClick={  this._selectMenu.bind(this)  }
                mode="inline"
            >
                {
                    rules.map((d,i)=>  {
                        if(d.children && d.children.length)  {
                            return  (<MenuItemGroup
                                    key={  d.key  }
                                    title={<span>
                                        <Icon type="caret-right" /><span>{d.title}</span>
                                    </span>}>
                                    {
                                        d.children.map((dd,ii)=>  {
                                            return <Menu.Item  key={  dd.key  }>
                                                <a data-key={dd.key} href={`${dd.path}`} className='f-selected-side'>{ dd.title }</a>
                                            </Menu.Item>
                                        })  
                                    }
                            </MenuItemGroup>)
                        }else  {
                            return (<Menu.Item key={ d.key }>
                                <a data-key={d.key}  href={`${d.path}`}>{ d.title }</a>
                            </Menu.Item>);
                        }
                    })
                }
            </Menu>
        </div>);
    }
    componentDidMount() {
        this._getRules();
    }
    _selectMenu({
        item,
        key,
        keyPath
    }) {
        this.setState({
            selectedKeys: key
        });
        store.session('lMenu.selectedKeys', key);
    }
    _getSelected() {
        for (let i = 0; i < $('.f-selected-side').length; i++) {
            if ($('.f-selected-side').eq(i).attr('href') == pathname) {
                $('.f-selected-side').eq(i).parents('li').eq(0).css('background', '#f1f8ef');
                $('.f-selected-side').eq(i).css('color', '#4aa236');
                $('.f-selected-side').eq(i).parents('li').eq(0).css('border-right', ' 3px solid #4aa236');
            } else {
                $('.f-selected-side').eq(i).parents('li').eq(0).css('background', 'white');
                $('.f-selected-side').eq(i).css('color', 'rgba(0, 0, 0, 0.65)');
                $('.f-selected-side').eq(i).parents('li').eq(0).css('border-right', 'none')
            }
        }
    }
    _getRules() {
        let menuTree = store.get('userInfo') && store.get('userInfo').menuTree;
        let tMenuKey = this.state.tMenuKey;
        let rules = null;
        // console.log(menuTree);
        if (menuTree && menuTree.length) {
            menuTree.forEach((d, i) => {
                let key = d.key;
                if (tMenuKey == key) {
                    rules = d.children || [];
                }
            });
            // console.log('rules', rules);
            this.setState({
                rules: rules
            },this._getSelected);
        }
    }
}