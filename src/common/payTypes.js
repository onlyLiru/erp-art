import React, {
	Component
} from 'react';
import {
	Input,
	message
} from 'antd';

export default class PayTypes extends Component {
	constructor(props) {
		super(props);
		this.state = {
			cash: '',
			aliPay: '',
			weixinPay: '',
			bankPay: '',
			transfer: '',
			credit: '',
			checkPay: '',
			trades: [],
			total: 0,
		}
	}
	render() {

		return (<div>
				<span className="f-mr5 f-mb2 f-inline-block">
					<Input
						addonBefore="现金" 
						addonAfter="元"
						style={{ width:'200px' }}
						value={ this.state.cash }
						onChange= {
							(e)=> {
								this._handleChange(e,'cash','1');
							}
						}
						size="large"
					/>
				</span>
				<span className="f-mr5 f-mb2 f-inline-block">
					<Input 
						addonBefore="POS微信" 
						addonAfter="元"
						style={{ width:'200px' }} 
						size="large" 
						value={this.state.weixinPay}
						onChange= {
							(e)=> {
								this._handleChange(e,'weixinPay','2');
							}
						}
					/>
				</span>
				<span className="f-mr5 f-mb2 f-inline-block">
					<Input
						addonBefore="POS支付宝" 
						addonAfter="元"
						style={{ width:'200px' }}
						value={ this.state.aliPay }
						onChange= {
							(e)=> {
								this._handleChange(e,'aliPay','3');
							}
						}
						size="large"
					/>
				</span>
				<span className="f-mr5 f-mb2 f-inline-block">
					<Input 
						addonBefore="刷借记卡" 
						addonAfter="元"
						style={{ width:'200px' }} 
						size="large" 
						value={this.state.bankPay}
						onChange= {
							(e)=> {
								this._handleChange(e,'bankPay','4');
							}
						}
					/>
				</span>
				<span className="f-mr5 f-mb2 f-inline-block">
					<Input 
						addonBefore="刷信用卡" 
						addonAfter="元"
						style={{ width:'200px' }} 
						size="large" 
						value={this.state.credit}
						onChange= {
							(e)=> {
								this._handleChange(e,'credit','5');
							}
						}
					/>
				</span>
				<span className="f-mr5 f-mb2 f-inline-block">
					<Input 
						addonBefore="线上支付宝" 
						addonAfter="元"
						style={{ width:'200px' }} 
						size="large"
						value={this.state.checkPay}
						onChange= {
							(e)=> {
								this._handleChange(e,'checkPay','6');
							}
						}
					/>
				</span>
				<span className="f-mr5 f-mb2 f-inline-block">
					<Input 
						addonBefore="银行转账" 
						addonAfter="元"
						style={{ width:'200px' }} 
						size="large"
						value={this.state.transfer}
						onChange= {
							(e)=> {
								this._handleChange(e,'transfer','7');
							}
						}
					/>
				</span>
		</div>);
	}
	_handleChange(e, type, index) {
		let total = 0;
		let {
			value
		} = e.target;
		let {
			onInput
		} = this.props;
		let {
			trades
		} = this.state;

		if (isNaN(value) || value < 0) {
			message.error('输入有误');
			return;
		}

		trades = trades.filter((d, i) => {
			return (d.tradeType != index && d.tradeAmount.length);
		});

		if (value.length) {
			trades = [
				...trades, {
					tradeType: index,
					tradeAmount: value
				}
			]
		}

		if (trades && trades.length) {
			trades.forEach((d, i) => {
				total += Number(d.tradeAmount);
			});
		}

		this.setState({
			[type]: value,
			trades,
			total
		}, () => {
			// console.log('支付信息:', this.state);
			onInput(this.state);
		});
	}
}