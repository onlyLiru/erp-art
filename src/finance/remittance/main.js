import React, {
	Component
} from 'react';
import store from 'store2';
import querystring from 'querystring';
import {
	Breadcrumb,
	message,
	Modal
} from 'antd';
import {
	default as MyList
} from './list.js';
import {
	default as SearchForm
} from './searchform.js';
import {
	fpost,
	host
} from '../../common/io.js';
import {
	numberFormate
} from '../../common/g.js';
import '../main.less';

const confirm = Modal.confirm;
let sessionId = store.get('sessionId');

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			result: null,
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {}
		}
	}
	render() {
		let {
			result,
			loading,
			pagination
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>财务管理</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">财务打款</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<SearchForm
				ref={ form => this.searchForm = form }
				onSearch= { this._search.bind(this) }
			/>

			<MyList 
				data={result}
		        loading={loading}
		        onChange={this._getList.bind(this)}
		        remit={this._remit.bind(this)}
		        pagination={pagination}
		        exportFile = { this._exportFile.bind(this) }
			/>

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_search(values) { //搜索
		// console.log(values);
		this.setState({
			searchParma: {
				...this.state.searchParma,
				...values
			},
			currentPage: 1,
			pagination: {
				...this.state.pagination,
				current: 1,
			}
		}, () => {
			this._getList();
		});
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			searchParma
		} = this.state;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		searchParma.pageSize = pageSize;
		searchParma.currentPage = currentPage;

		console.log('pager', pager);

		fpost('/api/reception/pageFinancePay', searchParma)
			.then((res) => {
				console.log(res);
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result || !res.result.records) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records;
				data.forEach((d) => {
					/*金额千分位*/
					d.totalArrearAmount = numberFormate(d.totalArrearAmount)
					d.balanceAmount = numberFormate(d.balanceAmount)
					d.key = d.id
				});

				let total = Number(result.total);

				this.setState({
					result: data,
					pagination: {
						...pagination,
						total: total,
						current: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				}, () => {
					// console.log(this.state)
				});
			})
			.catch((err) => {
				console.log(err);

				this.setState({
					loading: false
				});
			});
	}
	_remit(row) {
		let {
			id
		} = row;
		let self = this;

		confirm({
		    title: '确定打款吗?',
		    onOk() {
				fpost('/api/reception/finishFinancePay', {
						financePayId:id
					})
					.then(res => res.json())
					.then((res) => {
						if (!res.success) {
							message.error(res.message || '系统错误');
							throw new Error(res.message || '系统错误');
						};
						return (res);
					})
					.then((res) => {
						self._getList();
					})
					.catch((err) => {
						console.log(err);
					});
		    },
		    onCancel() {
		      
		    },
		  });

	}
	_exportFile() {
		let {
			searchParma
		} = this.state;
		searchParma = querystring.stringify(searchParma);
		let url = `${host}/api/reception/student/exportStudentExcel?x-auth-token=${sessionId}&${searchParma}`;
		window.location.href = url;
	}
}