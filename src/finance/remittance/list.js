import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../common/io.js';


export default class Main extends Component {
  constructor(props){
    super(props);
  }
  render() {

    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile
    } = this.props;

    const columns = [{
      title: '学员姓名（学号）',
      dataIndex: 'studentName',
      width: '150px',
      className: 'f-align-center',
      fixed: 'left',
      render: (value, row, index) => {
        let {
          studentName,
          stuNo
        } = row;
        return <a href={`/front/studenthome?studentId=${row.studentId}`}
          className="f-line1 f-over-hide"
          style={{
            width:'150px'
          }}
          title={`${studentName}(${stuNo})`}
        >
          { studentName }({stuNo})
        </a>
      }
    }, {
      title: '家长手机号码',
      dataIndex: 'phone',
      className: 'f-align-center',
      width: '150px'
    }, {
      title: '校区',
      dataIndex: 'schoolAreaName',
      width: '200px'
    }, {
      title: '打款方式',
      dataIndex: 'tradeTypeStr',
      width: '200px'
    }, {
      title: '打款金额',
      dataIndex: 'amount',
      className: 'f-align-center',
      width: '150px'
    }, {
      title: '收款账号',
      dataIndex: 'accountInfo',
      className: 'f-align-center',
      width: '250px'
    }, {
      title: '打款状态',
      dataIndex: 'isPaidStr',
      className: 'f-align-center',
      width: '100px'
    }, {
      title: '打款时间',
      dataIndex: 'paymentTime',
      className: 'f-align-center',
      width: '200px'
    }, {
      title: '申请人',
      dataIndex: 'applyUserName',
      width: '100px'
    }, {
      title: '申请时间',
      dataIndex: 'applyTime',
      width: '200px'
    }, {
      title: '操作',
      dataIndex: 'des',
      fixed: 'right',
      width: '100px',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          approveId,
          type,
          financePayId,
          isPaid
        } = row;
        return (<div>
          <Dropdown 
            overlay={
              <Menu>
                <Menu.Item key="1">
                  <a href={`/front/check/detail?id=${approveId}&stage=2&type=${type}`}>查看详情</a>
                </Menu.Item>
                <Menu.Item key="3">
                  {
                    isPaid == '1' ?
                      <span className="f-pale">已打款</span>
                    :
                      <span onClick={()=> this.props.remit(row)}>打款</span>
                  }
                </Menu.Item>
              </Menu>
            }>
            <a>
              操作<Icon type="down" />
            </a>
          </Dropdown>
        </div>);
      }
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">财务打款列表</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 1800}}
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
				  />
			</div>
    );
  }
}

