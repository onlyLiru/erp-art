import React, {
  Component
} from 'react';
import store from 'store2';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message,
  Progress,
  Modal,
  Popover
} from 'antd';
import {
  fpost,
  fpostArray
} from '../../common/io.js';
import {
  default as MangeClassTimes
} from '../../common/manageClassTimes/main.js'

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: false,
      saveing: false,
      defaultTimes:null,//编辑时候承载获取的默认数据
      rulesList: []
    }
  }
  render() {
    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile,
      onDelete,
      onSelectRow
    } = this.props;

    let {
      visibleModal,
      saveing
    } = this.state;

    const columns = [{
      title: '班级名称',
      dataIndex: 'name',
      width: '150px',
      render: function(value, row, index) {
        let {
          id,
          name
        } = row;
        return (<div title={name} className="f-line1" style={{width:'150px'}}>
          <a href={`/teach/classInfo?classId=${id}`}>{name}</a>
        </div>);
      }
    }, {
      title: '校区名称',
      dataIndex: 'schoolAreaName',
      width: '150px',
      render:function(value, row, index) {
        return(<div title={value} className="f-line1" style={{width:'150px'}}>
            {value}
          </div>);
      }
    }, {
      title: '班级状态',
      dataIndex: 'stateStr',
      width: '200px'
    }, {
      title: '课程',
      dataIndex: 'courseName',
      width: '100px',
      render:function(value, row, index) {
        return(<div title={value} className="f-line1" style={{width:'100px'}}>
            {value}
          </div>);
      }
    }, {
      title: '上课时段',
      dataIndex: 'ruleStrList',
      width: '200px',
      render:function(value, row, index) {
        return(<div className="f-line1" style={{width:'200px'}}>
              <Popover 
                placement="topLeft"
                content={
                  <div>
                    {
                      (value || []).map((d,i)=> {
                         return <p key={i}>{d}</p>
                      })
                    }
                  </div>
                }>
                {value}
              </Popover>
          </div>);
      }
    }, {
      title: '学费(¥)',
      dataIndex: 'salePrice',
      width: '100px'
    }, {
      title: '上课老师',
      dataIndex: 'teacherName',
      width: '150px'
    }, {
      title: '教室',
      dataIndex: 'roomName',
      width: '150px'
    }, {
      title: '在读人数',
      dataIndex: 'currentStudents',
      className: 'f-align-center',
      width: '100px',
      render: function(value, row, index) {
        let {
          currentStudents,
          rated
        } = row;

        currentStudents = parseInt(currentStudents)
        rated = parseInt(rated)

        let percent = currentStudents <= 0 ? 0 : (Math.round(currentStudents / rated * 10000) / 100);

        return (<div>
          <Progress 
            type="circle" 
            width={60}
            percent={percent} 
            format={percent => `${currentStudents}/${rated}`}
          />
        </div>);
      }
    }, {
      title: '操作',
      dataIndex: 'des',
      fixed: 'right',
      width: '100px',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          id
        } = row;

        return (<div>
          <Dropdown 
            overlay={
              <Menu>
                <Menu.Item key="1">
                  <a 
                    href="javascript:" 
                    onClick={
                      ()=> {
                        this.setState({
                          classId:id,
                          schoolAreaName:row.schoolAreaName,
                          state:row.state
                        },this._getDefaultTimes);
                      }
                    }
                  >排课</a>
                </Menu.Item> 
                <Menu.Item key="2">
                  <a href={`/teach/createClass?id=${id}`}>编辑</a>
                </Menu.Item>
                <Menu.Item key="4">
                  <a onClick={
                    ()=> {
                      onDelete(id);
                    }
                  }>删除</a>
                </Menu.Item>
              </Menu>
            }>
            <a href="#">
              操作<Icon type="down" />
            </a>
          </Dropdown>
        </div>);
      }
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">班级列表</span>
          <a 
            className="f-btn-green f-right"
            target="_blank"
            onClick={exportFile}
          >
            <Icon className="f-fz7 f-vertical-middle f-bold" />班级学员批量导出
          </a>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            rowSelection={
              {
                onSelect:onSelectRow,
                onSelectAll:onSelectRow
              }
            }
            scroll={{ x: 1462}}
            pagination={ pagination }
            loading={loading}
            bordered
            onSelect={onChange}
				  />

          {
            visibleModal ?
              <Modal title={`排课`}
                visible={ visibleModal }
                onOk={ this._onOk.bind(this) }
                confirmLoading={ saveing }
                destroyOnClose={true}
                onCancel={ this._onCancel.bind(this) }
                width="800px"
              >
                <MangeClassTimes
                  schoolAreaName={ this.state.schoolAreaName }
                  getTimes={ this._getTimes.bind(this) }
                  defaultTimes={ this.state.defaultTimes }
                  ref={ form => this.form=form }
                />
              </Modal>
            : null
          }
			</div>
    );
  }
  _getTimes(rulesList) {
    console.log(rulesList);
    this.setState({
      rulesList
    });
  }
  _onOk() {
    this.form.validateFields((err, values) => {
      if (!err) {
        this._initParam(values);
      }
    });
  }
  _initParam(param) {
    console.log(param);
    let data;
    let ruleList=[];
    let {
      keys,
      teacherId,
      roomId
    } = param;
    data={
      classId:this.state.classId,
      teacherId,
      roomId,
      ruleList
    }
    keys.map((k,index)=> {
      let weekday = param[`weekday-${k}`];
      weekday = weekday.join();
      let startHour = param[`startHour-${k}`];
      startHour=(Array(2).join('0') + startHour).slice(-2);
      let startMinutes = param[`startMinutes-${k}`];
      startMinutes=(Array(2).join('0') + startMinutes).slice(-2);
      let startTime = startHour + ':' + startMinutes;

      let endHour = param[`endHour-${k}`];
      endHour=(Array(2).join('0') + endHour).slice(-2);
      let endMinutes = param[`endMinutes-${k}`];
      endMinutes=(Array(2).join('0') + endMinutes).slice(-2);
      let endTime = endHour  + ':' +  endMinutes;
      ruleList.push({
        weekday:weekday,
        startTime:startTime,
        endTime:endTime
      });
    });
    this._save(data);
  }
  _onCancel() {
    store.session.set('ifFirstEditTimes', '2');
    this.setState({
      visibleModal: false
    });
  }
  _getDefaultTimes() {
    let {
      classId,
      state
    } = this.state;
    if(state=='0') {//未排课
      this.setState({
        visibleModal:true,
        defaultTimes:null
      });
      return;
    };
    store.session.set('ifFirstEditTimes', '1');
    fpost('/api/educational/listTimesRuleByClassId', {
      classId
    })
      .then(res => res.json())
      .then((res) => {
        if (!res.success) {
          message.error(res.message || '系统错误');
          throw new Error(res.message || '系统错误');
        };
        return (res);
      })
      .then((res) => {
          // console.log(res.result);
          this.setState({
            defaultTimes:res.result,
            visibleModal:true
          });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  _save(data) {
    console.log(data);
    this.setState({
      saveing: true
    });

    let {
      onSaveOk
    } = this.props;

    fpostArray('/api/educational/modifyClassTimes', data)
      .then(res => res.json())
      .then((res) => {
        if (!res.success) {
          this.setState({
            saveing: false
          });
          message.error(res.message || '系统错误');
          throw new Error(res.message || '系统错误');
        };
        return (res);
      })
      .then((res) => {
        message.success(res.message);
        this.setState({
          saveing: false,
          visibleModal: false
        }, this.props.getData);
      })
      .catch((err) => {
        this.setState({
          saveing: false
        });
        console.log(err);
      });
  }
}