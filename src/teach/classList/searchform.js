import React, {
	Component
} from 'react';
import {
	Input,
	Button,
	Icon,
	Col,
	Row,
	Form,
	DatePicker,
	Select
} from 'antd';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import SelectCourses from '../../common/selectCourses.js';
import SelectSchool from '../../common/selectSchool.js';
import ClassRooms from '../../common/classRoom.js';
import {
	undefinedToEmpty
} from '../../common/g.js';
import {
	status
} from '../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isShow: true
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			isShow
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		if (!isShow) {
			return null;
		}

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form>
				<Row gutter={ 40 }>
    				<Col span={8}>
    					<FormItem
							label="班级名称"
							{...formItemLayout}
						>
							{
								getFieldDecorator('className', { 
									rules: [{
										required: false, 
										message: '请输入班级名称' 
									}] 
								})
								(
									<Input placeholder="请输入班级名称" />
								)
							}
						</FormItem>
    				</Col>
    				<Col span={ 8 }>
						<FormItem
							label="课程名称"
							{...formItemLayout}
						>
							{
								getFieldDecorator('courseId', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<SelectCourses 
										onSelect= {
											(v)=> {
												this.props.form.setFieldsValue({
													courseId:v
												});
											}
										}
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={8}>
						<FormItem
							label="校区"
							{...formItemLayout}
						>
							{
								getFieldDecorator('schoolAreaId', { 
									rules: [{
										message: '请选择校区' 
									}],
									initialValue:''
								})
								(
									<SelectSchool 
										onSelect={
											(v) => {
												this.props.form.setFieldsValue({
													schoolAreaId:v
												});
											}
										}
										width='100%' 
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={ 8 }>
						<FormItem
							label="上课老师"
							{...formItemLayout}
						>
							{
								getFieldDecorator('teacherId', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<SelectPerson
										url='/api/hr/staff/listStaffByCondForDropDown'
										data={
											{
												type:6
											}
										}
										onSelect={ 
											(v)=> {
												v = v ? v : null;
												this.props.form.setFieldsValue({
													teacherId: v
												});
											} 
										}
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={ 8 }>
						<FormItem
							label="教室"
							{...formItemLayout}
						>
							{
								getFieldDecorator('roomId', {
									rules: [{  
										message: '请选择教室' 
									}]
								})(
									<ClassRooms
										onSelect={ 
											(v)=> {
												v = v ? v : null;
												this.props.form.setFieldsValue({
													roomId: v
												});
											} 
										}
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={ 8 }>
						<FormItem
							label="班级状态"
							{...formItemLayout}
						>
							{
								getFieldDecorator('state', {
									rules: [{  
										message: '请选择班级状态' 
									}]
								})(
									<Select
						                allowClear
						                size="large"
						                placeholder="请选择班级状态"
						            >
						                {
						                    status.map((d,i)=> {
						                        return (<Option key={i} value={d.label}>{d.value}</Option>);
						                    })
						                }
						            </Select>
								)
							}
						</FormItem>
					</Col>
    	        </Row>
    	        <div className="f-align-center">
    	        	<Button 
    	        		size="large" 
    	        		className="f-mr4" 
    	        		onClick={ this._reset.bind(this) }
    	        	>重置</Button>
    	        	<Button 
    	        		type="primary"
    	        		size="large" 
    	        		icon="search" 
    	        		onClick={ this._search.bind(this) }
    	        	>查询</Button>
    	        </div>
	      	</Form>
		</header>);
	}
	componentDidMount() {

	}
	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			isShow: false
		}, () => {
			this.setState({
				isShow: true
			}, () => {
				this._search();
			})
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;