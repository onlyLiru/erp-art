import React, {
	Component
} from 'react';
import {
	Input,
	Button,
	Col,
	Row,
	Modal,
	Form,
	DatePicker,
	Select,
	message,
	Radio,
	Breadcrumb
} from 'antd';

import SelectSchool from '../../common/selectSchool.js';
import SelectCourses from '../../common/selectCourses.js';
import {
	fpost
} from '../../common/io.js';
import {
	getUnicodeParam,
	undefinedToEmpty
} from '../../common/g.js';

const FormItem = Form.Item;
const Option = Select.Option;
const {
	TextArea
} = Input;

let ID = getUnicodeParam('id');

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false,
			result: []
		}
	}
	render() {
		const formItemLayout = {
			labelCol: {
				span: 8
			},
			wrapperCol: {
				span: 16
			}
		};
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			name,
			schoolAreaId,
			courseId,
			salePrice,
			courseTimes,
			rated,
			desc
		} = this.state;

		if (ID && !name) {
			return null;
		}

		return (<div>

			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
			    <Breadcrumb.Item>滚动班管理</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">新建班级</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<Form onSubmit={ this._validator.bind(this) }>
				<div 
					className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-pd5"
				>
					<Row gutter={24}>
	    				<Col span={8}>
        					<FormItem
								label="班级名称"
								{...formItemLayout}
							>
								{
									getFieldDecorator('name', { 
										rules: [{
											required: true, 
											message: '请输入班级名称' 
										}],
										initialValue:name
									})
									(
										<Input
											placeholder="请输入班级名称" 
										/>
									)
								}
							</FormItem>
        				</Col>
        				<Col span={8}>
        					<FormItem
								label="校区"
								{...formItemLayout}
							>
								{
									getFieldDecorator('schoolAreaId', { 
										rules: [{
											required: true, 
											message: '请选择校区' 
										}],
										initialValue:schoolAreaId
									})
									(
										<SelectSchool 
											onSelect={
												(v) => {
													this.props.form.setFieldsValue({
														schoolAreaId:v
													});
												}
											}
											width='100%' 
											url="/api/system/schoolarea/listSchoolArea"
											initialValueData={ 
												schoolAreaId  ? [schoolAreaId] : null
											}
										/>
									)
								}
							</FormItem>
        				</Col>
        				<Col span={8}>
        					<FormItem 
        						label="课程"
        						{...formItemLayout}
    						>
								{
									getFieldDecorator('courseId', {
										rules: [{ 
											required: true,
											message:'请选择课程'
										}],
										initialValue:courseId
									})(
										<SelectCourses 
											onSelect= {
												(id,name,curData)=> {
													let {
														regularPeriods,
														regularPrice
													} = curData;
													this.props.form.setFieldsValue({
														courseId:id,
														courseTimes:regularPeriods,
														salePrice:regularPrice
													});
												}
											}
											initialValueData={ 
												courseId  ? [courseId] : null
											}
										/>
									)}
							</FormItem>
        				</Col>
        			</Row>
        			<Row gutter={24}>
	    				<Col span={8}>
        					<FormItem
								label="收费模式"
								{...formItemLayout}
							>
								按次
							</FormItem>
        				</Col>
        				<Col span={8}>
        					<FormItem
								label="学费标准"
								{...formItemLayout}
							>
								{
									getFieldDecorator('salePrice', { 
										rules: [{
											required: true, 
											message: '请输入学费标准' 
										},{
											pattern:/^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/,
											message: '请输入数字' 
										}],
										initialValue:salePrice
									})
									(
										<Input
											addonAfter="元/期"
											placeholder="请输入学费标准"
											size="large"
											onChange = {
												(e)=> {
													// console.log(e);
													// console.log(e.target.value);
												}
											}
										/>
									)
								}
							</FormItem>
        				</Col>
        				<Col span={8}>
        					<FormItem
								label="总课次"
								{...formItemLayout}
							>
								{
									getFieldDecorator('courseTimes', { 
										rules: [{
											required: true, 
											message: '请输入总课次' 
										},{
											pattern:/^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/,
											message: '请输入数字' 
										}],
										initialValue:courseTimes
									})
									(
										<Input
											addonAfter="次"
											placeholder="请输入总课次"
											size="large"
											onChange = {
												(e)=> {
													// console.log(e);
													// console.log(e.target.value);
												}
											}
										/>
									)
								}
							</FormItem>
        				</Col>
        			</Row>
    				<Row gutter={24}>
	    				<Col span={8}>
        					<FormItem
								label="额定人数"
								{...formItemLayout}
							>
								{
									getFieldDecorator('rated', { 
										rules: [{
											required: true, 
											message: '请输入额定人数' 
										},{
											pattern:/^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/,
											message: '请输入数字' 
										}],
										initialValue:rated
									})
									(
										<Input
											addonAfter="人"
											placeholder="请输入额定人数" 
											size="large"
											onChange = {
												(e)=> {
													// console.log(e);
													// console.log(e.target.value);
												}
											}
										/>
									)
								}
							</FormItem>
        				</Col>
        				<Col span={8}>
        					<FormItem
								label="备注"
								{...formItemLayout}
							>
								{
									getFieldDecorator('desc', { 
										rules: [{
											message: '请输入备注' 
										}],
										initialValue:desc
									})
									(
										<TextArea
											placeholder="请输入备注" 
										/>
									)
								}
							</FormItem>
        				</Col>
	    	        </Row>
	    	        <Row>
	    	        	<Col span="24" className="f-align-right">
			    	        <Button 
			    	        	 loading={this.state.saveing}
								type="primary"
								htmlType="submit"
								size="large" 
								icon="save" 
							>保存</Button>
						</Col>
					</Row>
				</div>
	      	</Form>

		</div>);
	}
	componentDidMount() {
		if (ID) {
			this._getData();
		}
	}
	_getData() {
		fpost('/api/educational/findClass', {
				classId: ID
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState(result);
				this.props.form.setFieldsValue(result);
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_validator(e) {
		e.preventDefault();
		let {
			id
		} = this.state;
		this.props.form.validateFields((err, values) => {
			if (!err) {
				console.log(values);
				values = {
					...values,
					chargeMode: 2
				}
				if (id) {
					values = {
						...values,
						classId: id
					}
				}
				values = undefinedToEmpty(values);
				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let url = ID ? '/api/educational/updateClass' : '/api/educational/createClass';

		fpost(url, values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					saveing: false
				});
				message.success(res.message);
				window.location.href = '/teach/classList';
			})
			.catch((err) => {
				console.log(err);
			});
	}
}

export default Form.create()(Main);