import React, {
	Component
} from 'react';
import {
	Modal,
	Form,
	message
} from 'antd';
import {
	undefinedToEmpty
} from '../../common/g.js';
import {
	fpostArray
} from '../../common/io.js';
import {
	default as MarkList
} from './markList.js';

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			
		}
	}
	render() {
		let {
			classId
		} = this.props;

		return (<Form>
			<Modal title={`记上课`}
				visible={ this.props.visibleModal }
				onCancel={this.props.toggleModal}
				confirmLoading={ false }
				onOk={this._validator.bind(this)}
				destroyOnClose={true}
				width='100%'
				bodyStyle={
					{
						height:document.body.clientHeight-100,
						paddingBottom:'70px'
					}
				}
				style={
					{
						top:'0',
						padding:'0'
					}
				}
			>
				<MarkList 
					form={ this.props.form }
					count={ this.props.count }
					classId={ classId }
					getData1={ this._getData1.bind(this) }
					getData2={ this._getData2.bind(this) }
				/>
			</Modal>
		</Form>);
	}
	_getData1(data) {
		console.log('data1:',data);

		this.setState({
			data1:data
		});
	}
	_getData2(data) {
		console.log('data2:',data);
		this.setState({
			data2:data
		});
	}
	_validator() {
		let {
			data1,
			data2
		} = this.state;

		let param={
			...data1,
			studentList:data2
		};
		this.props.form.validateFields((err, values) => {
			if (!err) {
				param={
					...param,
					...values
				}
				this._save(param);
			}
		});
	}
	_save(param) {
		param=undefinedToEmpty(param);
		console.log(param);
		fpostArray('/api/educational/createOrModifySignUp', param)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.props.toggleModal();
			})
			.catch((err) => {
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;