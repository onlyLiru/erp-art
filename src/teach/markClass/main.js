import React, {
	Component
} from 'react';
import store from 'store2';
import querystring from 'querystring';
import {
	Breadcrumb,
	Modal,
	message
} from 'antd';
import {
	default as MyList
} from './list.js';
import {
	default as SearchForm
} from './searchform.js';
import {
	fpost,
	host
} from '../../common/io.js';
import {
	numberFormate
} from '../../common/g.js';
import {
	default as MarkModal
} from './markModal.js';

import './main.less';
const confirm = Modal.confirm;
let sessionId = store.get('sessionId');

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			result: null,
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {
				chargeMode: 2
			},
			classId:'',
			count:0,//计算上周和下周的初始值

			visibleModal:false
		}
	}
	render() {
		let {
			result,
			loading,
			pagination,

			visibleModal,
			classId
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
			    <Breadcrumb.Item>滚动班管理</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">班级列表</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<SearchForm
				onSearch= { this._search.bind(this) }
			/>

			<MyList 
				data={result}
		        loading={loading}
		        onChange={this._getList.bind(this)}
		        pagination={pagination}
		        toggleModal={this._toggleMarkModal.bind(this)} 
			/>
			{
				visibleModal ?
					<MarkModal
						visibleModal={visibleModal}
						toggleModal={this._toggleMarkModal.bind(this)} 
						classId={classId}
						count={this.state.count}
					/>
				: null
			}

		</section>);
	}
	_toggleMarkModal(id) {
		this.setState({
			visibleModal:!this.state.visibleModal,
			classId:id,
			count:0
		});
	}
	componentDidMount() {
		this._getList();
	}
	_search(values) { //搜索
		// console.log(values);
		this.setState({
			searchParma: {
				...this.state.searchParma,
				...values
			},
			currentPage: 1,
			pagination: {
				...this.state.pagination,
				current: 1,
			}
		}, () => {
			this._getList();
		});
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			searchParma
		} = this.state;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		searchParma.pageSize = pageSize;
		searchParma.currentPage = currentPage;

		fpost('/api/educational/pageClass', searchParma)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result || !res.result.records) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records || [];
				let total = Number(result.total);

				data.map((d, i) => d.key = i);

				this.setState({
					result: data,
					pagination: {
						...pagination,
						total: total,
						current: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				});
			})
			.catch((err) => {
				this.setState({
					loading: false
				});
			});
	}
}