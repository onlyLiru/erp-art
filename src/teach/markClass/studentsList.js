import React, {
  Component
} from 'react';
import {
  Table,
  Radio,
  InputNumber,
  Input
} from 'antd';
const RadioGroup = Radio.Group;

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signUpStatus:'1'
    }
  }
  render() {
    let self = this;
    let {
      data
    } = this.props;
    data.map((d,i)=>d.key=i);

    const columns = [{
      title: '学生姓名(学号)',
      dataIndex: 'studentName',
      width: '150px',
      render: function(value, row, index) {
        let {
          studentNo,
          studentName
        } = row;
        return (<div>{studentName}({studentNo})</div>);
      }
    }, {
      title: '考勤情况',
      dataIndex: 'signUpStatus',
      width: '150px',
      render:function(value ,row ,index) {
        let {
          signUpStatus,
          signUpStatusStr,
          signUpDetailId
        } = row;
        return(
          signUpDetailId ? signUpStatusStr
          :
            <RadioGroup 
            size="large"
            onChange={
              (e)=> {
                let signUpStatus = e.target.value;
                self.setState({
                  signUpStatus
                });
                row.signUpStatus = signUpStatus;
                if(signUpStatus == "2") {
                  row.disciplineFocus = '';
                  row.active = '';
                  row.teacherComment = '';
                }else {
                  row.disciplineFocus = 5;
                  row.active = 5;
                }
                self.props.getData2(data);
              }
            }
            value={ value || self.state.signUpStatus }>
            <Radio value="1">出勤</Radio>
            <Radio value="2">缺勤</Radio>
          </RadioGroup>);
      }
    }, {
      title: '纪律专注',
      dataIndex: 'disciplineFocus',
      width: '150px',
      render:function(value ,row ,index) {
        let {
          signUpStatus,
          signUpStatusStr,
          signUpDetailId
        } = row;
        signUpStatus = signUpStatus || '1';
        return(
            signUpStatus == '1' ?
              <InputNumber
                size="large" 
                max={5} 
                defaultValue={value || 5} 
                formatter={
                  (value) => {
                    value = parseInt(value) || '';
                    return value;
                  }
                } 
                onChange={
                  (v)=> { 
                    row.disciplineFocus = v;
                    self.props.getData2(data);
                  }
                } 
              />
            : null
        );
      }
    }, {
      title: '课堂表现',
      dataIndex: 'active',
      width: '150px',
      render:function(value ,row ,index) {
        let {
          signUpStatus,
          signUpStatusStr,
          signUpDetailId
        } = row;
        signUpStatus = signUpStatus || '1';
        return(
          signUpStatus == '1' ?
            <InputNumber 
            size="large"
            min={1} max={5} 
            defaultValue={value || 5} 
            formatter={
              (value) => {
                value = parseInt(value) || '';
                return value;
              }
            } 
            onChange={
              (v)=> { 
                row.active = v;
                self.props.getData2(data);
              }
            } 
          /> : null
          );
      }
    }, {
      title: '老师评语',
      dataIndex: 'teacherComment',
      width: '200px',
      render:function(value ,row ,index) {
        let {
          signUpStatus,
          signUpStatusStr,
          signUpDetailId
        } = row;
        signUpStatus = signUpStatus || '1';

        return(
          signUpStatus == '1' ?
          <Input 
            size="large" 
            placeholder="点评下这孩子"
            defaultValue={value || ''} 
            onChange={
              (e)=> { 
                let v = e.target.value;
                row.teacherComment = v;
                self.props.getData2(data);
              }
            }
          /> : null
        );
      }
    }, {
      title: '备注',
      dataIndex: 'remark',
      width: '200px',
      render:function(value ,row ,index) {
        return(<Input 
          defaultValue={value || ''} 
          onChange={
            (e)=> { 
              let v = e.target.value;
              row.remark = v;
              self.props.getData2(data);
            }
          }
          size="large" 
          placeholder="备注点什么" 
        />);
      }
    }];

    return (
				<Table
			    columns={ columns }
			    dataSource={ data }
          scroll={{ x: 1200}}
          pagination={ false }
          bordered
			  />);
  }
}