import React,{Component} from 'react';
import {
	Form,
	Row,
	Col,
	Input,
	message
} from 'antd';
import {
	default as WeekTimes
} from './weekTimes.js';
import {
	default as StudentsList
} from './studentsList.js';
import {
	fpost,
	fpostArray
} from '../../common/io.js';
import {
  Loading
} from '../../common/g.js';

const FormItem = Form.Item;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state={
			loading:false,
			studentList:null,

			classSignUpId:'',
			classId:this.props.classId || '',
			classTimesRuleId:'',
			classDate:'',
			weekday:1
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			loading,
			classId,
			studentList
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		return(<div>
				<WeekTimes 
					classId = { classId }
					count = { this.props.count }
					initGetStudentsParam = { this._initGetStudentsParam.bind(this) }
				/>
				{
					studentList && !loading ?
						<StudentsList 
							data={ studentList }
							getData2={this.props.getData2}
						/>
					: null
				}
				<div className="f-pd2" />
				<Row gutter={ 40 }  style={{position:'absolute',bottom:'60px',left:'0',right:'0'}}>
					<Col span={12}>
						<FormItem
							label="上课内容"
							{...formItemLayout}
						>
							{
								getFieldDecorator('classContent', { 
									rules: [{
										required: false, 
										message: '请输入上课内容' 
									}] 
								})
								(
									<Input placeholder="请输入上课内容" />
								)
							}
						</FormItem>
					</Col>
					<Col span={12}>
						<FormItem
							label="课后作业"
							{...formItemLayout}
						>
							{
								getFieldDecorator('homework', {
									rules: [{  
										message: '请输入课后作业' 
									}]
								})(
									<Input placeholder="请输入课后作业" />
								)
							}
						</FormItem>
					</Col>
				</Row>
			</div>);
	}
	_initGetStudentsParam(data,i) {
		console.log(data);
		let {
			classContent,
			homework
		} = data;

		this.props.form.setFieldsValue({
			classContent,
			homework
		});

		this.setState({
			...data,
			weekday:i+1
		},()=> {
			this._getStudents();
			this.props.getData1(this.state);
		});
	}
	_getStudents() {
		let {
			classId,
			classSignUpId
		} = this.state;
		let param = classSignUpId ? { signUpId:classSignUpId } : { classId };
		this.setState({
			loading:true
		});
		fpost('/api/educational/listStudentsBySignUpIdOrClassId', param)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				(result || []).map((d,i)=> {
					if(!d.signUpDetailId) {
						d.signUpStatus = d.signUpStatus || '1';
						d.active = d.active || '5';
						d.disciplineFocus = d.disciplineFocus || '5';
					}
				});
				
				this.setState({
					studentList:result,
					loading: false
				});
				this.props.getData2(result);
			})
			.catch((err) => {
				console.log(err);
			});
	}
}