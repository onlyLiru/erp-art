import React, {
	Component
} from 'react';
import {
	Input,
	Button,
	Icon,
	Col,
	Row,
	Radio,
	message,
	Tag,
	Badge
} from 'antd';
import moment from 'moment';
import {
	fpost
} from '../../common/io.js';
import {
	Loading
} from '../../common/g.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state={
			count:this.props.count,
			startDate:moment().format('YYYY-MM-DD'),
			classId:this.props.classId,
			weekTimesData:null
		}
	}
	render() {
		let {
			startDate,
			weekTimesData
		} = this.state;
		if(!weekTimesData) {
			return <Loading />;
		}

		return (<section className="f-mb3">
			<div className="f-mb2">
	            <Button 
	              	size='large'
	              	onClick={
	              		()=> {
	              			this._changeWeek('-1')
	              		}
	              	}
	          	>
	                <Icon type="left" />上一周
	          	</Button>
	          	<span className="f-ml3 f-mr3">
	          		{ weekTimesData[0]['classDate'] }
	          		<span className="f-ml1 f-mr1">至</span>
	          		{ weekTimesData[weekTimesData.length-1]['classDate'] }
	          	</span>
	          	<Button
	          		size='large'
		          	onClick={
		          		()=> {
		          			this._changeWeek('1')
		          		}
		          	}
	          	>
	            	下一周<Icon type="right" />
	          	</Button>
	          	<span className="f-ml3 f-pale">注意:打红色标的没有记过上课</span>
          	</div>
          	<div className="weekTimesList">
          		{
	          		weekTimesData.map((d,i)=> {
	          			let {
	          				classDateStr,
	          				weekday,
	          				classTimesDetailList,
	          				isToday
	          			} = d;

	          			return(<div key={i}>
	          				<div className="weeks">
	          					<h3>{classDateStr} {isToday=='1' ? '(今天)' : null}</h3>
	          					<h3>{weekday}</h3>
	          				</div>
	          				<div 
	          				className="times">
	          					{
	          						classTimesDetailList.map((dd,ii)=> {
	          							let {
	          								startTime,
	          								endTime,
	          								classSignUpId,
	          								cur
	          							} = dd;
	          							return(<div 
	          							onClick={
	          								()=> {
	          									weekTimesData.map((myData,myIndex)=> {
	          										let {
	          											classTimesDetailList
	          										} = myData;
	          										classTimesDetailList.map((myData1,myIndex1)=> {
	          											myData1.cur = false;
	          										});
	          									});
	          									dd.cur = true;
	          									this.props.initGetStudentsParam(dd,i);
	          								}
	          							}
	          							className={ cur==true ? 'cur' : 'theTime' }
	          							key={ii}>
	          								<span className="f-mr1">{ startTime } - { endTime }</span>
	          								{ classSignUpId ? 
	          									null
	          									: 
	          									<Badge status="error" />
	          								}
	          							</div>);
	          						})
	          					}
	          				</div>
	          			</div>);
	          		})
          		}
          	</div>
		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_getList() {
		let {
			classId
		} = this.props;

		fpost('/api/educational/listWeekdayClassTimesByStartDateAndEndDate', {
			classId:classId,
			startDate:this.state.startDate
		})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					weekTimesData:result
				},()=> {
					for(let i=0;i<result.length; i++) {
						if(result[i].classTimesDetailList && result[i].classTimesDetailList.length) {
							result[i].classTimesDetailList[0].cur=true;
							this.props.initGetStudentsParam(result[i].classTimesDetailList[0],i);
							break;
						}
					}
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_changeWeek(type) {
		let startDate;
		let count= this.state.count;
		if(type=='-1'){//上一周
			count = count == 0 ? count-6 : count-7;
			startDate= moment().day(count).format('YYYY-MM-DD');
		}else if(type =='1') {//下一周
			count = count == 0 ? count+8 : count+7;
			startDate= moment().day(count).format('YYYY-MM-DD');
		}
		
		this.setState({
			count,
			startDate
		},this._getList);
	}
}