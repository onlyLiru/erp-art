import React, {
	Component
} from 'react';
import {
	Input,
	Button,
	Icon,
	Col,
	Row,
	Form,
	DatePicker,
	Select
} from 'antd';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import SelectCourses from '../../common/selectCourses.js';
import SelectSchool from '../../common/selectSchool.js';
import RangeDate from '../../common/rangeDate.js';
import {
	undefinedToEmpty,
	getDay
} from '../../common/g.js';
import {
	status
} from '../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option; 


class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isShow: true,
			startDate:getDay(0),
			endDate:getDay(7)
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			isShow,
			startDate,
			endDate
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		if (!isShow) {
			return null;
		}

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form>
				<Row gutter={ 40 }>
    				<Col span={8}>
    					<FormItem
							label="上课日期"
							{...formItemLayout}
						>
							{
								getFieldDecorator('rangeTime',{
									initialValue:''
								})(
									<RangeDate 
										onSelect={
											(d)=> {
												this.props.form.setFieldsValue({
													rangeDate:d,
													startDate:d.startString,
													endDate:d.endString,
												});
											}
										}
										showTime={false}
										initialValueData={{
											startTime:startDate,
											endTime:endDate
										}}
									/>
								)
							}
						</FormItem>
						<span className="f-hide">
					        {
								getFieldDecorator('startDate',{
									initialValue:startDate
								})(
									<Input />
								)
							}
							{
								getFieldDecorator('endDate',{
									initialValue:endDate
								})(
									<Input />
								)
							}
						</span>
    				</Col>
    				<Col span={8}>
						<FormItem
							label="校区"
							{...formItemLayout}
						>
							{
								getFieldDecorator('schoolAreaId', { 
									rules: [{
										message: '请选择校区' 
									}],
									initialValue:''
								})
								(
									<SelectSchool 
										onSelect={
											(v) => {
												this.props.form.setFieldsValue({
													schoolAreaId:v
												});
											}
										}
										width='100%' 
										url="/api/system/schoolarea/listSchoolArea"
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={ 8 }>
						<FormItem
							label="课程"
							{...formItemLayout}
						>
							{
								getFieldDecorator('courseId', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<SelectCourses 
										onSelect= {
											(v)=> {
												this.props.form.setFieldsValue({
													courseId:v
												});
											}
										}
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={8}>
    					<FormItem
							label="班级名称"
							{...formItemLayout}
						>
							{
								getFieldDecorator('className', { 
									rules: [{
										required: false, 
										message: '请输入班级名称' 
									}] 
								})
								(
									<Input placeholder="请输入班级名称" />
								)
							}
						</FormItem>
    				</Col>
    	        </Row>
    	        <div className="f-align-center">
    	        	<Button 
    	        		size="large" 
    	        		className="f-mr4" 
    	        		onClick={ this._reset.bind(this) }
    	        	>重置</Button>
    	        	<Button 
    	        		type="primary"
    	        		size="large" 
    	        		icon="search" 
    	        		onClick={ this._search.bind(this) }
    	        	>查询</Button>
    	        </div>
	      	</Form>
		</header>);
	}
	componentDidMount() {

	}
	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			isShow: false
		}, () => {
			this.setState({
				isShow: true
			}, () => {
				this._search();
			})
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;