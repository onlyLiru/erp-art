import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../common/io.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">班级课表</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
				  />
			</div>
    );
  }

}

const columns = [{
  title: '日期',
  dataIndex: 'classDate',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '时间段',
  dataIndex: 'age',
  className: 'f-align-center',
  width: '100px',
  render:function(value,row,index) {
    let{
      startTime,
      endTime
    } = row;
    return(<div>{startTime}-{endTime}</div>);
  } 
}, {
  title: '班级名称',
  dataIndex: 'className',
  width: '150px'
}, {
  title: '校区',
  dataIndex: 'schoolAreaName',
  width: '150px'
}, {
  title: '上课老师',
  dataIndex: 'teacherName',
  width: '100px'
}, {
  title: '教室',
  dataIndex: 'roomName',
  width: '100px'
}];