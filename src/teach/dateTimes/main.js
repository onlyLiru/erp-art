import React, {
	Component
} from 'react';
import moment from 'moment';
import {
	Breadcrumb,
	message
} from 'antd';
import {
	default as MonthTimes
} from './month.js';
import {
	default as Week
} from './week.js';
import {
	default as SearchForm
} from './searchform.js';
import {
	fpost,
	host
} from '../../common/io.js';
import {
	numberFormate,
	getDay
} from '../../common/g.js';
import '../main.less';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			rangeType:'1',
			startDate:moment().format('YYYY-MM-DD'),
			schoolAreaId:'',
			result: []
		}
	}
	render() {
		let {
			result,
			loading,
			pagination,
			startDate
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
			    <Breadcrumb.Item>教务</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">日期课表</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<SearchForm
				onSearch= { this._getList.bind(this) }
				rangeType={this.state.rangeType}
				startDate={startDate}
				ref={form=>this.form=form}
			/>

			{
				this.state.rangeType =='1' ?
				<Week 
					data={result}
				/>
				:
				<MonthTimes 
					data={result}
					startDate={startDate}
					onSearch= { this._getList.bind(this) }
				/>
			}


		</section>);
	}
	componentDidMount() {

	}
	_getList(param) {
		var self = this;
		this.setState({
			loading: true,
			...param
		},()=>{
			this.form.validateFields((err, values) => {
				if (!err) {
					this.setState({
						...values
					},self._getData);
				}
			});
		});
	}
	_getData() {
		console.log(this.state);
		let {
			schoolAreaId,
			startDate,
			rangeType
		}= this.state;
		let param={
			schoolAreaId,
			startDate,
			rangeType
		}
		fpost('/api/educational/listDailyClassTimeTableByCond', param)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					result,
					loading: false
				});
			})
			.catch((err) => {
				console.log(err);

				this.setState({
					loading: false
				});
			});
	}
}