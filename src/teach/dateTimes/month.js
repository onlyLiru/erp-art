import React, {
  Component
} from 'react';
import {
  Calendar,
  Icon,
  Dropdown,
  Menu,
  Button,
  message,
  Popover
} from 'antd';
import moment from 'moment';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../common/io.js';

let colors=['#00CCFF','#CCCC00','#FFCCFF','#00CCFF','#CCCC00','#FFCCFF','#00CCFF','#CCCC00','#FFCCFF','#00CCFF','#CCCC00','#FFCCFF','#00CCFF','#CCCC00','#FFCCFF']

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state={
      startDate:this.props.startDate
    }
    this._getListData=this._getListData.bind(this);
  }
  render() {

    let {
      data,
      onSearch,
      startDate
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
				<Calendar 
          dateCellRender={this._dateCellRender.bind(this)} 
          value={moment(startDate)}
          onPanelChange={
            (v)=> {
              onSearch({
                startDate:v.format('YYYY-MM-DD')
              });
            }
          } 
        />
			</div>
    );
  }
  _getListData(day) {
    let {
      data
    } = this.props;
    let listData;

    data.map((d,i)=> {
      if(d.classDate==day) {
        listData = d
      };
    });
   
    return listData || [];
  }
  _dateCellRender(value) {
    let {
      data
    } = this.props;
    let day = value.format('YYYY-MM-DD');
    
    return data.map((d,i)=> {
      let {
        timeTableList,
        classDate
      } = d;
      if(classDate==day) {
        return this._initHtml(d);
      };
    });

  }
  _initHtml(data) {
    let {
      timeTableList
    } = data;
    return (
      timeTableList.map((dd,ii)=>{
        const content = (
          <div>
            <div className="f-line1">
              <span className="f-mr1 f-pale">校区:</span> 
              {dd.schoolAreaName}
            </div>
            <div className="f-line1">
              <div>
                <span className="f-mr1 f-pale">时间:</span> 
                {dd.classDate}
              </div>
              <div>
                <span className="f-mr1 f-pale" style={{visibility:'hidden'}}>时间:</span> 
                {dd.startTime}-{dd.endTime}
              </div>
            </div>
            <div className="f-line1">
              <span className="f-mr1 f-pale">班级:</span> 
              {dd.className}
            </div>
            <div className="f-line1">
              <span className="f-mr1 f-pale">老师:</span>
              {dd.teacherName}
            </div>
            <div className="f-line1">
              <span className="f-mr1 f-pale">教室:</span>
              {dd.roomName}
            </div>
          </div>
        );
        return (<div 
            key={ii}
            style={{
              background:colors[ii],
              color:'#fff',
              borderRadius:'4px',
              padding:'2px 4px',
              marginBottom:'2px'
            }}
          >
          <Popover content={content} placement="left" trigger="hover">
            <div>{dd.startTime}-{dd.endTime}</div>
            <div className="f-line1">{dd.className}</div>
          </Popover>
        </div>);
      })
    );
  }
}
