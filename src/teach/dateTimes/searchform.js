import React, {
	Component
} from 'react';
import moment from 'moment';
import {
	Input,
	Button,
	Icon,
	Col,
	Row,
	Form,
	DatePicker,
	Select,
	Radio
} from 'antd';
import SelectSchool from '../../common/selectSchool.js';

const FormItem = Form.Item;
const Option = Select.Option; 
let count=0;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			rangeType:this.props.rangeType,
			startDate:this.props.startDate
		}
		this._changeWeek=this._changeWeek.bind(this);
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			rangeType,
			startDate
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form style={{marginBottom:'-20px'}}>
				<Row gutter={ 40 }>
    				<Col span={8}>
						<FormItem
							label="校区"
							{...formItemLayout}
						>
							{
								getFieldDecorator('schoolAreaId', { 
									rules: [{
										message: '请选择校区' 
									}],
									initialValue:''
								})
								(
									<SelectSchool 
										onSelect={
											(v) => {
												this.props.form.setFieldsValue({
													schoolAreaId:v
												});
												this._search();
											}
										}
										width='100%' 
										url="/api/system/schoolarea/listSchoolArea"
										setDefault={true}
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={ 16 }>
    					<Radio.Group 
    						className="f-mr5" 
    						size='large' 
    						defaultValue={rangeType} 
    						onChange={this._handleSizeChange.bind(this)}
						>
				          <Radio.Button value="1">周课表</Radio.Button>
				          <Radio.Button value="2">月课表</Radio.Button>
				        </Radio.Group>
				        {
				        	this.state.rangeType == '1' ?
				        		<Button.Group size='large'>
				                  <Button 
				                  	onClick={
				                  		()=> {
				                  			this._changeWeek('-1')
				                  		}
				                  	}
			                  	>
				                    <Icon type="left" />上一周
				                  </Button>
				                  <Button
				                  	onClick={
				                  		()=> {
				                  			this._changeWeek('1')
				                  		}
				                  	}
				                  >
				                    下一周<Icon type="right" />
				                  </Button>
				                </Button.Group>
				            : null
				        }
					</Col>
    	        </Row>
	      	</Form>
		</header>);
	}
	componentDidMount() {

	}
	_handleSizeChange(e) {
		this.setState({
			rangeType:e.target.value
		},this._search);
	}
	_changeWeek(type) {
		let startDate = this.state.startDate;
		if(type=='-1'){//上一周
			count = count == 0 ? count-6 : count-7;
			startDate= moment().day(count).format('YYYY-MM-DD');
		}else if(type =='1') {//下一周
			count = count == 0 ? count+8 : count+7;
			startDate= moment().day(count).format('YYYY-MM-DD');
		}
		this.setState({
			startDate
		},this._search);
	}
	_search(e) { //搜索
		let {
			rangeType,
			startDate
		} = this.state;
		let {
			onSearch
		} = this.props;
		onSearch({
			rangeType,
			startDate
		});
		// this.props.form.validateFields((err, values) => {
		// 	if (!err) {
		// 		values = {
		// 			...values,
		// 			rangeType,
		// 			startDate
		// 		}
		// 		onSearch(values);
		// 	}
		// });
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;