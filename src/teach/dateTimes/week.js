import React, {
  Component
} from 'react';
import {
  Popover,
  Row,
  Col
} from 'antd';
import moment from 'moment';

let colors=['#00CCFF','#CCCC00','#FFCCFF','#00CCFF','#CCCC00','#FFCCFF','#00CCFF','#CCCC00','#FFCCFF','#00CCFF','#CCCC00','#FFCCFF','#00CCFF','#CCCC00','#FFCCFF']

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state={

    }
  }
  render() {

    let {
      data
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
				<Row gutter={6} type="flex">
          {
            data.map((d,i)=> {
              let {
                timeTableList,
                weekday,
                classDate
              } = d;
              let span = 3;
              if(i==0 || i==2 || i==4) {
                span = 4;
              }
              return(
                <Col key={i} span={span}>
                  <div>
                    <p className="f-align-right">{weekday}</p>
                    <p className="f-align-right">{classDate}</p>
                    <div style={{background:'#f2f2f2',height:'6px',marginBottom:'5px'}}></div>
                    {
                      timeTableList.map((dd,ii)=> {
                        const content = (
                          <div>
                            <div className="f-line1">
                              <span className="f-mr1 f-pale">校区:</span> 
                              {dd.schoolAreaName}
                            </div>
                            <div className="f-line1">
                              <div>
                                <span className="f-mr1 f-pale">时间:</span> 
                                {dd.classDate}
                              </div>
                              <div>
                                <span className="f-mr1 f-pale" style={{visibility:'hidden'}}>时间:</span> 
                                {dd.startTime}-{dd.endTime}
                              </div>
                            </div>
                            <div className="f-line1">
                              <span className="f-mr1 f-pale">班级:</span> 
                              {dd.className}
                            </div>
                            <div className="f-line1">
                              <span className="f-mr1 f-pale">老师:</span>
                              {dd.teacherName}
                            </div>
                            <div className="f-line1">
                              <span className="f-mr1 f-pale">教室:</span>
                              {dd.roomName}
                            </div>
                          </div>
                        );
                        return (<div 
                            key={ii}
                            style={{
                              background:colors[ii],
                              color:'#fff',
                              borderRadius:'4px',
                              padding:'2px 4px',
                              marginBottom:'2px'
                            }}
                          >
                          <Popover content={content} placement="left" trigger="hover">
                            <div>{dd.startTime}-{dd.endTime}</div>
                            <div className="f-line1">{dd.className}</div>
                          </Popover>
                        </div>);
                      })
                    }
                  </div>
                </Col>
              );
            })
          }
        </Row>
			</div>
    );
  }
}
