import React, {
	Component
} from 'react';
import { Breadcrumb, Button, Table, Modal, DatePicker, Form, Row, Col, Select, message, Pagination, TimePicker } from 'antd';
import moment from 'moment';
import '../main.less';
import { fpost } from '../../common/io.js';
import { getUrlParam, Loading } from '../../common/g.js';
import $ from 'jquery';
const confirm = Modal.confirm;
const FormItem = Form.Item;
const u=require('../../common/io.js');
const Option = Select.Option;
const dateFormat = 'YYYY-MM-DD HH:mm:ss';
const dateFormat1 = 'HH:mm:ss';
let listLength;
let isb=[];
export class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			visible: false,
			batch: false, //是否批量
			currentPage: 1,
			pageSize: 10,
			pageResult: null, //分页返回的列表总信息
			total: null,
			classroomList: [],
			masterList: [], //班主任列表
			clazzVO: {},
			modalState: '1', //批量修改
			classStartTime: null,
			classEndTime: null,
			classDate: null,
			teacherId: null,
			roomId: null,
			confirmLoading:false,
			ww:'',
			selectedRowKeys: [],
		}
		this.modalConfirm = this.modalConfirm.bind(this)

	}
	showModal1 = () => {
		this._getClassroom(); //获取教室
		this._getMaster(); //获取上课老师
		this.setState({
			visible: true, //确定对话框的展示
			start: null
		})
	}
	showConfirm() {
	  confirm({
	    title: '无法保存的(id以及对应的原因）：',
	    content: this.state.ww,
	    onOk() {
	    },
	    onCancel() {},
	  });
	 }
	showModal = (data) => {
		this._init(data);
		this._getClassroom(); //获取教室
		this._getMaster(); //获取上课老师
		this.setState({
			visible: true, //确定对话框的展示
			modalState: '2', //单个修改
		}, function() {
			if(this.state.modalState == '2') {
				this.setState({
					id: data.id
				})
			}
		});
//		$('.sksd').parents('.ant-form-item-control-wrapper').siblings('div.ant-col-4').find('label').attr('title','fsdfdsfd')
	}
	_init(data) {
		fpost('/api/educational/class/times/detail/update/init', {
				classTimesDetailId: data.id
			})
		.then((res) => {
			return res.json();
		})
		.then((res) => {
			if(res.success == true) {
				this.setState({
					classEndTime: res.result.classEndTime.split(' ')[1],
					classStartTime: res.result.classStartTime.split(' ')[1],
					classDate: res.result.classDate.split(' ')[0],
					roomId: res.result.roomId,
					teacherId: res.result.teacherId
				})
			} else {
				message.error(res.message)
			}
		});
	}
	handleOk = (e) => { //对话框里的确定按钮对应事件
		if(this.state.modalState == '2') { //单个修改
			this._singleSave();
		} else {
			this._doubleSave();
		}
	}
	_doubleSave() {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				const data = {}
				if(!err) {
					fieldsValue['classStartTime'] = fieldsValue['classStartTime'] ? fieldsValue['classStartTime'].format('HH:mm:ss') : '';
					fieldsValue['classEndTime'] = fieldsValue['classEndTime'] ? fieldsValue['classEndTime'].format('HH:mm:ss') : '';
					fieldsValue['teacherId'] = fieldsValue['teacherId'] ? fieldsValue['teacherId'] : "";
					fieldsValue['roomId'] = fieldsValue['roomId'] ? fieldsValue['roomId'] : "";
					data.startTime = fieldsValue['classStartTime'];
					data.endTime = fieldsValue['classEndTime'];
					data.roomId = fieldsValue['roomId'];
					data.teacherId = fieldsValue['teacherId'];
					if(data.startTime > data.endTime) {
						message.error('开始时间不能大于结束时间哦');
						return false;
					}
					let str=JSON.stringify(isb).split('[')[1].split(']')[0];
					var reg = new RegExp('"',"g");  
					str = str.replace(reg, ""); 
					data.classTimesDetailIds = str;
					data.schoolAreaId = this.state.schoolAreaId || "";
					if(!data.classEndTime && !data.classStartTime && data.roomId.length==0 && data.teacherId.length==0) {
						message.error('请至少选填一个进行修改');
						return false;
					}
					this.save2(data);
				}
			}
		)
	}
	_singleSave() {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				const data = {}
				if(!err) {
					fieldsValue['classDate'] = fieldsValue['classDate'] ? fieldsValue['classDate'].format('YYYY-MM-DD HH:mm:ss') : '';
					fieldsValue['classStartTime'] = fieldsValue['classStartTime'] ? fieldsValue['classStartTime'].format('HH:mm:ss') : '';
					fieldsValue['classEndTime'] = fieldsValue['classEndTime'] ? fieldsValue['classEndTime'].format('HH:mm:ss') : '';
					fieldsValue['teacherId'] = fieldsValue['teacherId'] ? fieldsValue['teacherId'] : "";
					fieldsValue['roomId'] = fieldsValue['roomId'] ? fieldsValue['roomId'] : "";
					data.classDate = fieldsValue['classDate'];
					if(fieldsValue['classStartTime']) {
						data.classStartTime = fieldsValue['classDate'].split(' ')[0] + ' ' + fieldsValue['classStartTime'];
					} else {
						data.classStartTime = '';
					}
					if(fieldsValue['classEndTime']) {
						data.classEndTime = fieldsValue['classDate'].split(' ')[0] + ' ' + fieldsValue['classEndTime'];
					} else {
						data.classEndTime = '';
					}
					data.roomId = fieldsValue['roomId'];
					data.teacherId = fieldsValue['teacherId'];
					if(data.classStartTime > data.classEndTime) {
						message.error('开始时间不能大于结束时间哦');
						return false;
					}
					if(!data.classDate && !data.classEndTime && !data.classStartTime && data.roomId.length==0 && data.teacherId.length==0) {
						message.error('请至少选填一个进行修改');
						return false;
					}
					data.id = this.state.id;
					data.schoolGroupId = this.state.schoolGroupId || '';
					data.schoolAreaId = this.state.schoolAreaId || '';
					this.save1(data);
				}else{
					message.error('请先完善信息再保存哦');
				}
			}
		)
	}
	save2(data) { //批量修改的保存
		let self = this;
		fpost('/api/educational/class/times/detail/batch/update', data)
			.then((res) => {
				return res.json();
			})
		.then((res) => {
			if(res.success == true) {
				if(res.result=='ok'){
					message.success('批量修改成功');
					self.setState({
				      confirmLoading: true,
				    });
					setTimeout(() => {
						self.setState({
							visible: false, //确定对话框的展示
							modalState: '1',
							confirmLoading:false,
							batch:false,
						},function(){
							window.location.reload();
						})
					}, 500);
				}else if(res.result.length>0){
						const ss=res.result;
						const qq=[];
						ss.map((item,i)=> {
			              qq.push(JSON.stringify(item).split('{')[1].split('}')[0]+';')
			                        	})
						self.setState({
							ww:JSON.stringify(qq).split('[')[1].split(']')[0].replace(/\\/g,'').replace(/"/g,'').replace(/,/g,'\n')
						},function(){
							self.showConfirm();
						})
				}
				
			} else {
				message.error(res.message)
			}
		});
	}
	save1(data) { //单个修改保存
		let self = this;
		fpost('/api/educational/class/times/detail/update', data)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result=='ok'){
						message.success('修改成功');
						self.setState({
					      confirmLoading: true,
					    });
						setTimeout(() => {
							self.setState({
								visible: false, //确定对话框的展示
								modalState: '1',
								confirmLoading:false,
								batch:false,
							},function(){
								window.location.reload();
							})
						}, 500);
					}else{
						message.error(res.result);
						self.setState({
							confirmLoading:false,
						});
					}
				} else {
					message.error(res.message);
					self.setState({
						confirmLoading:false,
					});
				}
			});
	}
	handleCancel = (e) => { //对话框里的取消按钮对应的事件
		this.props.form.resetFields(); //重置
		this.setState({
			visible: false,
			start: null,
			modalState: '1',
			roomId: '',
			teacherId: '',
			batch:false,
			confirmLoading:false,
			classStartTime:null,
			classEndTime:null
		});
	}
	modalConfirm(data) { //确定每条数据的修改按钮对应的对话框如何展示
		this.showModal(data);
		this.setState({
			batch: true
		});
	}
	componentDidMount() {
		this._getDetail(); //获取页面详情
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			pageResult:[],
			currentPage: pageNumber
		}, function() {
			this._getDetail();
		});
	}
	_getMaster() { //获取班主任下拉
		const self = this;
		fpost('/api/hr/staff/listStaffs')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					self.setState({
						masterList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getDetail1() {
		const self = this;
		fpost('/api/educational/class/times/detail/manage', {
				classId: getUrlParam('id'),
				currentPage: self.state.currentPage,
				pageSize: self.state.pageSize
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result.classTimesDetailVOPage) {
						res.result.classTimesDetailVOPage.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.classTimesDetailVOPage.records,
							total: parseInt(res.result.classTimesDetailVOPage.total),
						});
						$("table tbody tr").each(function(i, e){
		            			for(var i=0; i<isb.length; i++) {
						        if(isb[i] == $(e).find('input').attr('data-isbn')) {
						           $(e).find('input').attr('checked',true)
						        }
					        }
			            });
			            self._event();
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_event(){
		$('table tbody tr .checkbox').off().on('click',function(){
            	$("tbody tr").each(function(i, e){
            		if($(e).find('input').is(":checked")){
        				if($(e).find('input').attr('data-isbn')!=isb[i]){
//	            					使用indexOf查找某元素，确认数组中不存在这个元素时再把该元素添加至数组末尾
        					if(isb.indexOf($(e).find('input').attr('data-isbn'))==-1){
        						isb.push($(e).find('input').attr('data-isbn'));
        					}
        				}
					$(e).find('input').attr('data-select','')
            		}else{
            			for(var i=0; i<isb.length; i++) {
				        if(isb[i] == $(e).find('input').attr('data-isbn')) {
//					        	数组从i位置开始,删除一个元素
				            isb.splice(i, 1);
				            break;
				        }
			        }
            		}
            });
        });
	}
	_getDetail() {
		const self = this;
		fpost('/api/educational/class/times/detail/manage', {
				classId: getUrlParam('id'),
				currentPage: self.state.currentPage,
				pageSize: self.state.pageSize
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result.classTimesDetailVOPage) {
						res.result.classTimesDetailVOPage.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.classTimesDetailVOPage.records,
							total: parseInt(res.result.classTimesDetailVOPage.total),
							clazzVO: res.result.clazzVO,
							schoolAreaId: res.result.classTimesDetailVOPage.records ? res.result.classTimesDetailVOPage.records[0].schoolAreaId : '',
							schoolGroupId: res.result.classTimesDetailVOPage.records ? res.result.classTimesDetailVOPage.records[0].schoolGroupId : "",
						},function(){
							isb=[];
						});
						for(var i = 0; i < $('.classEndTime').length; i++){
							$('.classEndTime').eq(i).html('--'+$('.classEndTime').eq(i).html().split(' ')[1])
						}
			            self._event();
					}
				} else {
					message.error(res.message)
				}
			});
	}
	selectModal = (e) => { //确定是否可以进行批量修改，若未选择某条数据则有错误信息提示
		if(isb&&isb.length) {
			this.showModal1();
		} else {
			message.warning('您还未选中需要修改的记录哦', 2);
		}
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}

	onStartChange = (value) => {
		this.onChange('startValue', value);
	} //开始日期改变
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	onShowSizeChange (current, pageSize) {
		this.setState({
			currentPage:current,
			pageSize:pageSize
		},function(){
			this._getDetail()
		})
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ',{
			schoolAreaId:self.state.schoolAreaId
		})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	allSelect(){  //全选
		for (let i=0;i<$('table tbody tr').length;i++){
			isb=[];
			if($('table tbody tr').eq(i).find('input').attr('disabled')){
			}else{
				$('table tbody tr').eq(i).find('input').prop('checked',true);
            		isb.push($('table tbody tr').eq(i).find('input').attr('data-isbn'));
			}
			
		}
	}
	cancelAllselect(){  //取消全选
		for (let i=0;i<$('table tbody tr').length;i++){
			isb=[];
			$('table tbody tr').eq(i).find('input').prop('checked',false);
		}
	}
	render() {
		const columns = [{
			title: '选择',
			key: '10',
			width: 100,
			className: 'f-align-center',
			render: (text, record, index) => {
				return(
					<span className='f-relative' style={{width:'14px',height:'14px',display:'block',left:'45%',backgroundColor: '#fff'}}>
						<input name={record.id} type="checkbox" disabled={record.isSignUp==1?true:false} style={{left:0,top:0,backgroundColor: '#fff'}} className="check checkbox f-absolute hightC_white" data-rel='1' data-id={record.id} data-isbn={record.id} data-select='1'/>
					</span>
				)
			}
		},{
			title: 'id',
			key: '0',
			width: 100,
			dataIndex: 'id',
			className: 'f-align-center',
		},{
			title: '课次',
			key: '1',
			width: 100,
			dataIndex: 'classTimes',
			className: 'f-align-center',
		}, {
			title: '课次状态',
			dataIndex: 'displaySignUp',
			key: '2',
			width: 100,
			className: 'f-align-center',
		}, {
			title: '上课时间',
			key: '3',
			width: 180,
			className: 'f-align-center',
			render: (text, record, index) => {
				return(
					<div>
						<i className='classStartTime'>{record.classStartTime}</i>
						<i className='classEndTime'>{record.classEndTime}</i>
					</div>
				)
			}
		}, {
			title: '课时',
			dataIndex: 'period',
			key: '4',
			className: 'f-align-center',
			width: 100,
		}, {
			title: '教室',
			dataIndex: 'classRoomName',
			key: '5',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '上课老师',
			dataIndex: 'teacherName',
			key: '6',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '操作',
			key: '7',
			className: 'f-align-center',
			width: 150,
			render: (text, record, index) => {
				return(<div>
		            	{record.isSignUp==1?<Button className="f-radius4 f-bg-blue f-white" onClick={()=>{window.location.href='/teach/checkRecord?id='+record.id}}><i className='iconfont icon-biji'></i>查看出勤</Button>:
		            		 <div><i className='f-pointer f-mr5 iconfont icon-bianji1 f-fz5' onClick={()=>this.modalConfirm(record)}></i>
		                		<Button className='f-radius4 f-bg-green f-white f-viewdetail-check' onClick={()=>{window.location.href='/teach/modifyRecord?id='+record.id+'&state=1'+'&linkId='+getUrlParam('id')}}><i className='iconfont icon-iconfontrizhijilu f-mr1 f-viewdetail-down' style={{fontSize:'15px'}}></i>记上课</Button>
		                	</div>
		            	}
		            </div>);
			}
		}];

		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 4
			},
			wrapperCol: {
				span: 20
			}
		};
//		const rowSelection = { //列表前面的复选框
//			onChange: (selectedRowKeys, selectedRows) => {
//				let ids = [];
//				for(var i=0;i<selectedRows.length;i++){
//					ids.push(selectedRows[i].id)
//				}
//				listLength = JSON.stringify(ids);
//			},
//			getCheckboxProps: record => ({
////				disabled: record.isSignUp === '1', // Column configuration not to be checked
//			}),
//		};

		return(<div className='courseArranging classListPage'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>班级管理</Breadcrumb.Item>
				    <Breadcrumb.Item><a href='/teach/classList'>班级列表</a></Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">排课管理</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='f-bg-white f-flex f-radius1 f-box-shadow1' style={{padding:'30px 20px',justifyContent: 'space-between'}}>
					<div>
						<span className='f-dark f-fz3'>班级名称：</span>
						<span className='f-15 f-fz4'>{this.state.clazzVO.name}</span>
					</div>
					<div>
						<span className='f-dark f-fz3'>校区：</span>
						<span className='f-15 f-fz4'>{this.state.clazzVO.schoolAreaName}</span>
					</div>
					<div>
						<span className='f-dark f-fz3'>上课老师：</span>
						<span className='f-15 f-fz4'>{this.state.clazzVO.teacherName}</span>
					</div>
					<div>
						<span className='f-dark f-fz3'>课次：</span>
						<span className='f-15 f-fz4'>{this.state.clazzVO.classTimes}次</span>
					</div>
					<div>
						<span className='f-dark f-fz3'>班级状态：</span>
						<span className='f-15 f-fz4'>{this.state.clazzVO.stateName}</span>
					</div>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
				<div className='f-pb2 f-flex' style={{justifyContent: 'space-between'}}>
					<h3 className="f-title-blue f-mb3">排课信息</h3>
					<a className="f-btn-blue" onClick={this.selectModal}>批量修改</a>
					<Modal
			          title='修改'
			          visible={this.state.visible}
			          onOk={this.handleOk}
			          onCancel={this.handleCancel}
			          confirmLoading={this.state.confirmLoading}
			          maskClosable={false}
			         >
					<Form onSubmit={this.handleSubmit} className='viewDetailPage'>
						{this.state.batch?
							<FormItem {...formItemLayout} label='上课日期'>
				                  {getFieldDecorator('classDate', {
				                  	rules: [{
						              required: true,
						              message: '请选择上课日期',
						            }],
				                  	initialValue:this.state.classDate ?moment(this.state.classDate, dateFormat):null,
				                  })(<DatePicker style={{width:'100%'}}/>)}
		                		</FormItem>:null
						}
						 <Row>
						 <Col span={4} style={{color: 'rgba(0, 0, 0, 0.85)',fontSize:'14px'}} className='f-label'>上课时段:</Col>
	                        <div className='f-flex wid100 width100 sksd'>
				    			 <Col span={11}>
				    			 <FormItem>
						          {getFieldDecorator('classStartTime',{
						          	rules: [{
						              required: true,
						              message: '请选择开始时段',
						            }],
						          	initialValue: this.state.classStartTime?moment(this.state.classStartTime, dateFormat1):null,
						          })(
						            <TimePicker style={{width:'100%'}}/>
						          )}
						     </FormItem>
				    			 </Col>
				    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
				    			  <Col span={11}>
				    			  <FormItem>
						          {getFieldDecorator('classEndTime',{
						          	rules: [{
						              required: true,
						              message: '请选择结束时段',
						            }],
						          	initialValue: this.state.classEndTime?moment(this.state.classEndTime, dateFormat1):null,
						          })(
						            <TimePicker style={{width:'100%'}}/>
						          )}
						     </FormItem>
					         	</Col>
					        </div>
                        </Row>
                        <FormItem {...formItemLayout} label='上课老师'>
				          	{getFieldDecorator('teacherId', {
			                  	rules: [{
					              required: true,
					              message: '请选择上课老师',
					            }],
						          initialValue: this.state.teacherId?this.state.teacherId.toString():[],
			                  })(
			                    <Select
			                  	  showSearch
						          placeholder='请选择上课老师'
						          optionFilterProp="children"
                					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						        >
									{this.state.masterList?
					                        	this.state.masterList.map((item,i)=> {
					                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
					                        	})
					                        	:null
				                      }
						        </Select> 
			                  )}
				     	</FormItem>
                        
	                		<FormItem {...formItemLayout} label='教室'>
		                  {getFieldDecorator('roomId', {
		                  	rules: [{
				              required: true,
				              message: '请选择教室',
				            }],
						          initialValue: this.state.roomId?this.state.roomId.toString():[],
			                  })(
			                  	<Select showSearch placeholder="请选择" optionFilterProp="children" style={{width:'100%'}}
			                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
				                      {this.state.classroomList?
					                        	this.state.classroomList.map((item,i)=> {
					                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
					                        	})
					                        	:null
				                      }
			                      </Select>
			                  )}
	                		</FormItem>
					</Form>
			        </Modal>
				</div>
				<div className='f-bg-white'>
					{this.state.pageResult?this.state.pageResult.length?
						<div className='viewDpage'>
							<div className='f-mb2'>
								<Button className='f-mr2' onClick={this.allSelect.bind(this)}>全选</Button><Button onClick={this.cancelAllselect.bind(this)}>取消全部</Button>
							</div>
							<Table columns={columns} dataSource={this.state.pageResult} bordered pagination={false} />
								<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
									<div>
										<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
									</div>
								</div>
						</div>
						:<Loading/>
						:<div className='f-align-center'>暂无数据......</div>
					}
				</div>
				</div>
				
		</div>);
	}
}
const ViewDetails = Form.create()(Main);
export {
	ViewDetails
}