import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message,
  Progress,
  Modal
} from 'antd';
import {
  fpost,
  fpostArray
} from '../../common/io.js';
import {
  getUnicodeParam
} from '../../common/g.js';
let classId = getUnicodeParam('classId');

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }
  render() {
    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile,
      onDelete
    } = this.props;

    const columns = [{
      title: '学员姓名(学号)',
      dataIndex: 'studentName',
      width: '200px',
      render: function(value, row, index) {
        let {
          studentName,
          stuNo
        } = row;
        return (<div>
            {`${studentName}` }
            {
              stuNo ? <div>({stuNo})</div> : null
            }
          </div>);
      }
    }, {
      title: '家长手机号',
      dataIndex: 'phone',
      width: '150px'
    }, {
      title: '报名时间',
      dataIndex: 'create_time',
      width: '200px'
    }, {
      title: '学费(¥)',
      dataIndex: 'receivableAmount',
      width: '100px'
    }, {
      title: '欠费(¥)',
      dataIndex: 'arrearAmount',
      width: '100px'
    }, {
      title: '剩余课次',
      dataIndex: 'remainPeriod',
      width: '100px'
    }, {
      title: '剩余学费',
      dataIndex: 'remainAmountStr',
      width: '100px'
    }, {
      title: '操作',
      dataIndex: 'des',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          studentId
        } = row;

        return (<a href={`/front/rollClassSignup?studentId=${studentId}&classId=${classId}`}>续报</a>);
      }
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">在读学员列表</span>
          <a 
            className="f-btn-green f-right"
            target="_blank"
            onClick={exportFile}
          >
            <Icon className="f-fz7 f-vertical-middle f-bold" />导出学员
          </a>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            pagination={ pagination }
            loading={loading}
            bordered
				  />
			</div>
    );
  }
}