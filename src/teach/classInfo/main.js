import React, {
	Component
} from 'react';
import store from 'store2';
import querystring from 'querystring';
import {
	Breadcrumb,
	message,
	Row,
	Col
} from 'antd';
import {
	default as MyList
} from './list.js';
import {
	fpost,
	host
} from '../../common/io.js';
import {
	numberFormate,
	getUnicodeParam
} from '../../common/g.js';
import '../main.less';

const classId = getUnicodeParam('classId');
let sessionId = store.get('sessionId');

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			studentList: [],
			classInfo: {},
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {
				classId: classId
			},
			classIds: []
		}
	}
	render() {
		let {
			studentList,
			classInfo,
			loading,
			pagination
		} = this.state;

		let {
			ruleStrList
		}= classInfo;
		// ruleStrList = ruleStrList || [];

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
			    <Breadcrumb.Item>滚动班管理</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">班级详情</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
				<Row gutter={40}>
					<Col span="8" className="f-mb2">
						<span className="f-mr3">班级名称:</span>{classInfo.name}
					</Col>
					<Col span="8" className="f-mb2">
					<span className="f-mr3">课程:</span>{classInfo.courseName}
					</Col>
					<Col span="8" className="f-mb2">
					<span className="f-mr3">校区:</span>{classInfo.schoolAreaName}
					</Col>
					<Col span="8" className="f-mb2">
					<span className="f-mr3">上课老师:</span>{classInfo.teacherName}
					</Col>
					<Col span="8" className="f-mb2">
					<span className="f-mr3">教室:</span>{classInfo.roomName}
					</Col>
					<Col span="8" className="f-mb2">
					<span className="f-mr3">班级状态:</span>{classInfo.stateStr}
					</Col>
					<Col span="8" className="f-mb2 flex">
						<Row gutter={0}>
							<Col span={5}>上课时间:</Col>
							<Col span={19}>{
								(ruleStrList || []).map((d,i)=> {
									return(<p key={i}>{d}</p>);
								})
							}</Col>
						</Row>
					</Col>
					<Col span="8" className="f-mb2">
					<span className="f-mr3">总课次:</span>{classInfo.courseTimes}
					</Col>
					<Col span="8" className="f-mb2">
					<span className="f-mr3">学费标准:</span>{classInfo.salePrice}
					</Col>
				</Row>
			</header>

			<MyList 
				data={studentList}
		        loading={loading}
		        onChange={this._getList.bind(this)}
		        pagination={pagination}
		        exportFile = { this._exportFile.bind(this) }
			/>

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			searchParma
		} = this.state;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		searchParma.pageSize = pageSize;
		searchParma.currentPage = currentPage;

		fpost('/api/educational/findClassAndCurrentStudents', searchParma)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let total = Number(result.total);
				let studentList = result.studentList;
				studentList.map((d, i) => d.key = i);

				this.setState({
					...result,
					pagination: {
						...pagination,
						total: total,
						current: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				});
			})
			.catch((err) => {
				this.setState({
					loading: false
				});
			});
	}
	_exportFile() {
		let ids = [classId];

		let url = `${host}/api/educational/exportClassAndCurrentStudents?x-auth-token=${sessionId}&classIds=${ids}`;
		window.location.href = url;
	}
}