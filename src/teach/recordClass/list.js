import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message,
  Progress,
  Modal
} from 'antd';
import {
  fpost,
  fpostArray
} from '../../common/io.js';

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: false,
      saveing: false,
      rulesList: []
    }
  }
  render() {
    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    let {
      visibleModal,
      saveing
    } = this.state;

    const columns = [{
      title: '学员姓名（学号）',
      dataIndex: 'studentName',
      width: '150px',
      render: (value, row, index) => {
        let {
          studentName,
          stuNo
        } = row;
        return <div
          title={`${studentName}(${stuNo})`}
        >
        { studentName }{ stuNo ? <span>({stuNo})</span> : null }
        </div>
      }
    }, {
      title: '校区名称',
      dataIndex: 'schoolAreaName',
      width: '150px'
    }, {
      title: '班级名称',
      dataIndex: 'className',
      width: '150px'
    }, {
      title: '课程',
      dataIndex: 'courseName',
      width: '150px'
    }, {
      title: '上课时间',
      dataIndex: 'signUpDate',
      width: '200px',
      render:(value,row ,index)=> {
        let {
          signUpDate,
          startTime,
          endTime
        } = row;
        return(<div>
          <span className="f-mr1">{signUpDate} </span>
          {startTime} -
          {endTime}
        </div>);
      }
    }, {
      title: '上课老师',
      dataIndex: 'teacherName',
      width: '100px'
    },{
      title: '教室',
      dataIndex: 'roomName',
      width: '100px'
    },{
      title: '课次消耗',
      className:'f-align-center',
      dataIndex: 'consumeCoursePeriod',
      width: '100px'
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue">上课记录</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 1100}}
            pagination={ pagination }
            loading={loading}
            bordered
				  />
			</div>
    );
  }
}