import React, {
	Component
} from 'react';
import {
	default as Layout
} from '../common/layout.js';
import './main.less';
import {
	Route
} from 'react-router-dom';
//使用fetch请求接口数据
//教务教学
//班级管理
import {
	default as ClassList
} from './classList/main.js';
import {
	default as ClassInfo
} from './classInfo/main.js';
import {
	default as ClassTimes
} from './classTimes/main.js';
import {
	default as TeacherTimes
} from './teacherTimes/main.js';
import {
	default as RoomTimes
} from './roomTimes/main.js';
import {
	default as DateTimes
} from './dateTimes/main.js';
import {
	ViewDetails
} from './classManage/viewDetails.js';
import {
	default as CreateClass
} from './createClass/main.js';
import {
	ClassMember
} from './classManage/classMember.js';
//上课记录
import {
	CheckRecord
} from './classRecord/checkRecord.js';
import {
	RecordAttend
} from './classRecord/recordAttend.js';
import {
	ClassRecord
} from './classRecord/classRecord.js';
import {
	ModifyRecord
} from './classRecord/modifyRecord.js';
import {
	RemedialTeach
} from './classRecord/remedialTeach.js';
import {
	RemedialRecord
} from './classRecord/remedialRecord.js';
//教务
import {
	TeacherCurriculum
} from './educationalAdmin/teacherCurriculum.js';
import {
	ClassroomTime
} from './educationalAdmin/classroomTime.js';
import {
	PointListDetail
} from './educationalAdmin/pointListDetail.js';
import {
	PointList
} from './educationalAdmin/pointList.js';
import {
	CourseDetail
} from './educationalAdmin/courseDetail.js';
import {
	ClassroomDetail
} from './educationalAdmin/classroomDetail.js';
import {
	HourStatistics
} from './educationalAdmin/hourStatistics.js';
import {
	StatisticsDetail
} from './educationalAdmin/statisticsDetail.js';
import {
	default as MissedLessonList
} from './classRecord/missedLessonList/main.js';
import {
	default as MarkClass
} from './markClass/main.js';//记上课
import {
	default as RecordClass
} from './recordClass/main.js';//上课记录
export default ({
	match
}) => {
	window.store.session('tMenu', {
		selectedKeys: 'jwjx'
	})
	//	定义左边菜单名称
	return (<div>
		<Layout main={ Main }/>
	</div>);
}

class Main extends Component {
	render() {
		return (
			<div>
				<Route path='/teach' exact component={ ClassList } />
				{/*班级列表*/}
				<Route path='/teach/classList' component={ ClassList } />
				{/*查看排课明细*/}
				<Route path='/teach/viewDetails' component={ ViewDetails } />
				{/*新建班级*/}
				<Route path='/teach/createClass' component={ CreateClass } />
				<Route path='/teach/editClass' component={ CreateClass } />
				{/*班级成员*/}
				<Route path='/teach/classMember' component={ ClassMember } />
				{/*班级详情*/}
				<Route path='/teach/classInfo' component={ ClassInfo } />
				{/*班级课表*/}
				<Route path='/teach/classTimes' component={ ClassTimes } />
				{/*教室课表*/}
				<Route path='/teach/roomTimes' component={ RoomTimes } />
				{/*老师课表*/}
				<Route path='/teach/teacherTimes' component={ TeacherTimes } />
				{/*日期课表*/}
				<Route path='/teach/dateTimes' component={ DateTimes } />
				

				{/*查看上课记录*/}
				<Route path='/teach/checkRecord' component={ CheckRecord } /> 
				{/*修改上课记录*/}
				<Route path='/teach/modifyRecord' component={ ModifyRecord } /> 
				{/*记上课*/}
				<Route path='/teach/recordAttend' component={ RecordAttend } /> 
				{/*记上课*/}
				<Route path='/teach/markClass' component={ MarkClass } /> 
				{/*上课记录*/}
				<Route path='/teach/classRecord' component={ RecordClass } /> 
				{/*记补课*/}
				<Route path='/teach/remedialTeach' component={ RemedialTeach } /> 
				{/*补课记录*/}
				<Route path='/teach/remedialRecord' component={RemedialRecord } /> 
				
				{/*教师课程信息*/}
				<Route path='/teach/teacherCurriculum' component={ TeacherCurriculum } /> 
				{/*教室时段*/}
				<Route path='/teach/classroomTime' component={ ClassroomTime } /> 
				{/*点名表*/}
				<Route path='/teach/pointList' component={ PointList } /> 
				{/*点名表详情*/}
				<Route path='/teach/pointListDetail' component={ PointListDetail } /> 
				{/*教师课程详情*/}
				<Route path='/teach/courseDetail' component={ CourseDetail } /> 
				{/*教室时段课程详情*/}
				<Route path='/teach/classroomDetail' component={ ClassroomDetail } /> 
				{/*教师课时统计*/}
				<Route path='/teach/hourStatistics' component={ HourStatistics } /> 
				{/*教师课时统计列表详情*/}
				<Route path='/teach/statisticsDetail' component={ StatisticsDetail } />
				{/*补课列表*/}
				<Route path='/teach/missedLessonList' component={ MissedLessonList } /> 
			</div>
		);
	}
}