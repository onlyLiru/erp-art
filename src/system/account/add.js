import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Input,
	Col,
	Row,
	Modal,
	Form,
	DatePicker,
	Select,
	Radio,
	Switch,
	message,
	InputNumber
} from 'antd';
import {
	fpost
} from '../../common/io.js';
import SelectSchool from '../../common/selectSchool.js';
import SelectRoleGroups from '../../common/selectRoleGroups.js';
import TreeSelectComponent from '../../common/treeSelect.js';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import {
	undefinedToEmpty
} from '../../common/g.js';
import RangeDate from '../../common/rangeDate.js';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			code: store.get('userInfo') && store.get('userInfo').institution && store.get('userInfo').institution.code, //集团代码
			saveing: false,
			isAllArea: '1', //是否全部校区1是0否
			schoolAreaIds: [],
			isExternal: '0', //是否外部人员1是0否
			isDisabled: true,
			openLearningCenter: false,
			rangeDate: null,
			rangeTime: null,
			groups: [], //所有权限组
			authMenuStr: [], //默认权限,
			staffId: '',
			roleId: [],
			treeEdited: false, //看看树形数据有没有操作过，如果没有则传allMenuIds
			allMenuIds: '',
		}
	}
	render() {
		const {
			getFieldDecorator,
			getFieldValue
		} = this.props.form;
		let {
			visible,
			onOk,
			onCancel,
			defaultData
		} = this.props;

		const formItemLayout = {
			labelCol: {
				span: 7
			},
			wrapperCol: {
				span: 15
			}
		};

		let {
			code,
			saveing,
			isAllArea,
			schoolAreaIds,
			isExternal,
			rangeDate,
			rangeTime,
			groups,
			authMenuStr,
			isDisabled,
			openLearningCenter,
			staffId,
			roleId
		} = this.state;
		let modalTitle = defaultData ? '编辑' : '新增';

		return (<Modal title={`${modalTitle}账号`}
				width='88%'
				visible={ visible }
				confirmLoading={ saveing }
				onOk={ this._ok.bind(this) }
				onCancel={ onCancel }
			>
				<Form>
					<Row rutter={24} type="flex">
						<Col span={12}>
							{
								defaultData && this.state.isExternal ?
									<FormItem
										label="用户来源"
										{...formItemLayout}
									>
										{
											isExternal == '1' ? '外部新建' : '从人事管理中选择'
										}
									</FormItem>
								: 
									<FormItem
										label="用户来源"
										{...formItemLayout}
									>
										{
											getFieldDecorator('isExternal', {
												rules: [{ 
													required: true, 
													message: '请选择用户来源' 
												}],
												initialValue:isExternal
											})(
												<RadioGroup
													disabled={true}
													onChange= {
														(e)=> {
															let v =e.target.value;
															this.setState({
																isExternal:v
															});
														}
													}
												>
											        <Radio value='1'>外部新建</Radio>
											        <Radio value='0'>从人事管理中选择</Radio>
											    </RadioGroup>
											)
										}
									</FormItem>
							}
							{
								isExternal == '0' ?
									<FormItem
										label="选择用户"
										{...formItemLayout}
									>
										{
											getFieldDecorator('staffId', {
												rules: [{  
													required:true,
													message:'请选择用户'
												}],
												initialValue:staffId
											})(
												<SelectPerson 
													onSelect={ 
														(v)=> {
															v = v ? v : null;
															this.props.form.setFieldsValue({
																staffId: v
															});
														} 
													}
													initialValueData={ staffId }
												/>
											)
										}
									</FormItem>
								: 
									<section>
										<FormItem
											label="姓名"
											{...formItemLayout}
										>
											{
												getFieldDecorator('name', {
													rules: [{ 
														required: true, 
														message: '请输入姓名' 
													}],
												})(
													<Input placeholder="请输入姓名" />
												)
											}
										</FormItem>

										<FormItem
											label="手机号"
											{...formItemLayout}
										>
											{
												getFieldDecorator('phone', {
													rules: [{ 
														required: true, 
														message: '请输入手机号',
														pattern: /^1[0-9]\d{4,9}$/
													}],
												})(
													<Input placeholder="请输入手机号" />
												)
											}
										</FormItem>

										<FormItem
											label="邮箱"
											{...formItemLayout}
										>
											{
												getFieldDecorator('email', {
													rules: [{ 
														required: false,
														message: '请输入邮箱' 
													},{
														type:'email',
														message: '请输入正确的邮箱格式' 
													}],
												})(
													<Input placeholder="请输入邮箱" />
												)
											}
										</FormItem>
									</section>
							}

							<FormItem
								label="用户名"
								{...formItemLayout}
							>
								{
									getFieldDecorator('loginName', {
										rules: [{ 
											required: true, 
											message: '请输入用户名' 
										}],
									})(
										<Input placeholder="请输入用户名" addonAfter={`@${code}`} />
									)
								}
							</FormItem>
							<FormItem
								label="选择校区"
								{...formItemLayout}
							>
								{
									getFieldDecorator('isAllArea', {
										rules: [{ 
											required: true, 
											message: '请选择校区' 
										}],
										initialValue:isAllArea
									})(
										<RadioGroup
											onChange= {
												(e)=> {
													let v =e.target.value;
													this.setState({
														isAllArea:v
													});
												}
											}
										>
									        <Radio value='1'>全部校区</Radio>
									        <Radio value='0'>指定校区</Radio>
									    </RadioGroup>
									)
								}
							</FormItem>
							{
								isAllArea == '0' ?
									<FormItem
										label="指定校区"
										{...formItemLayout}
									>
										{
											getFieldDecorator('schoolAreaIds', { 
												rules: [{
													required: true, 
													message: '请选择校区',
												}],
												initialValue: schoolAreaIds
											})
											(
												<SelectSchool 
													onSelect={
														(v) => {
															this.props.form.setFieldsValue({
																schoolAreaIds:v
															});
														}
													}
													width='100%' 
													mode="multiple"
													url="/api/system/schoolarea/listSchoolArea"
													initialValueData={schoolAreaIds}
												/>
											)
										}
									</FormItem>
								: null
							}

							<Row>
								<Col span={14}>
									<FormItem
										label="登陆时限"
										labelCol={
											{ span:12 }
										}
										wrapperCol={
											{span:12}
										}
									>
										{
											getFieldDecorator('loginTimeStart', {
												rules: [{ 
													required: true, 
													message: '请填写登陆时限' 
												}],
											})(
												<InputNumber 
													sytle={{
														widht:'100%'
													}}
													placeholder="开始"
													min={0} max={24} 
												/>
											)
										}
									</FormItem>
								</Col>
								<Col span={10} style={{position:'relative',left:'-50px'}}>
									<FormItem
										{...formItemLayout}
									>	
										{
											getFieldDecorator('loginTimeEnd', {
												rules: [{ 
													required: true, 
													message: '请填写结束时限' 
												}],
											})(
												<InputNumber 
													placeholder="结束"
													min={0} max={24} 
												/>
											)
										}
									</FormItem>
								</Col>
							</Row>

							<FormItem
								label="启用"
								{...formItemLayout}
							>
								{
									getFieldDecorator('isDisabled', {
										rules: [{ 
											required: true, 
											message: '是否启用' 
										}],
										valuePropName: 'checked',
										initialValue:isDisabled
									})(
										<Switch
											checkedChildren="启用" 
											unCheckedChildren="禁用" 
										/>
									)
								}
							</FormItem>
						</Col>
						<Col span={12}>
							<FormItem
								label="有效日期"
								{...formItemLayout}
							>
								{
									getFieldDecorator('rangeDate',{
										rules: [{ 
											required: true, 
											message: '请选择有效日期' 
										}],
										initialValue:rangeDate
									})(
										<RangeDate 
											onSelect={
												(d)=> {
													this.props.form.setFieldsValue({
														rangeDate:d,
														loginPeriodStart:d.startString,
														loginPeriodEnd:d.endString,
													});
												}
											}
											initialValueData={ rangeDate }
											showTime={true}
										/>
									)
								}
							</FormItem>

							<span className="f-hide">
						        {
									getFieldDecorator('loginPeriodStart',{
										initialValue:''
									})(
										<Input />
									)
								}
								{
									getFieldDecorator('loginPeriodEnd',{
										initialValue:''
									})(
										<Input />
									)
								}
							</span>
							<FormItem
								label="权限组"
								{ ...formItemLayout }
							>
								{
									getFieldDecorator('roleId', {
										rules: [{ 
											required: true, 
											message: '请选择权限组' 
										}],
										initialValue:roleId
									})(
										<SelectRoleGroups 
											onSelect={
												(v) => {
													this.props.form.setFieldsValue({
														roleId:v
													});

													this._onChangeGroup(v);
												}
											}
											width="100%"
											initialValueData={roleId}
										/>
									)
								}
							</FormItem>

							{
								authMenuStr && authMenuStr.length ?
									<FormItem
										label="编辑权限"
										{ ...formItemLayout }
									>
										{
											getFieldDecorator('authMenuStr', {
												rules: [{ 
													required: true, 
													message: '请选择权限' 
												}],
												initialValue: authMenuStr
											})(<div style={{maxHeight:'520px',overflow:'scroll'}}>
												<TreeSelectComponent 
													onSelect={
														(d)=> {
															this.props.form.setFieldsValue({
																authMenuStr:d
															});
															this.setState({
																treeEdited:true
															});
														}
													}
													initialValueData={ authMenuStr }
												/>
												</div>
											)
										}
									</FormItem>
								: null
							}
						</Col>
					</Row>
					{
						defaultData ?
							<span className="f-hide">
								{
									getFieldDecorator('id')
									(
										<Input />
									)
								}
							</span>
						: null
					}
		      	</Form>
		</Modal>);
	}
	componentDidMount() {
		let {
			defaultData
		} = this.props;

		console.log(defaultData);

		if (defaultData) { //如果是编辑，设置默认值
			let {
				id,
				isExternal,
				name,
				phone,
				email,
				loginName,
				isAllArea,
				schoolAreaIds,
				loginPeriodStart,
				loginPeriodEnd,
				loginTimeStart,
				loginTimeEnd,
				isDisabled,
				openLearningCenter,
				roleId,
				menuIds,
				allMenuIds,
				staffId
			} = defaultData;
			loginName = loginName.split('@')[0];
			isDisabled = isDisabled == '0' ? true : false;
			schoolAreaIds = schoolAreaIds.split(',');
			menuIds = menuIds && menuIds.length ? menuIds.split(',') : [];

			this.setState({
				isExternal,
				isAllArea,
				isDisabled,
				schoolAreaIds,
				staffId,
				roleId,
				allMenuIds,
				authMenuStr: menuIds,
				rangeDate: {
					startTime: loginPeriodStart,
					endTime: loginPeriodEnd
				}
			});

			this.props.form.setFieldsValue({
				id,
				name,
				phone,
				email,
				loginName,
				loginPeriodStart,
				loginPeriodEnd,
				loginTimeStart,
				loginTimeEnd,
			});
		}

	}
	_ok(e) {
		let {
			treeEdited,
			allMenuIds,
			code
		} = this.state;

		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			// console.log(values);
			if (!err) {
				console.log(values);
				let isDisabled = values.isDisabled == true ? '0' : '1';
				values = {
					...values,
					isDisabled: isDisabled
				}
				if (!treeEdited) { //如果没有编辑过就传后台默认给的带有父id的数据字段，不然后台保存有误
					values.authMenuStr = allMenuIds;
				}
				values.loginName = values.loginName + '@' + code;

				values = undefinedToEmpty(values);

				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;

		fpost('/api/system/user/saveUser', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onSaveOk();
			})
			.catch((err) => {
				console.log(err);
				this.setState({
					saveing: false
				});
			});
	}
	_onChangeGroup(id = '') {
		this.setState({
			authMenuStr: []
		});
		if (!id) {
			return;
		}
		fpost('/api/system/auth/findRoleById', {
				id: id
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				let {
					menuIds,
					allMenuIds
				} = res.result;
				if (menuIds && menuIds.length) {
					menuIds = menuIds.split(',');
				} else {
					menuIds = [];
				};
				this.setState({
					authMenuStr: menuIds,
					allMenuIds
				}, () => {
					this.props.form.setFieldsValue({
						authMenuStr: allMenuIds
					});
					console.log(this.props.form.getFieldValue('authMenuStr'));
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;