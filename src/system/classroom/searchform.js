import React, {
	Component
} from 'react';
import {
	Button,
	Input,
	Form
} from 'antd';
import SelectSchool from '../../common/selectSchool.js';

const FormItem = Form.Item;

class MainForm extends Component {
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			onSearch
		} = this.props;

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form layout="inline">
				<FormItem>
					{
						getFieldDecorator('name')
						(
							<Input 
								size="large"
								placeholder="请输入教室名称"
								style={{width:'350px'}}
							/>
						)
					}
				</FormItem>
				<FormItem>
					{
						getFieldDecorator('schoolAreaId')
						(
							<SelectSchool
								url="/api/system/schoolarea/listSchoolArea" 
								onSelect={ 
									(v)=> {
										this.setState({
											schoolAreaId: v
										});
										this.props.form.setFieldsValue({
											schoolAreaId:v
										});
									} 
								} 
							/>
						)
					}
				</FormItem>
				<FormItem>
					<Button 
						type="primary" 
						size="large" 
						icon="search"
						onClick={
							onSearch
						}
					>搜索</Button>
				</FormItem>
	      	</Form>
		</header>);
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;