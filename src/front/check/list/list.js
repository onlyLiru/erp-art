import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../../common/io.js';
import {
  getUnicodeParam
} from '../../../common/g.js';
let stage = getUnicodeParam('stage') || '1';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">审批列表</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 2500}}
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
				  />
			</div>
    );
  }

}

const columns = [{
  title: '学员姓名（学号）',
  dataIndex: 'studentName',
  width: '150px',
  className: 'f-align-center',
  fixed: 'left',
  render: (value, row, index) => {
    let {
      studentName,
      stuNo
    } = row;
    return <a href={`/front/studenthome?studentId=${row.studentId}`}
      className="f-line1 f-over-hide"
      style={{
        width:'150px'
      }}
      title={`${studentName}(${stuNo})`}
    >
  		{ studentName }({stuNo})
  	</a>
  }
}, {
  title: '家长手机号',
  dataIndex: 'phone',
  width: '150px'
}, {
  title: '校区',
  dataIndex: 'schoolAreaName',
  width: '200px'
}, {
  title: '班级',
  dataIndex: 'className',
  width: '200px'
}, {
  title: '退款方式',
  dataIndex: 'tradeTypeStr',
  width: '150px'
}, {
  title: '金额(¥)',
  dataIndex: 'amount',
  className: 'f-align-center',
  width: '150px'
}, {
  title: '状态',
  dataIndex: 'stateStr',
  width: '100px'
}, {
  title: '申请人',
  dataIndex: 'applyUserName',
  width: '100px'
}, {
  title: '申请时间',
  dataIndex: 'applyTime',
  width: '200px'
}, {
  title: '一级审批状态',
  dataIndex: 'firstStageStateStr',
  width: '150px'
}, {
  title: '一级审批人',
  dataIndex: 'firstApproveUserName',
  width: '150px'
}, {
  title: '一级审批时间',
  dataIndex: 'firstApproveTime',
  width: '200px'
}, {
  title: '二级审批状态',
  dataIndex: 'secondStageStateStr',
  width: '150px'
}, {
  title: '二级审批人',
  dataIndex: 'secondApproveUserName',
  width: '150px'
}, {
  title: '二级审批时间',
  dataIndex: 'secondApproveTime',
  width: '200px'
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  width: '100px',
  className: "f-align-center",
  render: (value, row, index) => {
    let {
      id,
      firstStageState,
      secondStageState,
      type //1:退课审批,2:转班审批,3:提现审批
    } = row;
    /*
      1.二级审批列表页查询时，一级审批状态这个参数一定要传1 
      2.一级审批列表页查询时，返回的一级审批状态(firstStageState)为0时操作展示审批字样，否则展示查看，二级审批列表页查询时，返回的二级审批状态(secondStageState)为0时操作展示审批字样，否则展示查看
    */
    let stageState = stage == '1' ? firstStageState : secondStageState;
    let viewText=stageState == "0" ? "审批" : "查看";
    
    let Html = ()=> <a href={`/front/check/detail?id=${id}&stage=${stage}&type=${type}&stageState=${stageState}`}>{viewText}</a>
    return (<Html />);
  }
}];