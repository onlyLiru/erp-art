import React, {
	Component
} from 'react';
import store from 'store2';
import querystring from 'querystring';
import {
	Breadcrumb,
	message
} from 'antd';
import {
	default as MyList
} from './list.js';
import {
	default as SearchForm
} from './searchform.js';
import {
	fpost,
	host
} from '../../../common/io.js';
import {
	numberFormate,
	getUnicodeParam
} from '../../../common/g.js';

let sessionId = store.get('sessionId');
let stage = getUnicodeParam('stage') || '1';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			result: null,
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {
				type:'1',
				firstStageState:stage == '2' ? '1' : ''
			}
		}
	}
	render() {
		let {
			result,
			loading,
			pagination
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>审批</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">{ stage =='1' ? '一' : '二' }级审批</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<SearchForm
				ref={ form => this.searchForm = form }
				onSearch= { this._search.bind(this) }
			/>

			<MyList 
				data={result}
		        loading={loading}
		        onChange={this._getList.bind(this)}
		        pagination={pagination}
		        exportFile = { this._exportFile.bind(this) }
			/>

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_search(values) { //搜索
		console.log(values);
		if(stage == '1') {
			values ={
				...values,
				firstStageState:values.stageState
			}
		}else {
			/*一级审批状态，-1:已否决 0:待审批 1:已通过，二级审批查询时，这个参数一定要传1*/
			values ={
				...values,
				firstStageState:'1',
				secondStageState:values.stageState
			}
		}
		this.setState({
			searchParma: {
				...this.state.searchParma,
				...values
			},
			currentPage: 1,
			pagination: {
				...this.state.pagination,
				current: 1,
			}
		}, () => {
			this._getList();
		});
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			searchParma
		} = this.state;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		searchParma.pageSize = pageSize;
		searchParma.currentPage = currentPage;

		console.log('pager', pager);

		fpost('/api/reception/pageApproves', searchParma)
			.then((res) => {
				console.log(res);
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result || !res.result.records) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records;
				data.forEach((d) => {
					/*金额千分位*/
					d.amount = numberFormate(d.amount)
					d.key = d.id
				});

				let total = Number(result.total);

				this.setState({
					result: data,
					pagination: {
						...pagination,
						total: total,
						current: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				}, () => {
					// console.log(this.state)
				});
			})
			.catch((err) => {
				console.log(err);

				this.setState({
					loading: false
				});
			});
	}
	_exportFile() {
		let {
			searchParma
		} = this.state;
		searchParma = querystring.stringify(searchParma);
		let url = `${host}/api/reception/student/exportStudentExcel?x-auth-token=${sessionId}&${searchParma}`;
		window.location.href = url;
	}
}