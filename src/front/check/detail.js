import React,{Component} from 'react';
import {
	Breadcrumb,
	message,
	Row,
	Col,
	Input,
	Button
} from 'antd';
import {
	fpost
} from '../../common/io.js';
import {
	numberFormate,
	getUnicodeParam
} from '../../common/g.js';

let approveId = getUnicodeParam('id');
let displayStage = getUnicodeParam('stage');
let stageState = getUnicodeParam('stageState');
let type = getUnicodeParam('type'); //1:退课审批,2:转班审批,3:提现审批
const { TextArea } = Input;

export default class Main extends Component{
	constructor(props) {
		super(props);
		this.state={
			d:null
		}
		this._input=this._input.bind(this);
	}
	render() {
		let {
			d
		} = this.state;
		if(!d) { return null}
		return <div>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>审批</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">审批详情</span>
			    </Breadcrumb.Item>
			</Breadcrumb>
			<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
				{
					type == '1' ?
					<div>
						<h3 className="f-mb3">退课信息</h3>
						<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
							<Row gutter={24}>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">审批类型:</span> 
									{d.typeStr}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">学员姓名:</span> 
									{d.studentName} { d.stuNo || '' } 
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">家长手机号:</span> {d.phone}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">校区:</span> {d.schoolAreaName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">班级:</span> {d.className}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">原价:</span> ¥ {d.originalPrice}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">优惠:</span> ¥ {d.preferentialAmount}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">课程顾问:</span> {d.salesName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">欠费:</span>¥ {d.arrearAmount}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">剩余学费:</span> ¥ {d.remainReceivableAmount}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">剩余课次:</span>{d.remainCourseTimes}
								</Col>
							</Row>
						</header>
						<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
							<Row gutter={24}>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">退款方式:</span>{d.tradeTypeStr}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">退款金额:</span> ¥ {d.amount}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">退款账号:</span> {d.accountInfo}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">退款原因:</span> {d.reason}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">申请人:</span> {d.applyUserName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">申请时间:</span> {d.applyTime}
								</Col>
							</Row>
						</header>
					</div>
					: null
				}
				{
					type == '2' ?
					<div>
						<h3 className="f-mb3">转班信息</h3>
						<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
							<Row gutter={24}>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">审批类型:</span> 
									{d.typeStr}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">学员姓名:</span> 
									{d.studentName} { d.stuNo || '' } 
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">家长手机号:</span> {d.phone}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">转出校区:</span> {d.schoolAreaName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">转出班级:</span> {d.className}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">原价:</span> ¥ {d.originalPrice}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">优惠:</span> ¥ {d.preferentialAmount}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">课程顾问:</span> {d.salesName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">欠费:</span>¥ {d.arrearAmount}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">剩余学费:</span> ¥ {d.remainReceivableAmount}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">剩余课次:</span>{d.remainCourseTimes}
								</Col>
							</Row>
						</header>
						<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
							<Row gutter={24}>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">转入校区:</span> {d.transferInSchoolAreaName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">转入班级:</span> {d.transferInClassName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">课次单价:</span> ¥ {d.unitPrice}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">转入课次:</span> {d.transferInTimes}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">课程顾问:</span> {d.transferInSalesName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">转班应退:</span>¥ {d.resultAmount}
								</Col>
							</Row>
						</header>
						<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
							<Row gutter={24}>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">退款方式:</span>{d.tradeTypeStr}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">退款金额:</span> ¥ {d.amount}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">退款账号:</span> {d.accountInfo}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">退款原因:</span> {d.reason}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">申请人:</span> {d.applyUserName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">申请时间:</span> {d.applyTime}
								</Col>
							</Row>
						</header>
					</div>
					: null
				}
				{
					type == '3' ?
					<div>
						<h3 className="f-mb3">提现信息</h3>
						<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
							<Row gutter={24}>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">审批类型:</span> 
									{d.typeStr}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">学员姓名:</span> 
									{d.studentName} { d.stuNo || '' } 
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">家长手机号:</span> {d.phone}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">经办校区:</span> {d.schoolAreaName}
								</Col>
							</Row>
						</header>
						<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
							<Row gutter={24}>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">提现方式:</span>{d.tradeTypeStr}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">提现金额:</span> ¥ {d.amount}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">提现账号:</span> {d.accountInfo}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">提现原因:</span> {d.reason}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">申请人:</span> {d.applyUserName}
								</Col>
								<Col span={8} className="f-mb2">
									<span className="f-bold f-mr2">申请时间:</span> {d.applyTime}
								</Col>
							</Row>
						</header>
					</div>
					: null
				}
			</header>
			<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
				<h3 className="f-mb3">一级审批:</h3>
				{
					d.detailList[0].approveState == '0' ?
						<Row gutter={24}>
							<Col span={24}  className="f-mb5">
								<Row>
									<Col span={2}>审批意见:</Col>
									<Col span={8}>
										<TextArea onChange={
											(e)=> this._input('approveNote',e.target.value)
										} rows={4} />
									</Col>
								</Row>
							</Col>
							<Col span={24}>
								<Button 
									size="large" 
									type="primary" 
									className="f-mr2"
									onClick={
										()=> this._check('1')
									}
								>通过</Button>
								<Button 
									size="large" 
									type="danger" 
									className="f-mr2"
									onClick={
										()=> this._check('-1')
									}
								>否决</Button>
								<Button size="large" onClick={()=> window.history.back()}>返回</Button>
							</Col>
						</Row>
					: 
						<Row gutter={24}>
							<Col span={8} className="f-mb2">审批状态: {d.detailList[0].approveStateStr}</Col>
							<Col span={8} className="f-mb2">审批意见: {d.detailList[0].approveNote}</Col>
							<Col span={8} className="f-mb2">审批人: {d.detailList[0].approveUserName}</Col>
							<Col span={8} className="f-mb2">审批时间: {d.detailList[0].approveTime}</Col>
						</Row>
				}
			</header>
			{
				displayStage == '2' ?
					<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb3">
						<h3 className="f-mb3">二级审批:</h3>
						{
							d.detailList[1].approveState == '0' ?
								<Row gutter={24}>
									<Col span={24}  className="f-mb5">
										<Row>
											<Col span={2}>审批意见:</Col>
											<Col span={8}>
												<TextArea onChange={
													(e)=> this._input('approveNote',e.target.value)
												} rows={4} />
											</Col>
										</Row>
									</Col>
									<Col span={24}>
										<Button 
											size="large" 
											type="primary" 
											className="f-mr2"
											onClick={
												()=> this._check('1')
											}
										>通过</Button>
										<Button 
											size="large" 
											type="danger" 
											className="f-mr2"
											onClick={
												()=> this._check('-1')
											}
										>否决</Button>
										<Button size="large" onClick={()=> window.history.back()}>返回</Button>
									</Col>
								</Row>
							: 
								<Row gutter={24}>
									<Col span={8} className="f-mb2">审批状态: {d.detailList[1].approveStateStr}</Col>
									<Col span={8} className="f-mb2">审批意见: {d.detailList[1].approveNote}</Col>
									<Col span={8} className="f-mb2">审批人: {d.detailList[1].approveUserName}</Col>
									<Col span={8} className="f-mb2">审批时间: {d.detailList[1].approveTime}</Col>
								</Row>
						}
					</header>
				: null
			}
			{
				stageState != '0' ?
					<Button size="large" onClick={()=> window.history.back()}>返回</Button>
				: null
			}
		</div>;
	}
	componentDidMount() {
		this._getData();
	}
	_input(k,v) {
		this.setState({
			[k]:v
		});
	}
	_check(approveState) {
		let {
			approveNote,

		} = this.state;
		let param={
			approveState,
			approveNote,
			approveId,
			stage:displayStage
		}
		if(!approveNote) { 
			message.error('请输入审批意见'); 
			return; 
		}
		fpost('/api/reception/operateApprove',param)
		.then(res=> res.json())
		.then((res) => {
			if (!res.success) {
				message.error(res.message || '系统错误');
				throw new Error(res.message || '系统错误');
			};
			return (res);
		})
		.then((res) => {
			window.location.href=`/front/check/list?stage=${displayStage}`;
			// if(displayStage=='1') {
			// 	window.location.href=`/front/check/detail?id=${approveId}&type=${type}&stage=2`;
			// 	window.location.href=`/front/check/list?stage=${displayStage}`;
			// }else {
			// 	this._getData();
			// }
		})
		.catch((err) => {
			console.log(err);
		});
	}
	_getData() {
		let param={
			approveId,
			displayStage
		};

		fpost('/api/reception/findApproveFullInfo', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					d:res.result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
}