import React,{Component } from 'react';
import {
	Route
} from 'react-router-dom';
import {
	default as List
} from './list/main';
import {
	default as Detail
} from './detail';

let PARENTPATH;

export default class Main extends Component{
	constructor(props) {
		super(props);
		const {
			match
		} = props;
		PARENTPATH = match.path;
		this.state={

		}
	}
	render() {
		return (<div>
			<Route path={`${PARENTPATH}/list`} component ={List} />
			<Route path={`${PARENTPATH}/detail`} component ={Detail} />
		</div>);
	}
}