import React, {
	Component
} from 'react';
import {
	Input,
	InputNumber,
	Button,
	Breadcrumb,
	Modal,
	Form,
	Select,
	message
} from 'antd';

import {
	getUrlParam
} from '../../common/g.js';
import {
	fpost
} from '../../common/io.js';
import {
	payTypes
} from '../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;
const {
	TextArea
} = Input;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false,
			visible: false,
			orderInfoId: getUrlParam('id'),
			returnType: 2,
			orderDetailId: '',

			receiver: '',
			receivePhone: '',
			buyTime: '',
			salesName: '',
			schoolAreaName: '',
			totalAmount: '',
			orderDetails: null,
			preferentialAmount:''
		}
	}
	render() {
		let {
			visible,
			saveing,
			receiver,
			receivePhone,
			buyTime,
			salesName,
			totalAmount,
			schoolAreaName,
			orderDetails,
			preferentialAmount
		} = this.state;

		totalAmount = Number(totalAmount).toFixed(2);
		preferentialAmount = Number(preferentialAmount).toFixed(2);
		return (<div>

			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">物品售卖</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
		        <div className="f-pd5 f-right-title-box" style={{minHeight:'120px'}}>
			        <div className="f-bg-blue f-right-title">
	        			<h3>购买信息</h3>
	        		</div>
	        		<div className="f-pd3" />
	        		<span className="f-mr5">
	        			购买人姓名:<i className="f-fz4 f-black f-bold f-ml2">{receiver}</i>
	        		</span>
	        		<span className="f-mr5">
	        			手机号码:<i className="f-fz4 f-black f-bold f-ml2">{receivePhone}</i>
	        		</span>
	        		<span className="f-mr5">
	        			购买时间:<i className="f-fz4 f-black f-bold f-ml2">{buyTime}</i>
	        		</span>
	        		<span className="f-mr5">
	        			销售员:<i className="f-fz4 f-black f-bold f-ml2">{salesName}</i>
	        		</span>
	        		<div className="f-pd3" />
		        </div>
			</div>

			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
		        <div className="f-pd5 f-right-title-box" style={{minHeight:'120px'}}>
			        <div className="f-bg-blue f-right-title">
	        			<h3>物品信息</h3>
	        		</div>

	        		<div className="f-mr5 f-mb3">
	        			校区:
	        			<i className="f-fz4 f-black f-bold f-ml5">{schoolAreaName}</i>
        			</div> 

    				{
    					orderDetails && orderDetails.length ?
	    					<ul className="f-lh4">
	    						{
	    							orderDetails.map((d,i)=> {
	    								let {
	    									id,
	    									entityName,
	    									unitPrice,
	    									qty,
	    									isRefund,
	    									isTaken
	    								} = d;
	    								return(<li key={i} className="f-mb3">
				        					<span className="f-mr5">{entityName}</span>
				        					<span className="f-mr5">¥ {unitPrice}</span>
				        					<span className="f-mr5">数量:{qty}</span>
				        					{
				        						isRefund && isRefund == '0' && isTaken == 1 ?
							    					<Button 
							    						className="f-mr5"
							    						type="primary"
							    						onClick={
							    							()=> {
							    								this.setState({
							    									visible:true,
							    									orderDetailId:id
							    								});
							    							}
							    						}
							    					>
							    						退货
													</Button>
				        						:
				        							null
				        					}
				        					{
				        						isRefund && isRefund != '0' ?
							    					<span className="f-mr5 f-green">已退货</span>
				        						:
				        							null
				        					}
				        					{
				        						isTaken && isTaken == '0' ?
							    					<span className="f-mr5 f-red">未领取</span>
				        						:
				        							null
				        					}
				    					</li>);
	    							})
	    						}
	    					</ul>
    					: null

    				}
		        </div>
			</div>

			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
		        <div className="f-pd5 f-right-title-box">
			        <div className="f-bg-green f-right-title">
	        			<h3>费用</h3>
	        		</div>
	        		
	        		总计: 
	        		<i className="f-fz5 f-red f-ml5 f-mr5">¥{totalAmount}</i>
	        		{
	        			preferentialAmount ? 
			        		<span>
			        			优惠:
			        			<i className="f-fz5 f-red f-ml5">¥{preferentialAmount}</i>
			        		</span>
			        	: null
	        		}

		        </div>
			</div>

			<Modal
				title="退货"
				visible={this.state.visible}
				onOk={this._handleOk.bind(this)}
				confirmLoading={ saveing }
				onCancel={this._handleCancel.bind(this)}
				width='600px'
			>
				{
					visible ?
					<MyForm ref={form => this.form=form } />
					: null
				}
			</Modal>

		</div>);
	}
	componentDidMount() {
		this._getData();
	}
	_getData() {
		let {
			orderInfoId
		} = this.state;

		fpost('/api/reception/order/findOrder', {
				orderInfoId
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this._setState(res.result);
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_setState(data) {
		let {
			receiver,
			receivePhone,
			buyTime,
			salesName,
			totalAmount,
			schoolAreaName,
			orderDetails,
			preferentialAmount
		} = data;

		this.setState({
			receiver,
			receivePhone,
			buyTime,
			salesName,
			schoolAreaName,
			totalAmount,
			orderDetails,
			preferentialAmount
		});
	}
	_handleCancel() {
		this.setState({
			visible: false
		});
	}
	_handleOk(e) {
		e.preventDefault();
		this.form.validateFields((err, values) => {
			if (!err) {
				let param = {};
				let {
					orderDetailId,
					returnType
				} = this.state;
				let orderId = this.state.orderInfoId;
				param = {
					orderDetailId,
					orderId,
					returnType,
					...values
				}

				this._returnGoods(param);
			}
		});
	}
	_returnGoods(param) { //退货
		this.setState({
			saveing: true
		});

		fpost('/api/reception/order/salesReturn', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					saveing: false,
					visible: false
				});
				message.success('退货成功!');
				this._getData();
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

/*退货表单*/
class MainForm extends React.Component {
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		return (
			<Form>

				<FormItem
					label="退款金额"
					{...formItemLayout}
				>
					{
						getFieldDecorator('refundAmount', { 
							rules: [{
								required: true, 
								message: '退款金额' 
							}] 
						})
						(
							<InputNumber 
		                		min={0} 
	                		/>
						)
					}
				</FormItem>
				<FormItem
					label="退款方式"
					{...formItemLayout}
				>
					{
						getFieldDecorator('refundWay', { 
							rules: [{
								required: true, 
								message: '请选择退款方式' 
							}] 
						})
						(
							<Select style={{width:'160px'}} placeholder="请选择退款方式">
								{
									payTypes.map((d,i)=> {
										return <Option key={i} value={d.value}>{d.label}</Option>
									})
								}
							</Select>
						)
					}
				</FormItem>
				<FormItem
				    label="对内备注"
				    {...formItemLayout}
				>
				    {
				        getFieldDecorator('internalRemark', { 
				            rules: [{
				                required: false, 
				            }] 
				        })
				        (
				            <TextArea 
				            	rows={2} 
				            />
				        )
				    }
				</FormItem>
				<FormItem
				    label="对外备注"
				    {...formItemLayout}
				>
				    {
				        getFieldDecorator('foreignRemark', { 
				            rules: [{
				                required: false, 
				            }] 
				        })
				        (
				            <TextArea 
				            	rows={2} 
				            />
				        )
				    }
				</FormItem>

	      	</Form>
		);
	}
}

const MyForm = Form.create()(MainForm);