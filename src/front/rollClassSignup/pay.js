import React, {
	Component
} from 'react';
import {
	Input,
	Col,
	Row,
	Form,
	DatePicker,
	Select,
	Radio,
} from 'antd';

import PayTypes from '../../common/payTypes.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		let {
			totalPreferentialAmount,
			totalReceivableAmount,
			arrearAmount,
			totalMaterialAmount,
			totalAmount
		}= this.props.data;

		return (<div 
				className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mb5 f-mt5"
				style={{
					paddingLeft:'24px'
				}}
			>
			        <div className="f-pd5 f-right-title-box">
		        		<Row gutter={16} className="f-mb5">
		        			<Col span={2} className="f-align-right"></Col>
		        			<Col span={20}>
		        				<div className="f-mt2">
		        					<span className="f-mr5">
		        						<span className="f-mr2">优惠:</span>
		        						¥ {totalPreferentialAmount}
		        					</span>
		        					<span className="f-mr5">
		        						<span className="f-mr2">学费应收:</span>
		        						¥ {totalReceivableAmount}
		        					</span>
		        					<span className="f-mr5">
		        						<span className="f-mr2">物品应收:</span>
		        						¥ {totalMaterialAmount}
		        					</span>
		        					<span className="f-mr5">
		        						<span className="f-mr2">总计:</span>
		        						<i className="f-fz5 f-red" id="J-total">¥ {totalAmount}</i>
		        					</span>
		        					<span className="f-mr5">
		        						<span className="f-mr2">欠费:</span>
		        						<i className="f-fz5 f-red" id="J-total">¥ {arrearAmount}</i>
		        					</span>
		        				</div>
	        				</Col>
		        		</Row>
		        		<Row gutter={16}>
		        			<Col span={2} className="f-align-right f-lh6">支付方式 :</Col>
		        			<Col span={20}>
		        				<PayTypes 
		        					onInput = { this.props.onInput }
		        				/>
		        			</Col>
		        		</Row>
			        </div>
				</div>);
	}
}