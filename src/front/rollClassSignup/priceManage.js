import React, {
	Component
} from 'react';
import {
	Input,
	InputNumber,
	Button,
	Icon,
	Col,
	Row,
	Card,
	Modal,
	Form,
	DatePicker,
	Select,
	Upload,
	message,
	Radio,
	Tag,
	Checkbox
} from 'antd';
import store from 'store2';

import SelectGoods from '../../common/selectGoods.js';
import SelectDiscounts from '../../common/selectDiscounts.js';
import Remark from '../../common/remark.js';
import {
	fpost,
	fget
} from '../../common/io.js';
import {
	changeTwoDecimal
} from '../../common/g.js';

let SESSION = store.session();

const RangePicker = DatePicker.RangePicker;
const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;
const {
	TextArea
} = Input;

export default class Main extends Component {
	constructor(props) {
		super(props);

		this.state = {
			visibleSelectDiscounts: false, //是否显示选择优惠弹窗
			couponData: null, //已选择优惠

			visibleSelectGoods: false, //是否显示选择物品弹窗
			courseMaterialVOList: this.props.data.courseMaterialVOList, //已选择商品,
			courseMaterialVOListKeys: [], //默认商品id，用于默认选中
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visibleSelectGoods, //是否显示选择物品弹窗
			courseMaterialVOListKeys,
			courseMaterialVOList, //课程中包含的物品信息

			visibleSelectDiscounts, //是否显示选择优惠弹窗
			couponData,

		} = this.state;

		let {
			modifyMaterialPrice
		} = SESSION

		let {
			data
		} = this.props;
		let {
			schoolAreaId,
			courseId,
			className,
			courseName,
			totalOriginalPrice,
			totalCoursePeriod
		} = data;
		let {
			balanceAmount,
		} = this.props.studentData;
		console.log(balanceAmount);
		return (<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
			<div
				style={{
					paddingLeft:'14px'
				}}
			>
			<Row gutter={40} type="flex">
				<Col span={24}>
					<Row gutter={24}>
						<Col span={3} className="f-align-right f-lh7 f-black">班级名称:</Col>
						<Col span={3} className="f-lh7">{className}</Col>
						<Col span={3} className="f-align-right f-lh7 f-black">课程名称:</Col>
						<Col span={3} className="f-lh7">{courseName}</Col>
					</Row>
					<Row gutter={24}>
						<Col span={3} className="f-align-right f-lh7 f-black">课程费用:</Col>
						<Col span={20}>
							<div className="f-lh7 f-mb5 f-clear">
								<span className="f-mr5 f-left">
									<span className="f-mr2 f-left">原价:</span>
									<FormItem className="f-left">
										{
											getFieldDecorator('totalOriginalPrice',{
												rules:[{
													required:true,
													message:'请填写原价'
												}],
												initialValue:totalOriginalPrice
											})(
												<InputNumber 
													formatter={
														(value) => {
															// value = parseInt(value) || '';
															const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;
															if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
																return `¥ ${value}`
														    }else {
														    	return `¥ ${totalOriginalPrice}`
														    }
														}
													} 
													style={{ width:'100px' }} 
													size="large" 
													min={0}
													onChange={
														(v)=> {
															this.props.getTotalOriginalPrice(v || '');
														}
													}
												/>
											)
										}
									</FormItem>
								</span>
								<span className="f-mr5 f-left">
									<span className="f-mr2 f-left">总课次:</span>
			    					<FormItem className="f-left">
										{
											getFieldDecorator('totalCoursePeriod',{
												rules:[{
													required:true,
													message:'请填写课次'
												}],
												initialValue:totalCoursePeriod
											})(
												<InputNumber 
													formatter={
														(value) => {
															value = parseInt(value) || '';
															return `次 ${value}`
														}
													} 
													style={{ width:'100px' }} 
													size="large" 
													min={0}
													onChange={
														(v)=> {
															this.props.getTotalCoursePeriod(v || '');
														}
													}
												/>
											)
										}
									</FormItem>
								</span>
								{
									balanceAmount ?
										<span className="f-mr5 f-left">
											<span className="f-mr2 f-left">使用余额:</span>
											<FormItem className="f-left">
												{
													getFieldDecorator('offsetBalanceAmount',{
														rules:[{
															required:false,
															message:'请填写'
														}],
														initialValue:0
													})(
														<InputNumber 
															formatter={
																(value)=> {
																	value = (value > balanceAmount) ? balanceAmount : value;
																	return `¥ ${value}`
																}
															} 
															style={{ width:'100px' }} 
															size="large" 
															min={0}
															max={balanceAmount}
															onChange={
																(v)=> {
																	this.props.getOffsetBalanceAmount(v || '');
																}
															}
														/>
													)
												}
											</FormItem>
											（可用¥{balanceAmount}）
										</span>
									: null
								}
							</div>
						</Col>
					</Row>
					<Row gutter={24} className="f-mb5">
						<Col span={3} className="f-align-right f-lh7 f-black">物品信息:</Col>
						<Col span={20}>
							{
								(courseMaterialVOList && courseMaterialVOList.length) ?
									<ul className="f-lh7 my-goods">
										{
											courseMaterialVOList.map((d,i)=> {
												let {
													quantity,
													salePrice
												} = d;
												return (<li key={i}>
													<span className="f-mr5">{d.materialName}</span>
													<span className="f-mr5">
														{
															modifyMaterialPrice ?
																<InputNumber 
																	formatter={
																		(value)=> {
																			if(/^\d*(\.\d{0,2})?$/.test(value)){
																				return `¥ ${value}`
																			}else{
																				value = parseFloat(value);
																				value = Math.round(value * 100) / 100;
																				return `¥ ${value}`;
																			};
																		}
																	} 
																	style={{ width:'100px' }} 
																	size="large" 
																	value={salePrice}
																	defaultValue={0}
																	min={0}
																	onChange={ (e)=> this._onGoodsPriceChange(e,d) }
																/>
															: <span className="f-mr5">{`¥ ${salePrice}`}</span>
														}
													</span>
													<span className="f-mr5">
														<InputNumber 
															formatter={
																(value) => {
																	value = parseInt(value);
																	return `✖️ ${value}`
																}
															} 
															style={{ width:'100px' }} 
															size="large" 
															value={quantity}
															defaultValue={1}
															min={1}
															onChange={ (e)=> this._onGoodsUnitChange(e,d) }
														/>
													</span>
													<span>
														<Checkbox 
															checked={d.isTakeAway=='1' ? true : false}
															onChange={ (e)=> this._isGet(e,d) }
														>已领取</Checkbox>
													</span>
													<Icon 
														onClick={  
															(e)=> this._removeGoods(e,d)
														}
														type="delete" 
														className="f-fz5 f-ml3 f-pointer" 
													/> 
												</li>);
											})
										}
									</ul>
								: null
							}
							<Button
								className="f-mt1"
								size="large" 
								type="primary"
								onClick={ ()=> {
									this.setState({
										visibleSelectGoods:true
									});
								} }
							>添加物品</Button>
						</Col>
					</Row>
					<Row gutter={24}>
						<Col span={3} className="f-align-right f-lh7 f-black">优惠信息:</Col>
						<Col span={20}>
							{
								(couponData && couponData.coupon) ? 
									<span>
										{
											couponData.discount ?
												<Tag 
													color="orange" 
													className="f-lh7 discounts-tag"
												> 折扣: { couponData.discount } </Tag>
											: null
										}
										{
											couponData.cash ?
												<Tag 
													color="orange" 
													className="f-lh7 discounts-tag"
												> 现金优惠: ¥{ couponData.cash } </Tag>
											: null
										}
										{ couponData.coupon.map((d,i)=> {
											return (<Tag 
													key={i}
													color="orange" 
													className="f-lh7 discounts-tag"
												>
													{d.name}：¥{d.amount}
												</Tag>)
										}) }
									</span>
								: null
							}
							<Button 
								className="f-mb3" 
								size="large" 
								type="primary"
								onClick={ ()=> {
									this.setState({
										visibleSelectDiscounts:true
									});
								} }
							>选择优惠</Button>
						</Col>
					</Row>
					{/*备注*/}
					<div style={{
						position:'relative',
						left:'14px'
					}}>
						<Remark
							onInput= { this.props.getRemark }
						/>
					</div>

				</Col>
			</Row>
			{/*选择物品*/}
			{
				visibleSelectGoods ?
					<SelectGoods 
						visible={ visibleSelectGoods }
						handleOk= { this._selectGoodsOk.bind(this) }
						handleCancel= { this._selectGoodsCancel.bind(this) }
						// courseMaterialVOListKeys= { this.state.courseMaterialVOListKeys }
						schoolAreaId={schoolAreaId}
					/>
				: null
			}
			{/*选择优惠*/}
			{
				visibleSelectDiscounts ? 
					<SelectDiscounts
						visible={ visibleSelectDiscounts }
						handleOk= { this._selectDiscountsOk.bind(this) }
						handleCancel= { this._selectDiscountsCancel.bind(this) }
						courseId={ courseId }
						materialIds={ courseMaterialVOListKeys }
						defaultData={ couponData }
					/>
				: null
			}
			</div>
		</div>);
	}
	componentDidMount() {

	}
	componentDidUpdate() {
		
	}
	_computePrice() { //计算价格
		
	}
	_selectGoodsOk(v) { //已选择物品
		console.log(v);
		let {
			courseMaterialVOList,
			courseMaterialVOListKeys,
		} = this.state;

		if (!v || !v.length) {
			message.error('请选择物品');
			return;
		}

		v.forEach((d) => {
			let {
				materialId,
				salePrice
			} = d;
			/*去重start*/
			let oldG = courseMaterialVOList.filter(
				(oldD, i) => {
					return oldD.materialId == materialId;
				}
			);
			// console.log(oldG);
			if (!oldG || !oldG.length) { //看是否默认商品中已经有了这个商品，有得话就去重
				// console.log('hasSaveGoods?:',oldG);
				courseMaterialVOListKeys.push(materialId);
				d.quantity = 1;
				d.isTakeAway = 1;
				courseMaterialVOList.push(d);
			}
			/*去重end*/
		});

		this.setState({
			courseMaterialVOList,
			courseMaterialVOListKeys,
			visibleSelectGoods: false
		}, () => {
			// console.log('已选择物品:', this.state.courseMaterialVOList);
			this._getGoodsTotalPrice();
			this._clearDiscounts();
		});
	}
	_selectGoodsCancel() { //取消选择物品
		this.setState({
			visibleSelectGoods: false
		});
	}
	_onGoodsPriceChange(v, d) { //当改变物品价格
		let {
			courseMaterialVOList
		} = this.state;
		let curPrice = v;
		let materialId = d.materialId;
		let index;

		if (!curPrice || isNaN(curPrice) || curPrice < 0) {
			courseMaterialVOList.forEach((data, i) => {
				if (data.materialId == d.materialId) {
					data.salePrice = 0;
				}
			});
			this.setState({
				courseMaterialVOList
			}, () => {
				this._getGoodsTotalPrice();
			});
			return;
		}

		index = courseMaterialVOList.findIndex((obj) => {
			return obj.materialId == materialId;
		});

		courseMaterialVOList[index].salePrice = curPrice;

		this.setState({
			courseMaterialVOList
		}, () => {
			// console.log('当改变物品价格', this.state.courseMaterialVOList)
			this._getGoodsTotalPrice();
		});
	}
	_removeGoods(e, d) { //删除物品(挪到弹窗里面操作，取消checkbox选择即可)
		let {
			courseMaterialVOList,
			courseMaterialVOListKeys
		} = this.state;
		let materialId = d.materialId;
		courseMaterialVOList = courseMaterialVOList.filter((obj) => obj.materialId != materialId);
		courseMaterialVOListKeys = courseMaterialVOListKeys.filter((id) => id != materialId);
		this.setState({
			courseMaterialVOList,
			courseMaterialVOListKeys
		}, () => {
			// console.log(courseMaterialVOList);
			this._getGoodsTotalPrice();
			this._clearDiscounts();
		});
	}
	_onGoodsUnitChange(v, d) { //改变物品数量
		let curCount = v;
		let {
			courseMaterialVOList
		} = this.state;
		let materialId = d.materialId;
		let index;

		if (!curCount || isNaN(curCount)) {
			courseMaterialVOList.forEach((data, i) => {
				if (data.materialId == d.materialId) {
					data.quantity = 0;
				}
			});
			this.setState({
				courseMaterialVOList
			}, () => {
				this._getGoodsTotalPrice();
			});
			return;
		}

		index = courseMaterialVOList.findIndex((obj) => {
			return obj.materialId == materialId;
		});

		courseMaterialVOList[index].quantity = curCount;

		this.setState({
			courseMaterialVOList
		}, () => {
			// console.log('数量改变后', this.state.courseMaterialVOList);
			this._getGoodsTotalPrice();
		});
	}
	_isGet(e, d) {
		let isTakeAway = e.target.checked ? 1 : 0;
		let materialId = d.materialId;
		let {
			courseMaterialVOList
		} = this.state;

		let index = courseMaterialVOList.findIndex((obj) => {
			return obj.materialId == materialId;
		});

		courseMaterialVOList[index].isTakeAway = isTakeAway;

		this.setState({
			courseMaterialVOList
		}, () => {
			this._getGoodsTotalPrice();
			// console.log('取消已领取后', this.state.courseMaterialVOList);
		});
	}
	_getGoodsTotalPrice() { //计算物品总价格
		let {
			courseMaterialVOList
		} = this.state;
		let {
			getGoodsData
		} = this.props;
		let goodsTotalPrice = 0;
		courseMaterialVOList.forEach((d, i) => {
			let {
				salePrice,
				quantity
			} = d;
			d.quantity = quantity; //报名接口需要的数量字段quantity
			goodsTotalPrice += Number(salePrice) * Number(quantity);
		});

		goodsTotalPrice = changeTwoDecimal(goodsTotalPrice);

		getGoodsData(courseMaterialVOList,goodsTotalPrice);
	}
	_clearDiscounts() {
		let { selectDiscountsOk } = this.props;
		selectDiscountsOk([]);
		this.setState({
			couponData:null
		})
	}
	_selectDiscountsOk(v) { //确定选择优惠
		let { selectDiscountsOk } = this.props;
		this.setState({
			couponData: v,
			visibleSelectDiscounts: false
		});
		selectDiscountsOk(v);
	}
	_selectDiscountsCancel() { //取消选择优惠
		this.setState({
			visibleSelectDiscounts: false
		});
	}
}