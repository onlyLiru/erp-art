import React, {
	Component
} from 'react';
import {
	Col,
	Row,
	Form,
	DatePicker,
	Select,
	Radio
} from 'antd';
import moment from 'moment';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import {
	courseFrom
} from '../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state={
			salesFrom:''
		}
	}
	render() {
		const {
			getFieldDecorator,
			setFieldsValue
		} = this.props.form;
		let {
			salesFrom
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 7
			},
			wrapperCol: {
				span: 17
			}
		};

		let schoolAreaId = this.props.data.schoolAreaId;

		return (<div>
			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-pd4">
    			<Row gutter={ 40 } type="flex">
    				
    				<Col span={8}>
    					<FormItem
							label="课程销售"
							{...formItemLayout}
						>
							{
								getFieldDecorator('salesId', {
									rules: [{  
										required:true,
										message:'请选择'
									}]
								})(
									<SelectPerson
										url='/api/hr/staff/listStaffByCondForDropDown'
										data={
											{ 
												type:1,
												schoolAreaId:schoolAreaId
											}
										}
										onSelect={
											(v) => {
												this.props.form.setFieldsValue({
													salesId: v
												});
											}
										}
									/>
								)
							}
						</FormItem>
    				</Col>
    				<Col span={8}>
    					<FormItem
							label="是否新生"
							{...formItemLayout}
						>
							{
								getFieldDecorator('isNew', {
									rules: [{ 
										required: true, 
										message: '请选择是否新生' 
									}]
								})(
									<Select
										placeholder="请选择"
									>
								        <Option value='1'>是</Option>
								        <Option value='0'>否</Option>
								     </Select>
								)
							}
						</FormItem>
    				</Col>
    				<Col span={8}>
    					<FormItem
							label="销售来源"
							{...formItemLayout}
						>
							{
								getFieldDecorator('salesFrom', {
									rules: [{  
										required:true,
										message: '请选择来源' 
									}]
								})(
									<Select
										size="large"
										allowClear
	                					placeholder="请选择来源"
	                					onChange={
	                						(v)=> {
	                							this.setState({
	                								salesFrom:v
	                							});
	                						}
	                					}
									>
										{
											courseFrom.map((d,i)=> {
												return(<Option 
														key={i} 
														value={d.value}
													>{d.label}</Option>);
											})
										}
									</Select>
								)
							}
						</FormItem>
    				</Col>
    				{
    					salesFrom && 
    					(
    						salesFrom == '10'
    						|| salesFrom == '11'
    						|| salesFrom == '12'
    					) ?
		    				<Col span={8}>
	        					<FormItem
									label="上课老师"
									{...formItemLayout}
								>
									{
										getFieldDecorator('teacherId', {
											rules: [{  
												required:true,
												message:'请选择上课老师'
											}]
										})(
											<SelectPerson 
												onSelect={
													(v) => {
														this.props.form.setFieldsValue({
															teacherId: v
														});
													}
												}
											/>
										)
									}
								</FormItem>
	        				</Col>
	        			: null
    				}
    	        </Row>
			</div>
		</div>);
	}
}