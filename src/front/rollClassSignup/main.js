import React,{Component} from 'react';
import {
	Form,
	Button,
	message
} from 'antd';
import StudentInfo from './studentInfo.js';
import PriceManage from './priceManage.js';
import OtherInfo from './otherInfo.js';
import Pay from './pay.js';
import {
	fpost,
	fpostArray
} from '../../common/io.js';
import {
	getUnicodeParam,
	undefinedToEmpty
} from '../../common/g.js';

let classId = getUnicodeParam('classId');
let studentId = getUnicodeParam('studentId');
let timer;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state={
			rollingRegistrationCostVO:null,
			rollingStudentVO:null,

			rollingRegistrationPreferentialForm:null,//优惠
			rollingRegistrationMaterialForm:null,//物品
			registrationPayTypeVOList:null//支付类型
		}
	}
	render() {
		let {
			rollingRegistrationCostVO,
			rollingStudentVO
		}= this.state;

		if(!rollingRegistrationCostVO || !rollingStudentVO) {
			return null;
		};

		return(<Form>
			<StudentInfo 
				data={rollingStudentVO}
			/>
			<PriceManage 
				form={this.props.form}
				selectDiscountsOk={this._selectDiscountsOk.bind(this)}
				getRemark={this._getRemark.bind(this)}
				getGoodsData={this._getGoodData.bind(this)}
				getTotalOriginalPrice={this._getTotalOriginalPrice.bind(this)}
				getTotalCoursePeriod={this._getTotalCoursePeriod.bind(this)}
				getOffsetBalanceAmount={this._getOffsetBalanceAmount.bind(this)}
				data={rollingRegistrationCostVO}
				studentData={rollingStudentVO}
			/>
			<Pay 
				onInput = { this._getPayData.bind(this) }
				data={rollingRegistrationCostVO}
			/>
			<OtherInfo 
				form={this.props.form}
				data={rollingRegistrationCostVO}
			/>
			<div className="f-align-right f-mt5">
				<Button
					size="large" 
					className="f-mr5"
					onClick={
						()=> {
							window.history.back();
						}
					}
				>
					取消
				</Button>
				<Button 
					size="large" 
					type="primary"
					onClick={ this._validator.bind(this) }
				>
					保存
				</Button>
			</div>
		</Form>);
	}
	_getTotalOriginalPrice(v) {//获取原价
		console.log('原价',v);
		if(!v || isNaN(v)) { return; }
		let {
			rollingRegistrationCostVO
		} = this.state;
		rollingRegistrationCostVO={
			...rollingRegistrationCostVO,
			totalOriginalPrice:v
		}
		this.setState({
			rollingRegistrationCostVO
		},this._computePrice);
	}
	_getTotalCoursePeriod(v) {//获取课次
		console.log('总课次',v);
		if(!v || isNaN(v)) { return; }
		let {
			rollingRegistrationCostVO
		} = this.state;
		rollingRegistrationCostVO={
			...rollingRegistrationCostVO,
			totalCoursePeriod:v
		}
		this.setState({
			rollingRegistrationCostVO
		},this._computePrice);
	}
	_getOffsetBalanceAmount(v) {//获取使用余额
		console.log('使用余额',v);
		if(!v || isNaN(v)) { return; }
		let {
			rollingRegistrationCostVO
		} = this.state;
		rollingRegistrationCostVO={
			...rollingRegistrationCostVO,
			offsetBalanceAmount:v
		}
		this.setState({
			rollingRegistrationCostVO
		},this._computePrice);
	}
	_getPayData(v) { //支付信息
		console.log('支付信息',v);
		let {
			trades
		}= v;
		this.setState({
			registrationPayTypeVOList:trades
		},this._computePrice);
	}

	_selectDiscountsOk(v) { //获取优惠信息
		let {
			registrationCourseReduceList
		} = v;
		console.log('优惠信息',registrationCourseReduceList);
		this.setState({
			rollingRegistrationPreferentialForm:registrationCourseReduceList
		},this._computePrice);
	}
	_getGoodData(data,goodsTotalPrice) {
		console.log('商品信息',data,'物品总计:',goodsTotalPrice);
		this.setState({
			rollingRegistrationMaterialForm:data
		},this._computePrice);
	}
	_getRemark(v) {
		console.log('备注:', v);
		let {
			internalRemark,
			foreignRemark
		} = v;
		this.setState({
			internalRemark,
			foreignRemark
		});
	}
	_validator(e) {
		let {
			rollingRegistrationCostVO,
			rollingStudentVO,
			rollingRegistrationPreferentialForm,
			rollingRegistrationMaterialForm,
			registrationPayTypeVOList,
			internalRemark,
			foreignRemark
		}= this.state;

		// if(!registrationPayTypeVOList || !registrationPayTypeVOList.length) {
		// 	message.error('请选择支付方式');
		// 	return;
		// }

		this.props.form.validateFields((err, values) => {
			if (!err) {
				
				this._save({
					rollingRegistrationCostVO,
					...rollingRegistrationCostVO,
					rollingStudentVO,
					rollingRegistrationPreferentialForm,
					rollingRegistrationMaterialForm,
					registrationPayTypeVOList,
					studentId,
					classId,
					internalRemark,
					foreignRemark,
					...values
				});
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});
		let url = '/rolling/addRegistration';

		fpostArray(url, values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					saveing: false
				});
				message.success(res.message);
				// window.location.href = '/teach/classList';
				let registrationId = res.result.registrationId;
				let schoolAreaId = res.result.schoolAreaId;
				let url = "/front/studentslist";
				url = `/print?registrationId=${registrationId}&schoolAreaId=${schoolAreaId}&type=3`;
				window.location.href = url;
			})
			.catch((err) => {
				console.log(err);
			});
	}
	componentDidMount() {
		this._getData();
	}
	_getData() {
		fpost('/rolling/findRollingRegistrationVO', {
				studentId,
				classId
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let rollingRegistrationMaterialForm = result.rollingRegistrationCostVO.courseMaterialVOList
				this.setState({
					...result,
					rollingRegistrationMaterialForm
				},()=> console.log(this.state));
				// this.props.form.setFieldsValue(result);
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_computePrice() {
		let self = this;
		let {
			getFieldsValue
		} = this.props.form;
		let data={};
		let formData = getFieldsValue(['totalOriginalPrice','totalCoursePeriod','offsetBalanceAmount']);
		formData= undefinedToEmpty(formData);
		let {
			rollingRegistrationPreferentialForm,
			rollingRegistrationMaterialForm,
			registrationPayTypeVOList,
			rollingRegistrationCostVO
		}= this.state;
		data={
			studentId,
			classId,
			...formData,
			rollingRegistrationPreferentialForm,
			rollingRegistrationMaterialForm,
			registrationPayTypeVOList
		}

		if(timer) {
			clearTimeout(timer);
		}

		timer = setTimeout(cP,2000);
		
		function cP() {
			fpostArray('/rolling/calculateRegistrationCost', data)
				.then(res => res.json())
				.then((res) => {
					if (!res.success || !res.result) {
						this.setState({
							loading: false
						});
						message.error(res.message || '系统错误');
						throw new Error(res.message || '系统错误');
					};
					return (res.result);
				})
				.then((result) => {
					let {
						totalPreferentialAmount=0,
						totalReceivableAmount=0,
						totalMaterialAmount=0,
						totalAmount=0,
						arrearAmount=0
					} = result;
					rollingRegistrationCostVO={
						...rollingRegistrationCostVO,
						totalPreferentialAmount,
						totalReceivableAmount,
						totalMaterialAmount,
						totalAmount,
						arrearAmount
					}
					self.setState({
						rollingRegistrationCostVO
					});
				})
				.catch((err) => {
					console.log(err);
				});
			
		}
	}
}

const MainForm = Form.create()(Main);
export default MainForm;

