import React,{Component} from 'react';
export default class StudentInfo extends Component {
	constructor(props) {
		super(props);
		this.state={

		}
	}
	render() {
		let {
			data
		} = this.props;

		return(<div>
			<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
				<ul className="f-clear info-list info-bd-top"  style={{width:'100%'}}>
					<li style={{width:'33%'}}>
						<span className="f-pale f-mr2">学员姓名(学号):</span>
						{data.name}({data.stuNo})
					</li>
					<li style={{width:'33%'}}>
						<span className="f-pale f-mr2">家长手机:</span>
						{data.mobile}
					</li>
					<li style={{width:'33%'}}>
						<span className="f-pale f-mr2">学员生日:</span>
						{data.birthday}
					</li>
					<li style={{width:'33%'}}>
						<span className="f-pale f-mr2">市场来源:</span>
						{data.recruitSourceStr}
					</li>
					<li style={{width:'33%'}}>
						<span className="f-pale f-mr2">课程顾问:</span>
						{data.saleName}
					</li>
					<li style={{width:'33%'}}>
						<span className="f-pale f-mr2">余额:</span>
						<i className="f-cny f-mr2">¥ {data.balanceAmount}</i>
					</li>
				</ul>
			</header>
		</div>);
	}
}