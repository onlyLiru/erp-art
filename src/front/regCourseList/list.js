import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../common/io.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">报课列表</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 1700}}
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
				  />
			</div>
    );
  }

}

const columns = [{
  title: '学员姓名（学号）',
  dataIndex: 'studentName',
  width: '150px',
  className: 'f-align-center',
  fixed: 'left',
  render: (value, row, index) => {
    let {
      studentName,
      stuNo
    } = row;
    return <div 
      className="f-line1 f-over-hide"
      style={{
        width:'150px'
      }}
      title={`${studentName}(${stuNo})`}
    >
  		{ studentName }({stuNo})
  	</div>
  }
}, {
  title: '家长手机号',
  dataIndex: 'phone',
  className: 'f-align-center',
  width: '150px'
}, {
  title: '班级名称',
  dataIndex: 'className',
  width: '200px'
}, {
  title: '报名校区',
  dataIndex: 'schoolAreaName',
  width: '150px'
}, {
  title: '课程',
  dataIndex: 'courseName',
  width: '150px'
}, {
  title: '课程原价',
  dataIndex: 'originalPrice',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '优惠金额',
  dataIndex: 'preferentialAmount',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '欠费金额',
  dataIndex: 'arrearAmount',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '报课来源',
  dataIndex: 'salesFromStr',
  width: '100px'
}, {
  title: '课程顾问',
  dataIndex: 'salesName',
  width: '100px'
}, {
  title: '报课状态',
  dataIndex: 'stateStr',
  width: '100px'
}, {
  title: '是否续报',
  dataIndex: 'isContinueStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '是否新生',
  dataIndex: 'isFreshManStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  width: '100px',
  className: "f-align-center",
  render: (value, row, index) => {
    let {
      potentialStudentId,
      name,
      phone
    } = row;
    return (<div>
  		<Dropdown 
        overlay={
          <Menu>
            <Menu.Item key="3">
              <a href={`/front/students/detail?id=${row.id}`}>查看</a>
            </Menu.Item>
            <Menu.Item>
              <a href={`/front/students/singRecord?id=${row.id}`}>上课记录</a>
            </Menu.Item>
            <Menu.Item key="1">
              <a href={`/front/selectclass?studentId=${row.studentId}`}>续报</a>
            </Menu.Item>
          </Menu>
        }>
        <a>
          操作<Icon type="down" />
        </a>
      </Dropdown>
  	</div>);
  }
}];