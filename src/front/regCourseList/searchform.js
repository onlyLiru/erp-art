import React, {
	Component
} from 'react';
import $ from 'jquery';
import {
	Input,
	Button,
	Icon,
	Col,
	Row,
	Form,
	DatePicker,
	Select
} from 'antd';
import RangeDate from '../../common/rangeDate.js';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import SelectSchool from '../../common/selectSchool.js';
import SelectCourses from '../../common/selectCourses.js';
import {
	gradeList,
	fromData
} from '../../common/staticData.js';
import {
	undefinedToEmpty
} from '../../common/g.js';
const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isAdvancedSearch: false
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const {
			isAdvancedSearch
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form>
				<Row gutter={ 40 }>
    				<Col span={8}>
    					<FormItem
							label="学员姓名"
							{...formItemLayout}
						>
							{
								getFieldDecorator('name', { 
									rules: [{
										required: false, 
										message: '请输入学员姓名' 
									}] 
								})
								(
									<Input placeholder="请输入学员姓名" />
								)
							}
						</FormItem>
    				</Col>
    				<Col span={8}>
    					<FormItem
							label="报名校区"
							{...formItemLayout}
						>
							{
								getFieldDecorator('schoolAreaId', { 
									rules: [{
										required: false, 
										message: '请选择校区' 
									}],
									initialValue:''
								})
								(
									<SelectSchool 
										onSelect={
											(v) => {
												this.props.form.setFieldsValue({
													schoolAreaId:v
												});
											}
										}
										width='100%' 
										url="/api/system/schoolarea/listSchoolArea"
									/>
								)
							}
						</FormItem>
    				</Col>
    				<Col span={ 8 }>
						<FormItem
							label="课程"
							{...formItemLayout}
						>
							{
								getFieldDecorator('courseId', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<SelectCourses 
										onSelect= {
											(v)=> {
												this.props.form.setFieldsValue({
													courseId:v
												});
											}
										}
									/>
								)
							}
						</FormItem>
					</Col>
    				<Col span={8}>
    					<FormItem
							label="报名日期"
							{...formItemLayout}
						>
							{
								getFieldDecorator('rangeTime',{
									initialValue:''
								})(
									<RangeDate 
										onSelect={
											(d)=> {
												this.props.form.setFieldsValue({
													rangeTime:d,
													beginTime:d.startString,
													endTime:d.endString,
												});
											}
										}
										showTime={false}
									/>
								)
							}
						</FormItem>
						<span className="f-hide">
					        {
								getFieldDecorator('beginTime',{
									initialValue:''
								})(
									<Input />
								)
							}
							{
								getFieldDecorator('endTime',{
									initialValue:''
								})(
									<Input />
								)
							}
						</span>
    				</Col>
    				<Col span={8}>
    					<FormItem
							label="班级名称"
							{...formItemLayout}
						>
							{
								getFieldDecorator('className', { 
									rules: [{
										required: false, 
										message: '请输入班级名称' 
									}],
									initialValue:''
								})
								(
									<Input
										placeholder="请输入班级名称" 
									/>
								)
							}
						</FormItem>
    				</Col>
    				<Col span={ 8 }>
						<FormItem
							label="上课老师"
							{...formItemLayout}
						>
							{
								getFieldDecorator('teacherId', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<SelectPerson
										url='/api/hr/staff/listStaffByCondForDropDown'
										data={
											{
												type:6
											}
										}
										onSelect={ 
											(v)=> {
												v = v ? v : null;
												this.props.form.setFieldsValue({
													teacherId: v
												});
											} 
										}
									/>
								)
							}
						</FormItem>
					</Col>
				{ isAdvancedSearch ? 
					<div>
						<Col span={8}>
	    					<FormItem
								label="报课来源"
								{...formItemLayout}
							>
								{
									getFieldDecorator('salesFrom', {
										rules: [{  
											message: '请选择' 
										}]
									})(
										<Select
											size="large"
											allowClear
		                					placeholder="请选择"
										>
											{
												fromData.map((d,i)=> {
													return(<Option 
															key={i} 
															value={d.value}
														>{d.label}</Option>);
												})
											}
										</Select>
									)
								}
							</FormItem>
	    				</Col>
	    				<Col span={ 8 }>
							<FormItem
								label="课程顾问"
								{...formItemLayout}
							>
								{
									getFieldDecorator('salesId', {
										rules: [{  
											message: '请选择' 
										}]
									})(
										<SelectPerson
											url='/api/hr/staff/listStaffByCondForDropDown'
											data={
												{ 
													type:2
												}
											} 
											onSelect={ 
												(v)=> {
													v = v ? v : null;
													this.props.form.setFieldsValue({
														salesId: v
													});
												} 
											}
										/>
									)
								}
							</FormItem>
						</Col>
	    				<Col span={ 8 }>
							<FormItem
								label="报课状态"
								{...formItemLayout}
							>
								{
									getFieldDecorator('state', {
										rules: [{  
											message: '请选择' 
										}]
									})(
										<Select
					                  	  showSearch
					                  	  allowClear
								          placeholder='请选择'
								          style={{ width: '100%' }}
								          optionFilterProp="children"
				        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								        >
											<Option key={1} value='1'>正常报名</Option>
											<Option key={2} value='2'>转出</Option>
											<Option key={3} value='3'>未开始</Option>
											<Option key={4} value='4'>退课</Option>
											<Option key={5} value='5'>结课</Option>
											<Option key={6} value='6'>停课</Option>
								        </Select>
									)
								}
							</FormItem>
						</Col>
	    				<Col span={ 8 }>
							<FormItem
								label="是否欠费"
								{...formItemLayout}
							>
								{
									getFieldDecorator('isArrear', {
										rules: [{  
											message: '请选择' 
										}]
									})(
										<Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                     <Option key={0} value='0'>缴清</Option>
						                     <Option key={1} value='1'>欠费</Option>
					                      </Select> 
									)
								}
							</FormItem>
						</Col>
	    				<Col span={ 8 }>
							<FormItem
								label="是否优惠"
								{...formItemLayout}
							>
								{
									getFieldDecorator('isOriginal', {
										rules: [{  
											message: '请选择' 
										}]
									})(
										<Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      <Option key={0} value='0'>优惠</Option>
				                     		  <Option key={1} value='1'>原价</Option>
					                      </Select>
									)
								}
							</FormItem>
						</Col>
					</div>
	       			: null
	    	    }
    	        </Row>
    	        <div className="f-align-center">
    	        	<Button 
    	        		size="large" 
    	        		className="f-mr4" 
    	        		onClick={ this._reset.bind(this) }
    	        	>重置</Button>
    	        	<Button 
    	        		type="primary"
    	        		size="large" 
    	        		icon="search" 
    	        		onClick={ this._search.bind(this) }
    	        	>查询</Button>
    	        	<a 
    	        		style={{ marginLeft: 8, fontSize: 12 }} 
    	        		onClick={
    	        			()=> {
    	        				this.setState({
    	        					isAdvancedSearch:!this.state.isAdvancedSearch
    	        				});
    	        			}
    	        		}>
    	              高级搜索 
    	              <Icon type={this.state.isAdvancedSearch ? 'up' : 'down'} />
    	            </a>
    	        </div>
	      	</Form>
		</header>);
	}
	componentDidMount() {
		this._changeSearchType();
	}
	_changeSearchType() {
		const self = this;
		$('#J-searchTab li').on('click', function(e) {
			let type = $(this).attr('data-type');
			let isAdvancedSearch = type == 1 ? true : false;

			$(this).addClass('cur f-blue').siblings().removeClass('cur f-blue');

			self.setState({
				isAdvancedSearch
			});
		});
	}
	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			isAdvancedSearch: false
		}, () => {
			this.setState({
				isAdvancedSearch: true
			}, () => {
				this._search();
			})
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;