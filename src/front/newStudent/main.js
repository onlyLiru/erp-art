import React,{Component} from 'react';
import {
	Breadcrumb,
	Button,
	message
} from 'antd';
import {
	undefinedToEmpty,
	getUnicodeParam
} from '../../common/g.js';
import moment from 'moment';
import {
	fpost
} from '../../common/io.js';

import MyForm from './form.js';
let ID = getUnicodeParam('studentId');

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state={
			result:null
		}
	}
	render(){
		let {
			result
		} = this.state;

		if(ID && !result) {
			return null;
		}

		return(<div>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>学员管理</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">创建学员</span>
			    </Breadcrumb.Item>
			</Breadcrumb>
			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white">
				<MyForm 
					ref={(form)=>this.form=form}
					data={result || {}}
				/>
				<div className="f-pd5 f-align-center">
					<Button 
						type="primary"
						size="large"
						className="f-mr3"
						onClick={ 
							()=> {
								this._submit('0') 
							}
						}
					>保存并报名</Button>
					<Button 
						size="large"
						onClick={
							()=> {
								this._submit('1') 
							}
						}
					>仅保存</Button>
				</div>
			</div>
		</div>);
	}
	componentDidMount() {
		if (ID) {
			this._getData();
		}
	}
	_getData() {
		fpost('/api/reception/student/findStudent', {
				studentId: ID
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({result});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_submit(type) {
		this.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				let {
					motherPhone,
					fatherPhone,
					otherPhone,
					birthday,
					citySelect
				} = values;
				birthday = birthday ? moment(birthday).format('YYYY-MM-DD') : '';
				let provinceId = citySelect[0];
				let cityId = citySelect[1];
				let districtId = citySelect[2];

				values.birthday = birthday;
				values.provinceId = provinceId;
				values.cityId = cityId;
				values.districtId = districtId;

				if (!motherPhone && !fatherPhone && !otherPhone) {
					message.error('至少要有一个电话号码!');
					return;
				}
				values = undefinedToEmpty(values);
				this._save(values,type);
			}
		});
	}
	_save(values,type) {
		let url =ID ? '/api/reception/student/updateStudent' : '/api/reception/student/registerStudent';
		
		this.setState({
			saveing: true
		});

		fpost(url, values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					saveing: false
				});
				message.success(res.message);
				if(type=='0') {
					window.location.href = `/front/selectclass?studentId=${res.result}`;
				}else {
					window.location.href = '/front/studentslist';
				}
			})
			.catch((err) => {
				console.log(err);
			});
	}
}