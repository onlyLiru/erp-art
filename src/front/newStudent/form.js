import React, {
	Component
} from 'react';
import {
	Input,
	Col,
	Row,
	Modal,
	Form,
	DatePicker,
	Select,
	message,
	Radio,
} from 'antd';
import moment from 'moment';
import {
	fpost
} from '../../common/io.js';
import {
	undefinedToEmpty
} from '../../common/g.js';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import AvatarUpload from '../../common/uploadAvatar.js';
import SelectCity from '../../common/selectCity.js';
import {
	gradeList,
	fromData
} from '../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			saveing
		} = this.state;
		let {
			data
		} = this.props;

		const formItemLayout = {
			labelCol: {
				span: 8
			},
			wrapperCol: {
				span: 16
			}
		};

		let {
			id,
			name,
			headImgUrl,
			resourceId,
			gender,
			stuNo,
			birthday,
			motherPhone,
			motherJob,
			fatherPhone,
			fatherJob,
			otherPhone,
			recruitSource,
			salesId,
			salesName,
			currentSchoolName,
			balanceAmount = 0,

			isPublicSchool,
			grade,
			provinceId,
			cityId,
			districtId,
			address

		} = data;

		let citySelect = provinceId && cityId ?
			[provinceId, cityId, districtId] : [];

		birthday = birthday ? moment('2018-07-11') : null;

		return (<Form>
			<div className="f-pd5">
				<h3 className="f-mb3">基本信息</h3>
				<Row gutter={24} type="flex">
					<Col span={15}>
		    			<Row gutter={ 40 }>
		    				<Col span={12}>
		    					<span className="f-hide">
		    						{
		    							getFieldDecorator('id',{
		    								initialValue:id
		    							})
		    							(
		    								<Input />
		    							)
		    						}
		    					</span>
	        					<FormItem
									label="学员姓名"
									{...formItemLayout}
								>
									{
										getFieldDecorator('name', { 
											rules: [{
												required: true, 
												message: '请输入学员姓名' 
											}],
											initialValue:name
										})
										(
											<Input
												placeholder="请输入学员姓名" 
											/>
										)
									}
								</FormItem>
	        				</Col>
	        				<Col span={ 12 }>
	    						<FormItem
									label="性别"
									{...formItemLayout}
								>
									{
										getFieldDecorator('gender', {
											rules: [{ 
												required: true, 
												message: '请选择性别' 
											}],
											initialValue:gender || '1'
										})(
											<RadioGroup>
										        <Radio value='1'>男</Radio>
										        <Radio value='2'>女</Radio>
										     </RadioGroup>
										)
									}
								</FormItem>
	    					</Col>
	    				</Row>
	    				<Row gutter={ 40 }>
		    				<Col span={12}>
		    					<FormItem
									label="学员生日"
									{...formItemLayout}
								>
									{
										getFieldDecorator('birthday', {
											rules: [{  
												required:false,
												message: '请选择学员生日' 
											}],
											initialValue: birthday
										})(
											<DatePicker
												style={{width:'100%'}}
												placeholder="请选择学员生日" 
											/>
										)
									}
								</FormItem>
		    				</Col>
		    				<Col span={ 12 }>
								<FormItem
									label="母亲电话"
									{...formItemLayout}
								>
									{
										getFieldDecorator('motherPhone', {
											rules: [{ 
												required:true,
												message: '电话号码输入有误',
												pattern: /^1[0-9]\d{4,9}$/ 
											}],
											initialValue:motherPhone
										})(
											<Input
												placeholder="请输入母亲电话" 
											/>
										)
									}
								</FormItem>
							</Col>
						</Row>
						<Row gutter={ 40 }>
		    				<Col span={ 12 }>
								<FormItem
									label="母亲职业"
									{...formItemLayout}
								>
									{
										getFieldDecorator('motherJob', { 
											rules: [{
												required: false, 
												message: '请输入母亲职业' 
											}],
											initialValue:motherJob
										})
										(
											<Input
												placeholder="请输入母亲职业" 
											/>
										)
									}
								</FormItem>
							</Col>
		    	        	<Col span={ 12 }>
		    	        		<FormItem
		    	        			label="父亲电话"
		    	        			{...formItemLayout}
		    	        		>
		    	        			{
		    	        				getFieldDecorator('fatherPhone', {
		    	        					rules: [{  
		    	        						message: '电话号码输入有误',
		    	        						pattern: /^1[0-9]\d{4,9}$/  
		    	        					}],
		    	        					initialValue:fatherPhone
		    	        				})(
		    	        					<Input
		    	        						placeholder="请输入父亲电话" 
	    	        						/>
		    	        				)
		    	        			}
		    	        		</FormItem>
		    	        	</Col>
		    	       	</Row>
		    	       	<Row gutter={ 40 }>
    	    				<Col span={ 12 }>
    							<FormItem
    								label="父亲职业"
    								{...formItemLayout}
    							>
    								{
    									getFieldDecorator('fatherJob', { 
    										rules: [{
    											required: false, 
    											message: '请输入父亲职业' 
    										}],
    										initialValue:fatherJob
    									})
    									(
    										<Input
    											placeholder="请输入父亲职业" 
    										/>
    									)
    								}
    							</FormItem>
    						</Col>
		    	        	<Col span={12}>
		    					<FormItem
									label="其他电话"
									{...formItemLayout}
								>
									{
										getFieldDecorator('otherPhone', {
											rules: [{  
												message: '请输入其他电话' ,
												pattern: /^1[0-9]\d{4,9}$/ 
											}],
											initialValue:otherPhone
										})(
											<Input
												placeholder="请输入其他电话" 
											/>
										)
									}
								</FormItem>
		    				</Col>
		    	        </Row>
					</Col>
					<Col span={9}>
						<FormItem
							label="上传照片"
							{...formItemLayout}
						>
							{
								getFieldDecorator('resourceId', {
									initialValue:resourceId
								})(
									<AvatarUpload 
										imageUrl={headImgUrl}
										onSuccess={ 
											(v)=> {
												let {
													id,
													url
												} = v;
												this.props.form.setFieldsValue({
													resourceId: id
												});
											}
										}
									/>
								)
							}
						</FormItem>
					</Col>
				</Row>
        	</div>
	        <div className="f-pd5">
    			<h3 className="f-mb3">扩展信息</h3>
		        <Row gutter={ 40 }>
		        	<Col span={ 15 }>
		        		<Row gutter={ 40 }>
		    				<Col span={ 12 }>
								<FormItem
									label="市场来源"
									{...formItemLayout}
								>
									{
										getFieldDecorator('recruitSource', {
											rules: [{
												required:true,
												message: '请选择' 
											}],
											initialValue:recruitSource
										})(
											<Select
												size="large"
												allowClear
			                					placeholder="请选择"
											>
												{
													fromData.map((d,i)=> {
														return(<Option 
																key={i} 
																value={d.value}
															>{d.label}</Option>);
													})
												}
											</Select>
										)
									}
								</FormItem>
							</Col>
							<Col span={ 12 }>
								<FormItem
									label="课程顾问"
									{...formItemLayout}
								>
									{
										getFieldDecorator('salesId', {
											rules: [{  
												required:true,
												message:'请选择'
											}],
											initialValue:salesId
										})(
											<SelectPerson 
												url='/api/hr/staff/listStaffByCondForDropDown'
												data={
													{ 
														type:1
													}
												} 
												onSelect={ 
													(v)=> {
														this.props.form.setFieldsValue({
															salesId: v
														});
													} 
												}
												initialValueData={salesId}
											/>
										)
									}
								</FormItem>
							</Col>
		        			<Col span={ 12 }>
	    						<FormItem
									label="学校性质"
									{...formItemLayout}
								>
									{
										getFieldDecorator('isPublicSchool', {
											rules: [{ 
												required: false, 
												message: '请选择学校性质' 
											}],
											initialValue:isPublicSchool || '1'
										})(
											<RadioGroup>
										        <Radio value='1'>公立</Radio>
										        <Radio value='0'>私立</Radio>
										    </RadioGroup>
										)
									}
								</FormItem>
	    					</Col>
		    				<Col span={ 12 }>
								<FormItem
									label="就读学校"
									{...formItemLayout}
								>
									{
										getFieldDecorator('currentSchoolName', { 
											rules: [{  
												required: false, 
												message: '请输入学校名称',
											}],
											initialValue:currentSchoolName
										})
										(
											<Input placeholder="请输入学校名称" />
										)
									}
								</FormItem>
							</Col>
		        		</Row>
		        		<Row gutter={ 40 }>
		        			<Col span={ 12 }>
	    						<FormItem
									label="年级"
									{...formItemLayout}
								>
									{
										getFieldDecorator('grade', {
											rules: [{
												required:false,
												message: '请选择年级' 
											}],
											initialValue:grade
										})(
											<Select
												size="large"
												allowClear
			                					placeholder="请选择年级"
											>
												{
													gradeList.map((d,i)=> {
														return(<Option 
																key={i} 
																value={d.value}
															>{d.label}</Option>);
													})
												}
											</Select>
										)
									}
								</FormItem>
	    					</Col>
	    					<Col span={ 12 }>
								<FormItem
									label="居住区域"
									{...formItemLayout}
								>
									{
										getFieldDecorator('citySelect', {
											rules: [{ 
												required: false, 
												message: '请输入居住区域' 
											}],
											initialValue:citySelect
										})(
											<SelectCity 
												onSelect={
													(v) => {
														this.props.form.setFieldsValue({
															citySelect: v
														});
													}
												}
												initialValueData={ citySelect }
											/>
										)
									}
								</FormItem>
							</Col>
		        		</Row>
		        		<Row gutter={ 4 } style={{position:'relative',left:'-6px',marginRight:'-8px'}}>
		    				<Col>
								<FormItem
									label="详细地址"
									labelCol={
										{ span:4 }
									}
									wrapperCol={
										{ span: 20 }
									}
								>
									{
										getFieldDecorator('address', {
											rules: [{ 
												required: false, 
												message: '请输入详细地址' 
											}],
											initialValue:address
										})(
											<Input placeholder="请输入详细地址" />
										)
									}
								</FormItem>
							</Col>
		        		</Row>
		        	</Col>
    	        </Row>
	        </div>
      	</Form>);
	}
	_checkStuNo(rule, value, callback) {
		let url = '/api/reception/student/checkStudentNumberExist';
		let mes = '您输入的学号已经有人用了,换一个吧!';
		let param = {
			stuNo: value
		}
		fpost(url, param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					callback(mes);
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				callback();
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_checkStudent() { // 直接报名时，根据姓名和手机号检测是否存在学员或者潜客
		let signData = window.store.session('signData');
		let {
			id, //潜客id，或者学员id，根据type区分
			type, //1,是学员,2潜客
			signType, //1,单报，2，连报
			school //校区id
		} = signData;

		let param = this.props.form.getFieldsValue(['name', 'motherPhone', 'fatherPhone', 'otherPhone']);
		param = undefinedToEmpty(param);
		let {
			name,
			motherPhone,
			fatherPhone,
			otherPhone
		} = param;

		if (type || !name || (!motherPhone && !fatherPhone && !otherPhone)) {
			return;
		}

		fpost('/api/reception/student/checkStudentInfo', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					// message.error(res.message || '系统错误');
					if (!res || !res.result) {
						return;
					}
					let {
						name,
						phone
					} = res.result;

					Modal.confirm({
						title: '温馨提醒',
						content: res.message,
						okType: 'danger',
						okText: '前往查看',
						cancelText: '返回编辑',
						onOk() {
							window.location.href = `/front/studentslist?name=${name}&phone=${phone}`
						},
						onCancel() {
							// console.log('Cancel');
						},
					});
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.catch((err) => {
				console.log(err);
			});
	}
}



const MyForm = Form.create()(MainForm);
export default MyForm;