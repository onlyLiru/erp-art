import React, {
	Component
} from 'react';
import $ from 'jquery';
import {
	Input,
	Button,
	Icon,
	Col,
	Row,
	Form,
	DatePicker,
	Select
} from 'antd';
import RangeDate from '../../common/rangeDate.js';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import SelectCourses from '../../common/selectCourses.js';
import SelectSchool from '../../common/selectSchool.js';
import {
	gradeList,
	fromData
} from '../../common/staticData.js';
import {
	undefinedToEmpty
} from '../../common/g.js';
const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isAdvancedSearch: false
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const {
			isAdvancedSearch
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form>
				<Row gutter={ 40 }>
    				<Col span={8}>
    					<FormItem
							label="学员姓名"
							{...formItemLayout}
						>
							{
								getFieldDecorator('name', { 
									rules: [{
										required: false, 
										message: '请输入学员姓名' 
									}] 
								})
								(
									<Input placeholder="请输入学员姓名" />
								)
							}
						</FormItem>
    				</Col>
    				<Col span={8}>
    					<FormItem
							label="就读校区"
							{...formItemLayout}
						>
							{
								getFieldDecorator('schoolAreaId', { 
									rules: [{
										required: false, 
										message: '请选择校区' 
									}],
									initialValue:''
								})
								(
									<SelectSchool 
										onSelect={
											(v) => {
												this.props.form.setFieldsValue({
													schoolAreaId:v
												});
											}
										}
										width='100%' 
										url="/api/system/schoolarea/listSchoolArea"
									/>
								)
							}
						</FormItem>
    				</Col>
    				<Col span={ 8 }>
						<FormItem
							label="课程"
							{...formItemLayout}
						>
							{
								getFieldDecorator('courseId', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<SelectCourses 
										onSelect= {
											(v)=> {
												this.props.form.setFieldsValue({
													courseId:v
												});
											}
										}
									/>
								)
							}
						</FormItem>
					</Col>
					<Col span={8}>
    					<FormItem
							label="市场来源"
							{...formItemLayout}
						>
							{
								getFieldDecorator('source', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<Select
										size="large"
										allowClear
	                					placeholder="请选择"
									>
										{
											fromData.map((d,i)=> {
												return(<Option 
														key={i} 
														value={d.value}
													>{d.label}</Option>);
											})
										}
									</Select>
								)
							}
						</FormItem>
    				</Col>
    				<Col span={ 8 }>
						<FormItem
							label="课程顾问"
							{...formItemLayout}
						>
							{
								getFieldDecorator('salesId', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<SelectPerson
										url='/api/hr/staff/listStaffByCondForDropDown'
										data={
											{ 
												type:2
											}
										} 
										onSelect={ 
											(v)=> {
												v = v ? v : null;
												this.props.form.setFieldsValue({
													salesId: v
												});
											} 
										}
									/>
								)
							}
						</FormItem>
					</Col>
    	        </Row>
    	        <div className="f-align-center">
    	        	<Button 
    	        		size="large" 
    	        		className="f-mr4" 
    	        		onClick={ this._reset.bind(this) }
    	        	>重置</Button>
    	        	<Button 
    	        		type="primary"
    	        		size="large" 
    	        		icon="search" 
    	        		onClick={ this._search.bind(this) }
    	        	>查询</Button>
    	        </div>
	      	</Form>
		</header>);
	}
	componentDidMount() {
		this._changeSearchType();
	}
	_changeSearchType() {
		const self = this;
		$('#J-searchTab li').on('click', function(e) {
			let type = $(this).attr('data-type');
			let isAdvancedSearch = type == 1 ? true : false;

			$(this).addClass('cur f-blue').siblings().removeClass('cur f-blue');

			self.setState({
				isAdvancedSearch
			});
		});
	}
	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			isAdvancedSearch: false
		}, () => {
			this.setState({
				isAdvancedSearch: true
			}, () => {
				this._search();
			})
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;