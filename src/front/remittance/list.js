import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../common/io.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">学员列表</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 1116}}
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
				  />
			</div>
    );
  }

}

const columns = [{
  title: '学员姓名（学号）',
  dataIndex: 'name',
  width: '160px',
  className: 'f-align-center',
  fixed: 'left',
  render: (value, row, index) => {
    let {
      name,
      stuNo
    } = row;
    return <a href={`/front/studenthome?studentId=${row.id}`}
      className="f-line1 f-over-hide"
      style={{
        width:'160px'
      }}
      title={`${name}(${stuNo})`}
    >
  		{ name }({stuNo})
  	</a>
  }
}, {
  title: '头像',
  dataIndex: 'headImgUrl',
  className: 'f-align-center',
  width: '76px',
  render: (value, row, index) => {
    let {
      headImgUrl
    } = row;
    return (<div className="f-list-avatar">
        <img src= {value}/> 
      </div>);
  }
}, {
  title: '性别',
  dataIndex: 'genderStr',
  className: 'f-align-center',
  width: '60px'
}, {
  title: '生日',
  dataIndex: 'birthday',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '家长手机号码',
  dataIndex: 'phone',
  className: 'f-align-center',
  width: '160px'
}, {
  title: '余额',
  dataIndex: 'balanceAmount',
  className: 'f-align-center',
  width: '160px'
}, {
  title: '欠费金额',
  dataIndex: 'totalArrearAmount',
  className: 'f-align-center',
  width: '160px'
}, {
  title: '市场来源',
  dataIndex: 'sourceStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '课程顾问',
  dataIndex: 'salesName',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  width: '100px',
  className: "f-align-center",
  render: (value, row, index) => {
    let {
      potentialStudentId,
      name,
      phone
    } = row;
    return (<div>
  		<Dropdown 
        overlay={
          <Menu>
            <Menu.Item key="1">
              <a href={`/front/selectclass?studentId=${row.id}`}>报名</a>
            </Menu.Item>
            {
              potentialStudentId ? 
                <Menu.Item key="2">
                  <a href={`/market/client/viewCommunication?potentialStudentId=${potentialStudentId}&motherPhone=${phone}&potentialStudentName=${name}`}>售前沟通</a>
                </Menu.Item>
              : null
            }
            <Menu.Item key="3">
              <a href={`/front/newstudent?studentId=${row.id}`}>编辑</a>
            </Menu.Item>
          </Menu>
        }>
        <a>
          操作<Icon type="down" />
        </a>
      </Dropdown>
  	</div>);
  }
}];