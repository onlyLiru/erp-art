import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message,
  Progress,
  Modal,
  Popover
} from 'antd';
import {
  fpost,
  fpostArray
} from '../../common/io.js';
import {
  getUnicodeParam
} from '../../common/g.js';
import {
  default as MangeClassTimes
} from '../../common/manageClassTimes/main.js'
let ID = getUnicodeParam('studentId');

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: false,
      saveing: false,
      rulesList: []
    }
  }
  render() {
    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile,
      onDelete,
      onSelectRow
    } = this.props;

    let {
      visibleModal,
      saveing
    } = this.state;

    const columns = [{
      title: '班级名称',
      dataIndex: 'name',
      width: '150px',
      render: function(value, row, index) {
        let {
          id,
          name
        } = row;
        return (<div title={name} className="f-line1" style={{width:'150px'}}>
          <a href={`/teach/classInfo?classId=${id}`}>{name}</a>
        </div>);
      }
    }, {
      title: '校区名称',
      dataIndex: 'schoolAreaName',
      width: '150px',
      render:function(value, row, index) {
        return(<div title={value} className="f-line1" style={{width:'150px'}}>
            {value}
          </div>);
      }
    }, {
      title: '班级状态',
      dataIndex: 'stateStr',
      width: '200px'
    }, {
      title: '课程',
      dataIndex: 'courseName',
      width: '100px',
      render:function(value, row, index) {
        return(<div title={value} className="f-line1" style={{width:'100px'}}>
            {value}
          </div>);
      }
    }, {
      title: '上课时段',
      dataIndex: 'ruleStrList',
      width: '200px',
      render:function(value, row, index) {
        return(<div className="f-line1" style={{width:'200px'}}>
              <Popover 
                placement="topLeft"
                content={
                  <div>
                    {
                      (value || []).map((d,i)=> {
                         return <p key={i}>{d}</p>
                      })
                    }
                  </div>
                }>
                {value}
              </Popover>
          </div>);
      }
    }, {
      title: '学费(¥)',
      dataIndex: 'salePrice',
      width: '100px'
    }, {
      title: '上课老师',
      dataIndex: 'teacherName',
      width: '150px'
    }, {
      title: '教室',
      dataIndex: 'roomName',
      width: '150px'
    }, {
      title: '在读人数',
      dataIndex: 'currentStudents',
      className: 'f-align-center',
      width: '100px',
      render: function(value, row, index) {
        let {
          currentStudents,
          rated
        } = row;

        currentStudents = parseInt(currentStudents)
        rated = parseInt(rated)

        let percent = currentStudents <= 0 ? 0 : (Math.round(currentStudents / rated * 10000) / 100);

        return (<div>
          <Progress 
            type="circle" 
            width={60}
            percent={percent} 
            format={percent => `${currentStudents}/${rated}`}
          />
        </div>);
      }
    }, {
      title: '操作',
      dataIndex: 'des',
      fixed: 'right',
      width: '100px',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          id
        } = row;

        return (<a href={`/front/rollClassSignup?studentId=${ID}&classId=${id}`}>
          选择
        </a>);
      }
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">班级列表</span>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 1400}}
            pagination={ pagination }
            loading={loading}
            bordered
            onSelect={onChange}
				  />

          <Modal title={`排课`}
            visible={ visibleModal }
            onOk={ this._onOk.bind(this) }
            confirmLoading={ saveing }
            onCancel={ this._onCancel.bind(this) }
            width="800px"
          >
            <MangeClassTimes
              getTimes={ this._getTimes.bind(this) }
              ref={ form => this.form=form }
            />
          </Modal>
			</div>
    );
  }
  _getTimes(rulesList) {
    console.log(rulesList);
    this.setState({
      rulesList
    });
  }
  _onOk() {
    this.form.validateFields((err, values) => {
      if (!err) {
        this._initParam(values);
      }
    });
  }
  _initParam(param) {
    console.log(param);
    let data;
    let ruleList=[];
    let {
      keys,
      teacherId,
      roomId
    } = param;
    data={
      classId:this.state.classId,
      teacherId,
      roomId,
      ruleList
    }
    keys.map((k,index)=> {
      let weekday = param[`weekday-${k}`];
      weekday = weekday.join();
      let startHour = param[`startHour-${k}`];
      let startMinutes = param[`startMinutes-${k}`];
      let startTime = startHour + ':' + startMinutes;
      let endHour = param[`endHour-${k}`];
      let endMinutes = param[`endMinutes-${k}`];
      let endTime = endHour  + ':' +  endMinutes;
      ruleList.push({
        weekday:weekday,
        startTime:startTime,
        endTime:endTime
      });
    });
    this._save(data);
  }
  _onCancel() {
    this.setState({
      visibleModal: false
    });
  }
  _save(data) {
    console.log(data);
    this.setState({
      saveing: true
    });

    let {
      onSaveOk
    } = this.props;

    fpostArray('/api/educational/modifyClassTimes', data)
      .then(res => res.json())
      .then((res) => {
        if (!res.success) {
          this.setState({
            saveing: false
          });
          message.error(res.message || '系统错误');
          throw new Error(res.message || '系统错误');
        };
        return (res);
      })
      .then((res) => {
        message.success(res.message);
        this.setState({
          saveing: false,
          visibleModal: false
        }, this.props.getData);
      })
      .catch((err) => {
        this.setState({
          saveing: false
        });
        console.log(err);
      });
  }
}