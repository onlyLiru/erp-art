import React, {
	Component
} from 'react';
import store from 'store2';
import querystring from 'querystring';
import {
	Breadcrumb,
	Modal,
	message
} from 'antd';
import {
	default as MyList
} from './list.js';
import {
	default as SearchForm
} from './searchform.js';
import {
	default as StudentInfo
} from './info.js';
import {
	fpost,
	host
} from '../../common/io.js';
import {
	numberFormate,
	getUnicodeParam
} from '../../common/g.js';
import '../main.less';
const confirm = Modal.confirm;
let sessionId = store.get('sessionId');
let ID = getUnicodeParam('studentId');

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			studentInfo:{},
			result: null,
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {
				chargeMode: 2,
				studentId:ID
			},
			classIds: []
		}
	}
	render() {
		let {
			result,
			studentInfo,
			loading,
			pagination
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
			    <Breadcrumb.Item>滚动班管理</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">班级选择</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<StudentInfo 
				data={studentInfo}
			/>

			<SearchForm
				onSearch= { this._search.bind(this) }
			/>

			<MyList 
				data={result}
		        loading={loading}
		        onChange={this._getList.bind(this)}
		        onDelete={this._onDelete.bind(this)}
		        pagination={pagination}
		        onSelectRow = { this._onSelectRow.bind(this) }
		        exportFile = { this._exportFile.bind(this) }
		        getData= { this._getList.bind(this) }
			/>

		</section>);
	}
	componentDidMount() {
		this._getList();
		this._getData();
	}
	_getData() {
		fpost('/api/reception/student/findStudent', {
				studentId: ID
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					// message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					studentInfo:result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_search(values) { //搜索
		// console.log(values);
		this.setState({
			searchParma: {
				...this.state.searchParma,
				...values
			},
			currentPage: 1,
			pagination: {
				...this.state.pagination,
				current: 1,
			}
		}, () => {
			this._getList();
		});
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			searchParma
		} = this.state;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		searchParma.pageSize = pageSize;
		searchParma.currentPage = currentPage;

		fpost('/api/educational/pageClass', searchParma)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result || !res.result.records) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records || [];
				let total = Number(result.total);

				data.map((d, i) => d.key = i);

				this.setState({
					result: data,
					pagination: {
						...pagination,
						total: total,
						current: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				});
			})
			.catch((err) => {
				this.setState({
					loading: false
				});
			});
	}
	_onDelete(classId) {
		let self = this;
		confirm({
			title: '确定要删除吗?',
			onOk() {
				fpost('/api/educational/class/delete', {
						classId
					})
					.then(res => res.json())
					.then((res) => {
						if (!res.success) {
							message.error(res.message || '系统错误');
							throw new Error(res.message || '系统错误');
						};
						return (res);
					})
					.then((res) => {
						message.success(res.message || '删除成功!');
						self._getList();
					})
					.catch((err) => {
						console.log(err);
					});
			},
			onCancel() {

			},
		});
	}
	_onSelectRow(record, selected, selectedRows) {
		console.log(selectedRows);
		this.setState({
			classIds: selectedRows
		});

	}
	_exportFile() {
		// let {
		// 	searchParma
		// } = this.state;
		// searchParma = querystring.stringify(searchParma);
		let classIds = this.state.classIds;
		if (!classIds || !classIds.length) {
			message.error('请选择要导出的数据...');
			return;
		}
		let ids = [];
		classIds.map((d, i) => {
			ids.push(d.id);
		});

		let url = `${host}/api/educational/exportClassAndCurrentStudents?x-auth-token=${sessionId}&classIds=${ids}`;
		window.location.href = url;
	}
}