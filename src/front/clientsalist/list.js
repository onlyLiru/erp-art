import React, {
  Component
} from 'react';
import {
  Table,
} from 'antd';
import {
  Link
} from 'react-router-dom';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">外部人员售卖列表</span>
        </h3>
        <Table
            columns={ columns }
            dataSource={ data }
            scroll={{ x: 1250}}
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
          />
      </div>
    );
  }

}

const columns = [{
  title: '购买人姓名',
  dataIndex: 'receiver',
  width: '150px',
  className: "f-align-center",
  fixed: 'left',
  render: (value, row, index) => {
    let {
      receiver
    } = row;
    return <div 
      className="f-line1 f-over-hide"
      title={ receiver }
    >
      { receiver }
    </div>
  }
}, {
  title: '手机号码',
  className: "f-align-center",
  dataIndex: 'receivePhone',
  width: '150px'
}, {
  title: '订单号',
  className: "f-align-center",
  dataIndex: 'orderNo',
  width: '200px'
}, {
  title: '订单名称',
  className: "f-align-center",
  dataIndex: 'name',
  width: '150px'
}, {
  title: '总金额',
  className: "f-align-center",
  dataIndex: 'totalAmount',
  width: '100px'
}, , {
  title: '操作人',
  className: "f-align-center",
  dataIndex: 'operatorName',
  width: '100px'
}, {
  title: '操作时间',
  className: "f-align-center",
  dataIndex: 'modifyTime',
  width: '200px'
}, {
  title: '操作',
  dataIndex: 'des',
  width: '200px',
  className: "f-align-center",
  fixed: 'right',
  render: (value, row, index) => {
    return (<div>
      <Link className="ant-btn f-radius3 f-lh4 f-mr3" to={`/front/saledetail?id=${row.id}`}>查看</Link>
      <Link className="ant-btn f-radius3 f-lh4" to={`/print?orderInfoId=${row.id}&type=2`}>打印凭证</Link>
    </div>);
  }
}];