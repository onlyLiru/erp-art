import React, {
	Component
} from 'react';
import {
	Button,
	Breadcrumb,
	message,
	Modal
} from 'antd';
import printJS from 'print-js';
import moment from 'moment';
import {
	Link
} from 'react-router-dom';
import {
	default as EditForm
} from './edit.js';
import {
	default as ViewInfo
} from './view.js';
import {
	default as CourseInfo
} from './courseInfo.js';
import {
	default as AtherInfo
} from './otherInfo.js';
import {
	fpost
} from '../../../common/io.js';
import {
	Loading,
	NoData,
	getUrlParam,
	undefinedToEmpty
} from '../../../common/g.js';

import './main.less';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editng: false,
			saveing: false,
			loading: false,
			result: null,

			printData: null,
			showPrintModal: false
		}
	}
	render() {
		let {
			editng,
			saveing,
			loading,
			result,
			printData,
			showPrintModal
		} = this.state;

		if (loading) {
			return <Loading />
		}

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>学员列表</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">学员详情</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-clear f-mb3">
				<span className="f-title-blue f-mt2">基本信息</span>
				<div className="f-right">
					{
						editng ?
						(<span>
							<Button 
							onClick={
								()=> {
									this.setState({ editng:false });
								}
							}
							className="f-mr2">取消</Button>
							<Button 
								type="primary"
								loading = { saveing }
								onClick={this._ok.bind(this)}
							>保存</Button>
						</span>)
						: 
						(
							<Button
							onClick={
								()=> { this.setState({ editng:true }) }
							} 
							type="primary">编辑</Button>
						)
					}
				</div>
			</div>

			{
				editng ? 
					<EditForm 
						ref={form => this.eForm=form }
						data={result}
					/>
				:
					<ViewInfo 
						data={result}
						onRechargeOk= {//当充值成功，更新页面
							(id)=> {
								let studentId = getUrlParam('studentId');
								this._getData();
								window.location.href=`/print?id=${id}&studentId=${studentId}&type=0`
								// this._getPrintData(id);
							}
						}
						onDrawMoneyOk= {//当提现成功，更新页面
							()=> {
								this._getData();
							}
						}
					/>
			}

			<CourseInfo data={result} />

			{
				printData ? 
					<Modal
						title="打印凭证"
						width="70%"
						visible={showPrintModal}
						okText="打印"
						onCancel={
							()=> {
								this.setState({
									showPrintModal:false
								});
							}
						}
						onOk={
							()=> {
								printJS({ 
									printable: 'J-print-content', 
									type: 'html', 
									documentTitle:'米诺瓦美术国际教育',
									honorColor:true
								});
							}
						}
					>
						<div id="J-print-content" className="f-print-table">
					      	<table>
					      		<caption>{printData.schoolGroupName}充值业务凭证</caption>
					      		<thead>
					      			<tr>
					      				<th>业务类型</th>
					      				<th>学员姓名(学号)</th>
					      			</tr>
					      		</thead>
					      		<tbody>
					      			<tr>
						      			<td>余额充值</td>
						      			<td>{printData.name}({printData.stuNo})</td>
					      			</tr>
					      			<tr>
						      			<td>充值: ¥{printData.amount}</td>
						      			<td>备注:{printData.foreignRemark}</td>
					      			</tr>
					      			<tr>
					      				<td colSpan="2">
								      		<p>（交易号：{printData.seriesNo}）</p>
					      				</td>
					      			</tr>
					      		</tbody>
					      	</table>
				      	</div>
					</Modal>
				: null
			}

		</section>);
	}
	componentDidMount() {
		this._getData();
	}
	_getData() {
		let studentId = getUrlParam('studentId');
		this.setState({
			loading: true
		});
		fpost('/api/reception/student/findStudentWithCourseCount', {
				studentId
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					result: res.result,
					loading: false
				});
			})
			.catch((err) => {
				this.setState({
					loading: false
				});
				console.log(err);
			});
	}
	_ok(e) {
		e.preventDefault();
		this.eForm.validateFields((err, values) => {
			if (!err) {
				// console.log(values);
				let {
					motherPhone,
					fatherPhone,
					otherPhone,
					birthday,
					citySelect
				} = values;
				birthday = birthday ? moment(birthday).format('YYYY-MM-DD') : '';
				let provinceId = citySelect[0];
				let cityId = citySelect[1];
				let districtId = citySelect[2];

				values.birthday = birthday;
				values.provinceId = provinceId;
				values.cityId = cityId;
				values.districtId = districtId;

				if (!motherPhone && !fatherPhone && !otherPhone) {
					message.error('至少要有一个电话号码!');
					return;
				}

				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;

		fpost('/api/reception/student/updateStudent', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					saveing: false,
					editng: false
				});
				this._getData();
				message.success('保存成功!');
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
	_getPrintData(id) {
		let studentId = getUrlParam('studentId');
		let studentBalanceFlowId = id;


		fpost('/api/reception/student/studentRecharge/print', {
				studentId,
				studentBalanceFlowId
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				console.log(res);
				this.setState({
					printData: res.result
				}, () => {
					this._print();
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_print() {
		this.setState({
			showPrintModal: true
		});
	}
}