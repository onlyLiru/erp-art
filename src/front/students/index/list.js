import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../../common/io.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange,
      exportFile
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">学员列表</span>
          <a 
            className="f-btn-green f-right"
            target="_blank"
            onClick={exportFile}
          >
            <Icon className="f-fz7 f-vertical-middle f-bold" type="plus" />导出学员名单
          </a>
        </h3>
				<Table
				    columns={ columns }
				    dataSource={ data }
            scroll={{ x: 1476}}
            pagination={ pagination }
            loading={loading}
            bordered
            onChange={onChange}
				  />
			</div>
    );
  }

}

const columns = [{
  title: '学员姓名（学号）',
  dataIndex: 'name',
  width: '160px',
  className: 'f-align-center',
  fixed: 'left',
  render: (value, row, index) => {
    let {
      name,
      stuNo
    } = row;
    return <div 
      className="f-line1 f-over-hide"
      style={{
        width:'160px'
      }}
      title={`${name}(${stuNo})`}
    >
  		{ name }({stuNo})
  	</div>
  }
}, {
  title: '头像',
  dataIndex: 'headImgUrl',
  className: 'f-align-center',
  width: '76px',
  render: (value, row, index) => {
    let {
      headImgUrl
    } = row;
    return (<div className="f-list-avatar">
        <img src= {value}/> 
      </div>);
  }
}, {
  title: '性别',
  dataIndex: 'genderStr',
  className: 'f-align-center',
  width: '60px'
}, {
  title: '年龄',
  dataIndex: 'age',
  className: 'f-align-center',
  width: '60px'
}, {
  title: '生日',
  dataIndex: 'birthday',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '手机号码',
  dataIndex: 'phone',
  className: 'f-align-center',
  width: '160px'
}, {
  title: '总欠费',
  dataIndex: 'totalArrearAmount',
  className: 'f-align-center',
  width: '160px'
}, {
  title: '余额',
  dataIndex: 'balanceAmount',
  className: 'f-align-center',
  width: '160px'
}, {
  title: '市场来源',
  dataIndex: 'sourceStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '课程顾问',
  dataIndex: 'salesName',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '操作人',
  dataIndex: 'operatorName',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '操作时间',
  dataIndex: 'modifyTime',
  className: 'f-align-center',
  width: '200px'
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  width: '100px',
  className: "f-align-center",
  render: (value, row, index) => {
    let {
      potentialStudentId,
      name,
      phone
    } = row;
    return (<div>
  		<Dropdown 
        overlay={
          <Menu>
            <Menu.Item key="1">
              <Link to={`/front/students/courses?studentId=${row.id}&name=${row.name}&stuNo=${row.stuNo}`}>全部课程</Link>
            </Menu.Item>
            {/*<Menu.Item key="2">
              <Link to="/front/students/before">售前沟通</Link>
            </Menu.Item>*/}
            {
              potentialStudentId ? 
                <Menu.Item key="2">
                  <Link to={`/market/client/viewCommunication?potentialStudentId=${potentialStudentId}&motherPhone=${phone}&potentialStudentName=${name}`}>售前沟通</Link>
                </Menu.Item>
              : null
            }
            <Menu.Item key="4">
              <Link to={`/front/students/info?studentId=${row.id}`}>学员信息</Link>
            </Menu.Item>
          </Menu>
        }>
        <Button type="primary" className="f-radius4">
          查看<Icon type="down" />
        </Button>
      </Dropdown>
  	</div>);
  }
}];