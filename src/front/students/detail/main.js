import React, {
    Component
} from 'react';
import {
    Input,
    InputNumber,
    Button,
    Icon,
    Col,
    Row,
    Card,
    Breadcrumb,
    Modal,
    Form,
    DatePicker,
    Select,
    Upload,
    message,
    Radio,
    Avatar,
    Tooltip
} from 'antd';
import {
    fpost
} from '../../../common/io.js';
import {
    Loading,
    NoData,
    getUrlParam,
    getUnicodeParam
} from '../../../common/g.js';
import {
    payTypes
} from '../../../common/staticData.js';
import SelectClassRoom from '../../../common/selectClassRoom.js';
import './main.less';

const FormItem = Form.Item;
const Option = Select.Option;
const {
    TextArea
} = Input;

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleClassRoomModal: false,
            visible: false,
            saveing: false,
            result: null,
            courseId: getUrlParam('id') || '',
            loading: false,
            returnType: 1,
            registrationMaterialId: '',
            myClassName: ''
        }
    }
    render() {
        let {
            visible,
            saveing,
            loading,
            result,
            visibleClassRoomModal,
            myClassName
        } = this.state;

        if (!result) {
            return null;
        }

        let {
            courseName,
            className,
            classStateStr,
            materialList,
            totalMaterialAmount,
            originalPrice,
            receivableAmount,
            actualAmount,
            arrearAmount,
            preferentialAmount,
            internalRemark,
            foreignRemark,
            isArrear,
            type,
            typeStr,
            currentPeriod,
            totalPeriod,
            state,
            courseId,
            schoolAreaId,
            preferentialReduceList,
            balanceReduce
        } = result;

        let discountTip = preferentialReduceList.map((d, i) => {
            return <p key={i}><span className="f-mr2">{d.name} :</span> ¥{d.amount}</p>
        });

        return (<div>
			<Breadcrumb className="f-mb3">
                <Breadcrumb.Item>前台业务</Breadcrumb.Item>
                <Breadcrumb.Item>学员管理</Breadcrumb.Item>
                <Breadcrumb.Item>学员列表</Breadcrumb.Item>
			    <Breadcrumb.Item>课程列表</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">
			   			课程详情
			   		</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

            {
                (loading && !result) ? <Loading />
                :
    			<div className="f-pd5 f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5 f-right-title-box">
    		        <div className="f-bg-blue f-right-title">
            			<h3>{typeStr}</h3>
            		</div>
            		<div className="f-pd5">
            			<Row gutter={24}>
            				<Col span={20}>
            					<ul className="f-lh8">
            						<li>
            							<span className="f-pale f-mr2">课程名称:</span>
            							{courseName}
                                        {
                                            isArrear=='1' ?
            							     <span className="f-round f-white f-bg-yellow f-right f-align-center" style={{width:'40px',height:'40px'}}>欠</span>
                                            : null
                                        }
            						</li>
            						<li>
            							<span className="f-pale f-mr2">班级信息:</span>
                                        {
                                            state == '3' ? 
                                            <span>
                                                <Button
                                                    onClick={
                                                        ()=> {
                                                            this.setState({
                                                                visibleClassRoomModal:true
                                                            });
                                                        }
                                                    }
                                                >
                                                    选择班级
                                                </Button>
                                                <span className="f-ml2">{ className }</span>
                                            </span>
                                            :
                							<span>
                                                {className}
                                                <span className="f-red f-ml2">{classStateStr}</span>
                                            </span>
                                        }
            						</li>
                                    <li style={{display:'flex'}}>
                                        <span className="f-pale f-mr2">物品信息:</span>
                                        <div>
                                        {
                                            materialList ? 
                        						materialList.map((d,i)=> {
                                                    let {
                                                        id,
                                                        salesPrice,
                                                        isRefund,
                                                        materialName,
                                                        quantity,
                                                        totalAmount,
                                                        isTakeAway
                                                    } = d;
                                                    return <p key={i}>
                                                        <span className="f-mr2">{materialName}</span>
                                                        <span className="f-mr2">x{quantity}</span>
                                                        <span className="f-ml2 f-mr2">¥{salesPrice}</span>
                                                        {
                                                            isRefund && isRefund == '0' && isTakeAway == 1 ?
                                                                <Button 
                                                                    className="f-mr5"
                                                                    type="primary"
                                                                    onClick={
                                                                        ()=> {
                                                                            this.setState({
                                                                                visible:true,
                                                                                registrationMaterialId:id
                                                                            });
                                                                        }
                                                                    }
                                                                >
                                                                    退货
                                                                </Button>
                                                            :
                                                                null
                                                        }
                                                        {
                                                            isRefund && isRefund != '0' ?
                                                                <span className="f-mr5 f-green">已退货</span>
                                                            :
                                                                null
                                                        }
                                                        {
                                                            isTakeAway && isTakeAway == '0' ?
                                                                <span className="f-mr5 f-red">未领取</span>
                                                            :
                                                                null
                                                        }
                                                    </p>
                                                })
                                            : null
                                        }
                                        </div>
                                    </li>
            						<li>
            							<span className="f-pale f-mr2">课程费用:</span>
            							<span className="f-mr4">原价：¥{originalPrice}</span>
            							<span className="f-ml2 f-mr4">学费：¥{receivableAmount}</span>
            							<span className="f-ml2 f-mr4">实收：¥{actualAmount}</span>
                                        <span className="f-ml2 f-mr4">欠费：¥{arrearAmount}</span>
            							<span className="f-ml2 f-mr4">余额抵用：¥{balanceReduce}</span>
            							<span className="f-ml2 f-mr4">
    								        <span className="f-mr2">优惠：¥{preferentialAmount}</span>
                                            {
                                                preferentialReduceList && preferentialReduceList.length ?
                                                    <Tooltip placement="bottom" title={discountTip}>
                                                        <Icon type="exclamation-circle-o" />
                                                    </Tooltip>
                                                : null
                                            }
            							</span>
            						</li>
            						<li>
            							<span className="f-pale f-mr2">物品费用:</span>
            							<span className="f-mr2">总计：¥{totalMaterialAmount}</span>
            						</li>
            						<li>
            							<span className="f-pale f-mr2">对内备注:</span>
            							<span className="f-mr2">{internalRemark}</span>
            						</li>
            						<li>
            							<span className="f-pale f-mr2">对外备注:</span>
            							<span className="f-mr2">{foreignRemark}</span>
            						</li>
            					</ul>
            				</Col>
            				<Col span={4}>
            					<dl className="f-align-center f-lh8 right-tip">
            						<dt>{typeStr}</dt>
            						<dd>
                                        {
                                            type=='2' ?
                                                <p>{currentPeriod}/{totalPeriod}期</p>
                                            : null
                                        }
            							<p>课程+物品</p>
            							<span className="f-red">合计：¥{ receivableAmount + totalMaterialAmount}</span>
            						</dd>
            					</dl>
            				</Col>
            			</Row>
            		</div>

                    <Modal
                        title="退货"
                        visible={this.state.visible}
                        onOk={this._handleOk.bind(this)}
                        confirmLoading={ saveing }
                        onCancel={this._handleCancel.bind(this)}
                        width='600px'
                    >
                        {
                            visible ?
                            <MyForm ref={form => this.form=form } />
                            : null
                        }
                    </Modal>

                    {/*选择班级*/}
                    {
                        visibleClassRoomModal ? 
                            <SelectClassRoom 
                                visible={ visibleClassRoomModal }
                                filterRegistrationFull={1}
                                schoolAreaId={schoolAreaId}
                                courseId={courseId}
                                handleOk= { this._selectClassRoomOk.bind(this) }
                                handleCancel= { this._selectClassRoomCancel.bind(this) }
                            />
                        : null
                    }

    	        </div>
            }
		</div>);
    }
    componentDidMount() {
        this._getData();
    }
    _getData() {
        let {
            courseId
        } = this.state;

        this.setState({
            loading: true
        });

        fpost('/api/reception/student/course/findRegistrationCourseInfoById', {
                id: courseId
            })
            .then(res => res.json())
            .then((res) => {
                if (!res.success || !res.result) {
                    this.setState({
                        loading: false
                    });
                    message.error(res.message || '系统错误');
                    throw new Error(res.message || '系统错误');
                };
                return (res.result);
            })
            .then((result) => {
                this.setState({
                    result,
                    loading: false
                });
            })
            .catch((err) => {
                console.log(err);

                this.setState({
                    loading: false
                });
            });
    }
    _handleCancel() {
        this.setState({
            visible: false
        });
    }
    _handleOk(e) {
        let {
            result,
            returnType,
            registrationMaterialId
        } = this.state;
        let {
            schoolAreaId,
            studentId,
        } = result;

        e.preventDefault();
        this.form.validateFields((err, values) => {
            if (!err) {
                let param = {};
                param = {
                    schoolAreaId,
                    studentId,
                    returnType,
                    registrationMaterialId,
                    ...values
                }
                console.log(param);
                this._returnGoods(param);
            }
        });
    }
    _returnGoods(param) { //退货
        this.setState({
            saveing: true
        });

        fpost('/api/reception/order/salesReturn', param)
            .then(res => res.json())
            .then((res) => {
                if (!res.success) {
                    this.setState({
                        saveing: false
                    });
                    message.error(res.message || '系统错误');
                    throw new Error(res.message || '系统错误');
                };
                return (res);
            })
            .then((res) => {
                this.setState({
                    saveing: false,
                    visible: false
                });
                message.success('退货成功!');
                this._getData();
            })
            .catch((err) => {
                this.setState({
                    saveing: false
                });
                console.log(err);
            });
    }
    _selectClassRoomCancel() { //取消选择班级
        this.setState({
            visibleClassRoomModal: false
        });
    }
    _selectClassRoomOk(v) { //已选择班级信息
        console.log('已选择班级信息', v);
        if (!v) {
            return;
        }
        let classGrade = v;
        let {
            courseId
        } = this.state;

        let {
            id,
            name,
        } = classGrade;

        this.setState({
            visibleClassRoomModal: false,
            classGrade: v,
            myClassName: name,
            classId: id
        });

        fpost('/api/reception/student/course/chooseClassForRegistrationCourse', {
                registrationCourseId: courseId,
                classId: id
            })
            .then(res => res.json())
            .then((res) => {
                if (!res.success) {
                    message.error(res.message || '系统错误');
                    throw new Error(res.message || '系统错误');
                };
                return (res);
            })
            .then((res) => {
                message.success('选择成功!');
                this._getData();
            })
            .catch((err) => {
                console.log(err);
            });
    }
}

/*退货表单*/
class MainForm extends React.Component {
    render() {
        const {
            getFieldDecorator
        } = this.props.form;

        const formItemLayout = {
            labelCol: {
                span: 5
            },
            wrapperCol: {
                span: 18
            }
        };

        return (
            <Form>

                <FormItem
                    label="退款金额"
                    {...formItemLayout}
                >
                    {
                        getFieldDecorator('refundAmount', { 
                            rules: [{
                                required: true, 
                                message: '退款金额' 
                            }] 
                        })
                        (
                            <InputNumber 
                                min={0} 
                            />
                        )
                    }
                </FormItem>
                <FormItem
                    label="退款方式"
                    {...formItemLayout}
                >
                    {
                        getFieldDecorator('refundWay', { 
                            rules: [{
                                required: true, 
                                message: '请选择退款方式' 
                            }] 
                        })
                        (
                            <Select style={{width:'160px'}} placeholder="请选择退款方式">
                                {
                                    payTypes.map((d,i)=> {
                                        return <Option key={i} value={d.value}>{d.label}</Option>
                                    })
                                }
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem
                    label="对内备注"
                    {...formItemLayout}
                >
                    {
                        getFieldDecorator('internalRemark', { 
                            rules: [{
                                required: false, 
                            }] 
                        })
                        (
                            <TextArea 
                                rows={2} 
                            />
                        )
                    }
                </FormItem>
                <FormItem
                    label="对外备注"
                    {...formItemLayout}
                >
                    {
                        getFieldDecorator('foreignRemark', { 
                            rules: [{
                                required: false, 
                            }] 
                        })
                        (
                            <TextArea 
                                rows={2} 
                            />
                        )
                    }
                </FormItem>

            </Form>
        );
    }
}

const MyForm = Form.create()(MainForm);