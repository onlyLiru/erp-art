import React, {
	Component
} from 'react';
import {
	Table,
	Icon,
	Button
} from 'antd';
import {
	Link
} from 'react-router-dom';
import printJS from 'print-js';
import {
	payTypes
} from '../../common/staticData.js';

export default class Main extends Component {
	render() {

		let {
			data,
			loading,
			pagination,
			onChange
		} = this.props;

		return (
			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">今日收入</span>
        </h3>
        <div id="J-print-content">
	        <Table
	            columns={ columns }
	            bordered
	            dataSource={ data }
	            pagination={ false }
	            loading={loading}
	            onChange={onChange}
	        />
        </div>
        <br />
        <div className="f-align-right">
	        <Button
				size = "large"
				type = "primary"
				onClick = {
					() => {
						printJS({
							printable: 'J-print-content',
							type: 'html',
							documentTitle: '米诺瓦美术国际教育',
							honorColor: true
						});
					}
				} 
			>
				打印 
			</Button>
		</div>
      </div>
		);
	}

}

const columns = [{
 title: '业务类型',
 dataIndex: 'businessCodeStr',
}, {
 title: `${payTypes[0].label}`,
 dataIndex: 'cashAmount',
}, {
 title: `${payTypes[1].label}`,
 dataIndex: 'wxpayAmount',
}, {
 title: `${payTypes[2].label}`,
 dataIndex: 'alipayAmount',
}, {
 title: `${payTypes[3].label}`,
 dataIndex: 'cardAmount',
}, {
 title: `${payTypes[4].label}`,
 dataIndex: 'transferAmount',
}, {
 title: `${payTypes[5].label}`,
 dataIndex: 'creditCardAmount',
}, {
 title: `${payTypes[6].label}`,
 dataIndex: 'onlineAlipayAmount',
}];