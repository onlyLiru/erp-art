import React, {
	Component
} from 'react';
import {
	Col,
	Row,
	Form,
	Select,
	Radio,
	InputNumber,
	Input
} from 'antd';

import {
	payTypes,
	payTypesSub
} from '../../../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// isBalanceRefund: this.props.data.isBalanceRefund,
			tradeWay:'-1'
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			data
		} = this.props;
		let {
			amount,
			resultAmount
		} = data;
		let {
			tradeWay
		} = this.state;

		amount = amount ? amount : 0;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
	        <div className="f-pd5 f-right-title-box">
		        <div className="f-bg-green f-right-title">
        			<h3>费用信息</h3>
        		</div>
        		<Row gutter={16} className="f-mb5">
        			<Col span={2} className="f-align-right">应退: </Col>
        			<Col span={20}><i className="f-fz5 f-red">¥{resultAmount}</i></Col>
        		</Row>
        		<Row gutter={16} className="f-mb5">
        			<Col span={2} className="f-align-right f-lh6 ant-form-item-required">实退: </Col>
        			<Col span={20}>
	                    <FormItem>
		                    {
		                        getFieldDecorator('amount', { 
		                            rules: [{
		                                required: true, 
		                                message:'请输入实退金额'
		                            }],
		                            initialValue:amount
		                        })
		                        (
		                            <InputNumber
										formatter={value => `¥ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
										style={{ width:'100px' }} 
										size="large"
										min={0}
										max={resultAmount}
									/>
		                        )
		                    }
		                </FormItem>
        			</Col>
        		</Row>
        		<Row gutter={16} className="f-mb5">
        			<Col span={2} className="f-align-right f-lh6 ant-form-item-required">退课原因: </Col>
        			<Col span={20}>
	                    <FormItem
					        {...formItemLayout}
					    >
					        {
					            getFieldDecorator('reson', { 
					                rules: [{
					                    required: true, 
					                    message: '请输入退课原因' 
					                }] 
					            })
					            (
					                <Input />
					            )
					        }
					    </FormItem>
        			</Col>
        		</Row>
        		<Row gutter={16} className="f-mb5">
        			<Col span={2} className="f-align-right f-lh6 ant-form-item-required">退款方式: </Col>
        			<Col span={20}>
	                    <FormItem>
		                    {
						        getFieldDecorator('tradeWay', { 
						            rules: [{
						                required: true, 
						                message:'请选择退款方式'
						            }],
						            initialValue:'-1'
						        })
						        (
									<Select 
										size="large" 
										placeholder="请选择退款方式" 
										style={{width:'150px',marginRight:'-14px'}}
										onChange={
											(v)=> {
												this.setState({
													tradeWay:v
												});
											}
										}
									>
									    {
									        payTypesSub.map((d,i)=> {
									            return <Option key={i} value={d.value}>{d.label}</Option>
									        })
									    }
									</Select>
						        )
						    }
		                </FormItem>
        			</Col>
        		</Row>
        		{
					tradeWay == '7' ?
					<div>
						<Row gutter={16} className="f-mb5">
							<Col span={2} className="f-align-right ant-form-item-required">户名: </Col>
							<Col span={20}>
								<FormItem>
								    {
								        getFieldDecorator('cardName', { 
								            rules: [{
								                required: true, 
								                message: '请输入银行卡户名' 
								            }] 
								        })
								        (
								            <Input style={{width:'400px'}} placeholder="请输入银行卡户名" />
								        )
								    }
								</FormItem>
							</Col>
						</Row>
						<Row gutter={16} className="f-mb5">
							<Col span={2} className="f-align-right ant-form-item-required">银行: </Col>
							<Col span={20}>
								<FormItem>
				    		        {
				    		            getFieldDecorator('bankName', { 
				    		                rules: [{
				    		                    required: true, 
				    		                    message: '请填写银行支行' 
				    		                }] 
				    		            })
				    		            (
				    		                <Input style={{width:'400px'}} placeholder="请填写银行支行" />
				    		            )
				    		        }
				    		    </FormItem>
							</Col>
						</Row>
						<Row gutter={16} className="f-mb5">
							<Col span={2} className="f-align-right ant-form-item-required">银行账号:</Col>
							<Col span={20}>
								<FormItem>
		    		    		    {
				    		            getFieldDecorator('cardNum', { 
				    		                rules: [{
				    		                	whitespace: true,
				    		                    type:'number',
				    		                    transform(value) {
				    		                      if(value){
				    		                        return Number(value.replace(/ /g,''));
				    		                      }
				    		                    },
				    		                	message: '请输入正确的账号' 
				    		                },{
				    		                    required: true, 
				    		                    message: '请输入账号' 
				    		                }] 
				    		            })
				    		            (
				    		                <Input style={{width:'400px'}} placeholder="请输入提现账号" />
				    		            )
				    		        }
		    		    		</FormItem>
							</Col>
						</Row>
				    </div>
					: null
				}
	        </div>
        </div>);
	}
}