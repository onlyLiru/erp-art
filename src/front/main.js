import React, {
	Component
} from 'react';
import {
	Route
} from 'react-router-dom';
import {
	default as Layout
} from '../common/layout.js';
import './main.less';
import {
	default as StudentsList
} from './students/main'; //学生列表
import {
	default as AllStudents
} from './studentsList/main'; //学生列表(滚动班)
import {
	default as NewStudents
} from './newstudents/main'; //新生报名
import {
	default as OldStudents
} from './oldstudents/main'; //老生续报
import {
	default as ChangeClasses
} from './changeclass/main'; //办理转班
import {
	default as ShiftToClassList
} from './shiftToClassList/main'; //转入班级列表
import {
	default as PayForChangeClass
} from './payForChangeClass/main'; //转入班级结算
import {
	default as Moneyback
} from './moneyback/main'; //办理退费
import {
	default as ReturnMoney
} from './returnmoney/main'; //退差价
import {
	default as StopClasses
} from './stopclasses/main'; //办理停课
import {
	default as Fillmoney
} from './fillmoney/main'; //办补欠费
import {
	default as SaleList
} from './salelist/main'; //物品售卖
import {
	default as Studentsalist
} from './studentsalist/main'; //学员售卖列表
import {
	default as Clientsalist
} from './clientsalist/main'; //外部人员售卖列表
import {
	default as SaleGoods
} from './salegoods/main'; //物品售卖
import {
	default as SaleDetail
} from './saledetail/main'; //物品售卖详情
import {
	default as SignUp
} from './signup/main'; //报名
import {
	default as RollClassSign
} from './rollClassSignup/main'; //滚动班报名

import {
	default as DaylySummary
} from './dailySummary/main';

import {
	RegistrationCourse
} from './registrationCourse/main'; //报课列表

import {
	default as RegCourseList
} from './regCourseList/main'; //报课列表
import {
	default as AddNewStudent
} from './newStudent/main.js';//新建学员
import {
	default as SelectClass
} from './selectclass/main.js';//选择班级
import {
	default as StudentHome
} from './studentHome/main.js';//学员主页
import {
	default as CheckPage //审批
} from './check/main.js';

let PARENTPATH;

export default ({
	match
}) => {
	window.store.session('tMenu', {
		selectedKeys: 'qtyw'
	})

	PARENTPATH = match.path;

	return (<div>
		<Layout main={ Main } />
	</div>);
}

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		return (<div>
			<Route path={ PARENTPATH } exact component={ StudentsList } />
			<Route path={ `${ PARENTPATH }/students` } component={ StudentsList } />
			<Route path={ `${ PARENTPATH }/newstudents` } component={ NewStudents } />
			<Route path={ `${ PARENTPATH }/oldstudents` } component={ OldStudents } />
			<Route path={ `${ PARENTPATH }/changeclass` } component={ ChangeClasses } />
			<Route path={ `${ PARENTPATH }/shiftToClassList` } component={ ShiftToClassList } />
			<Route path={ `${ PARENTPATH }/payForChangeClass` } component={ PayForChangeClass } />
			<Route path={ `${ PARENTPATH }/moneyback` } component={ Moneyback } />
			<Route path={ `${ PARENTPATH }/returnmoney` } component={ ReturnMoney } />
			<Route path={ `${ PARENTPATH }/stopclasses` } component={ StopClasses } />
			<Route path={ `${ PARENTPATH }/fillmoney` } component={ Fillmoney } />
			<Route path={ `${ PARENTPATH }/salelist` } component={ SaleList } />
			<Route path={ `${ PARENTPATH }/studentsaleist` } component={ Studentsalist } />
			<Route path={ `${ PARENTPATH }/clientsaleist` } component={ Clientsalist } />
			<Route path={ `${ PARENTPATH }/salegoods` } component={ SaleGoods } />
			<Route path={ `${ PARENTPATH }/saledetail` } component={ SaleDetail } />
			<Route path={ `${ PARENTPATH }/signup` } component={ SignUp } />
			<Route path={ `${ PARENTPATH }/rollClassSignup` } component={ RollClassSign } />
			<Route path={ `${ PARENTPATH }/dailysummary` } component={ DaylySummary } />
			<Route path={ `${ PARENTPATH }/newstudent` } component={ AddNewStudent } />
			<Route path={ `${ PARENTPATH }/studentslist` } component={ AllStudents } />
			<Route path={ `${ PARENTPATH }/selectclass` } component={ SelectClass } />
			<Route path={ `${ PARENTPATH }/studenthome` } component={ StudentHome } />
			<Route path={ `${ PARENTPATH }/check` } component={ CheckPage } />
			{/*报课列表*/}
			<Route path={ `${ PARENTPATH }/registrationCourse` } component={ RegCourseList } />
		</div>);

	}
}