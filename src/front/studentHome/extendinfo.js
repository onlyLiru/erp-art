import React,{Component} from 'react';
import {
	Row,
	Col
} from 'antd';
export default class StudentInfo extends Component {
	constructor(props) {
		super(props);
		this.state={

		}
	}
	render() {
		let {
			data
		} = this.props;

		return(<div className="f-mb5">
			<ul className="f-clear info-list info-bd-top"  style={{width:'100%'}}>
				<li style={{width:'33%'}}>
					<span className="f-pale f-mr2">就读学校:</span>
					{data.currentSchoolName}
				</li>
				<li style={{width:'33%'}}>
					<span className="f-pale f-mr2">学校性质:</span>
					{data.isPublicSchoolStr ? '公立' : '私立'}
				</li>
				<li style={{width:'33%'}}>
					<span className="f-pale f-mr2">年级:</span>
					{data.gradeStr}
				</li>
				<li style={{width:'33%'}}>
					<span className="f-pale f-mr2">居住区域:</span>
					{data.provinceCityDistrict}
				</li>
				<li style={{width:'33%'}}>
					<span className="f-pale f-mr2">详细地址:</span>
					{data.address}
				</li>
			</ul>
		</div>);
	}
}