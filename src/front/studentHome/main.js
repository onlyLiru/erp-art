import React, {Component} from 'react';
import {
	Breadcrumb,
	Tabs
} from 'antd';
import {
	default as StudentInfo
} from './info.js';
import {
	default as ExtendsInfo
} from './extendinfo.js';
import {
	default as InfoBefore
} from './before/main.js';
import {
	fpost
} from '../../common/io.js';
import {
	numberFormate,
	getUnicodeParam
} from '../../common/g.js';

import {
	default as CoursesList
} from './courses/main.js';

const TabPane = Tabs.TabPane;
let ID = getUnicodeParam('studentId');

export default class Main extends Component {
	constructor(props){
		super(props);
		this.state={
			studentInfo:{},
			extendsInfo:{}
		}
	}
	render() {
		let {
			studentInfo,
			extendsInfo
		} = this.state;

		return(<div>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>学员管理</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">学员主页</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<StudentInfo 
				data={studentInfo}
				onRechargeOk= {//当充值成功，更新页面
					(id)=> {
						this._getBaseInfo();
						window.location.href=`/print?id=${id}&studentId=${ID}&type=0`
					}
				}
				onDrawMoneyOk= {//当提现成功，更新页面
					()=> {
						this._getBaseInfo();
					}
				}
			/>

			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-clear">
				<Tabs onChange={this._tabChange.bind(this)}>
				    <TabPane tab="报课信息" key="2">
				    	<CoursesList />
				    </TabPane>
				    <TabPane tab="扩展信息" key="1">
				    	<ExtendsInfo 
				    		data={extendsInfo}
				    	/>
				    </TabPane>
				</Tabs>
			</div>
		</div>);
	}
	componentDidMount() {
		this._getBaseInfo();
		this._getExtendInfo();
	}
	_tabChange(key) {
	  console.log(key);
	}
	_getBaseInfo() {
		fpost('/api/reception/student/findStudent', {
				studentId: ID
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					// message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					studentInfo:result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_getExtendInfo() {
		fpost('/rolling/findRollingStudentPlusVO', {
				studentId: ID
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					// message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					extendsInfo:result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
}