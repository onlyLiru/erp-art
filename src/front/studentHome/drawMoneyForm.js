import React, {
	Component
} from 'react';
import {
	InputNumber,
	Form,
	Select,
	Modal,
	message,
	Input,
	Row,
	Col
} from 'antd';
import {
	fpostArray
} from '../../common/io.js';
import {
	payTypes
} from '../../common/staticData.js';
import SelectSchool from '../../common/selectSchool.js';

const FormItem = Form.Item;
const Option = Select.Option;
const {
	TextArea
} = Input;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			studentId: this.props.studentId,
			saveing: false,
			trades: null,
			total: this.props.balanceAmount || 0
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visible,
			onCancel
		} = this.props;

		let {
			saveing,
			total,
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<Modal title="提现"
				visible={ visible }
				okText="申请提现"
				confirmLoading={ saveing }
				onOk={ this._ok.bind(this) }
				onCancel={ onCancel }
				width='60%'
			>
			<Form>
				<Row gutter={24}>
			    	<Col span={12}>
						    <FormItem
						        label="校区"
						        {...formItemLayout}
						    >
						        {
						            getFieldDecorator('schoolAreaId', { 
						                rules: [{
						                    required: true, 
						                    message: '请选择校区' 
						                }]
						            })
						            (
					                	<SelectSchool 
					                		width="100%"
											onSelect={ 
												(v)=> {
													this.props.form.setFieldsValue({
														schoolAreaId:v
													});
												}
											} 
										/>
						            )
						        }
						    </FormItem>
					    </Col>
				    </Row>
				    <Row gutter={24}>
				    	<Col span={12}>
						    <FormItem
						        label="提现金额"
						        {...formItemLayout}
						    >
						        {
						            getFieldDecorator('tradeAmount', { 
						                rules: [{
						                    required: true, 
						                    message: '请输入提现金额' 
						                }],
						                initialValue:total
						            })
						            (
					                	<InputNumber 
					                		min={0} 
					                		max={total} 
				                		/>
						            )
						        }
						        <span className="f-pale">{`最多可提现¥${total}`}</span>
						    </FormItem>
						</Col>
				    </Row>
				    <Row gutter={24}>
				    	<Col span={12}>
						    <FormItem
						        label="提现方式"
						        {...formItemLayout}
						    >
						        {
						            getFieldDecorator('trades', { 
						                rules: [{
						                    required: true, 
						                    message: '请选择提现方式' 
						                }],
						                initialValue:"7"
						            })
						            (
						                <Select placeholder="请选择提现方式">
						                    <Option value="7">银行转账</Option>
						                </Select>
						            )
						        }
						    </FormItem>
					    </Col>
					    <Col span={12}>
						    <FormItem
						        label="户名"
						        {...formItemLayout}
						    >
						        {
						            getFieldDecorator('cardName', { 
						                rules: [{
						                    required: true, 
						                    message: '请输入银行卡户名' 
						                }] 
						            })
						            (
						                <Input placeholder="请输入银行卡户名" />
						            )
						        }
						    </FormItem>
					    </Col>
				    </Row>
				    <Row gutter={24}>
				    	<Col span={12}>
						    <FormItem
						        label="银行"
						        {...formItemLayout}
						    >
						        {
						            getFieldDecorator('bankName', { 
						                rules: [{
						                    required: true, 
						                    message: '请填写银行支行' 
						                }] 
						            })
						            (
						                <Input placeholder="请填写银行支行" />
						            )
						        }
						    </FormItem>
					    </Col>
				    	<Col span={12}>
						    <FormItem
						        label="提现账号"
						        {...formItemLayout}
						    >
						        {
						            getFieldDecorator('cardNum', { 
						                rules: [{
						                	whitespace: true,
						                    type:'number',
						                    transform(value) {
						                      if(value){
						                        return Number(value.replace(/ /g,''));
						                      }
						                    },
						                	message: '请输入正确的账号' 
						                },{
						                    required: true, 
						                    message: '请输入提现账号' 
						                }] 
						            })
						            (
						                <Input placeholder="请输入提现账号" />
						            )
						        }
						    </FormItem>
						</Col>
				    </Row>
				    <Row gutter={24}>
				    	<Col span={12}>
						    <FormItem
						        label="提现原因"
						        {...formItemLayout}
						    >
						        {
						            getFieldDecorator('reason', { 
						                rules: [{
						                    required: true, 
						                    message: '请输入原因' 
						                }] 
						            })
						            (
						                <Input />
						            )
						        }
						    </FormItem>
				    	</Col>
				    </Row>
				    <Row gutter={24}>
				    	<Col span={12}>
						    <FormItem
						        label="对内备注"
						        {...formItemLayout}
						    >
						        {
						            getFieldDecorator('internalRemark', { 
						                rules: [{
						                    required: false, 
						                }] 
						            })
						            (
						                <TextArea 
						                	rows={2} 
						                />
						            )
						        }
						    </FormItem>
				    	</Col>
				    	<Col span={12}>
						    <FormItem
						        label="对外备注"
						        {...formItemLayout}
						    >
						        {
						            getFieldDecorator('foreignRemark', { 
						                rules: [{
						                    required: false, 
						                }] 
						            })
						            (
						                <TextArea 
						                	rows={2} 
						                />
						            )
						        }
						    </FormItem>
						</Col>
					</Row>
		      	</Form>
		</Modal>);
	}

	_ok(e) {
		let {
			studentId
		} = this.state;
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				let {
					trades,
					tradeAmount,
					foreignRemark,
					internalRemark,
					schoolAreaId,
					cardNum,
					cardName,
					bankName,
					reason
				} = values;
				values = {
					trades: [{
						tradeType: trades,
						tradeAmount: tradeAmount
					}],
					studentId: studentId,
					foreignRemark,
					internalRemark,
					schoolAreaId,
					reason,
					accountInfo:cardName+' '+ bankName + ' ' + cardNum.replace(/ /g,'')
				}
				console.log(values);
				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;


		fpostArray('/api/reception/student/studentWithdrawals', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onSaveOk();
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;