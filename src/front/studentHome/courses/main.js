import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	message,
	Row,
	Col
} from 'antd';

import {
	fpost
} from '../../../common/io.js';
import {
	Loading,
	NoData,
	getUrlParam,
	getUnicodeParam
} from '../../../common/g.js';

import {
	default as MyList
} from './list.js';

import './main.less';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			result: null,
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {
				studentId: getUrlParam('studentId') || '',
			}
		}
	}
	render() {
		let {
			result,
			loading,
			pagination,
			name,
			stuNo
		} = this.state;

		return (<MyList 
				data={result}
		        loading={loading}
		        onChange={this._getList.bind(this)}
		        pagination={pagination}
			/>);
	}
	componentDidMount() {
		this._getList();
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			searchParma
		} = this.state;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		searchParma.pageSize = pageSize;
		searchParma.currentPage = currentPage;

		console.log('pager', pager);

		fpost('/api/reception/student/course/pageRegistrationCourseByStudentId', searchParma)
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result || !res.result.records) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records;
				data.forEach((d) => {
					d.key = d.id
				});

				let total = Number(result.total);

				this.setState({
					result: data,
					pagination: {
						...pagination,
						total: total,
						currentPage: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				}, () => {
					console.log(this.state)
				});
			})
			.catch((err) => {
				console.log(err);

				this.setState({
					loading: false
				});
			});
	}
}