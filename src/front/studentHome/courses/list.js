import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  getUnicodeParam
} from '../../../common/g.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    const columns = [{
      title: '课程名称',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'courseName'
    }, {
      title: '报名时间',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'registrationTime'
    }, {
      title: '报名类型',
      dataIndex: 'type',
      className: 'f-align-center',
      width: '150px',
      render: (value, row, index) => {
        let arr = ['', '单报', '连报', '课程包']
        let {
          currentPeriod,
          totalPeriod
        } = row;
        return (<div>
          { arr[value] }
          { value == 2 ? <span className="f-ml2">({ currentPeriod }/ { totalPeriod })</span> : null }
        </div>);
      }
    }, {
      title: '班级名称',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'className'
    }, {
      title: '班主任',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'headmasterName'
    }, {
      title: '校区',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'schoolAreaName'
    }, {
      title: '开班',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'classStartTime'
    }, {
      title: '教室',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'classRoomName'
    }, {
      title: '学费',
      width: '50px',
      className: 'f-align-center',
      dataIndex: 'receivableAmount'
    }, {
      title: '班级状态',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'classStateStr'
    }, {
      title: '学费状态',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'isArrearStr'
    }, {
      title: '优惠状态',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'isOriginalStr'
    }, {
      title: '报班状态',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'stateStr'
    }, {
      title: '是否续报',
      width: '100px',
      className: 'f-align-center',
      dataIndex: 'isContinueStr'
    }, {
      title: '是否新生',
      width: '100px',
      className: 'f-align-center',
      dataIndex: 'isFreshManStr'
    }, {
      title: '操作',
      dataIndex: 'des',
      fixed: 'right',
      width: '100px',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          state, // 1:正常报名、2:转出、3:未开始、4:退课、5:结课、6:停课
          isArrear, //是否欠费 0:否 1:是
          id, //报课id
          studentId, //学员id
        } = row;
        return (<Dropdown 
              overlay={
                <Menu>
                  <Menu.Item key="3">
                    <a href={`/front/students/detail?id=${row.id}`}>查看</a>
                  </Menu.Item>
                  <Menu.Item key="3">
                    <a href={`/front/students/singRecord?id=${row.id}`}>上课记录</a>
                  </Menu.Item>
                  <Menu.Item key="1">
                    <a href={`/front/students/after?id=${row.id}&courseName=${row.courseName}&className=${row.className}`}>售后沟通</a>
                  </Menu.Item>
                </Menu>
              }>
              <a>
                操作<Icon type="down" />
              </a>
            </Dropdown>);
      }
    }];

    return (
        <Table
            columns={ columns }
            dataSource={ data }
            scroll={{ x: 2150}}
            bordered
            pagination={ pagination }
            loading={loading}
            onChange={onChange}
          />
    );
  }

}