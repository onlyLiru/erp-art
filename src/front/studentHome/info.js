import React,{Component} from 'react';
import {
	Row,
	Col,
	Button,
	message
} from 'antd';
import {
	numberFormate,
	getUrlParam
} from '../../common/g.js';
import {
	default as RechargeForm
} from './rechargeForm.js';
import {
	default as DrawMoney
} from './drawMoneyForm.js';

let studentId = getUrlParam('studentId');

export default class StudentInfo extends Component {
	constructor(props) {
		super(props);
		this.state={
			visibleRecharge: false,
			visibleDrawMoney: false,
		}
	}
	render() {
		let {
			data
		} = this.props;

		let {
			visibleRecharge,
			visibleDrawMoney
		} = this.state;

		return(<div className="f-mb5">
			<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-clear">
				<Row gutter={40}>
					<Col span={3} className="f-align-center">
						<img style={{width:'60px',height:'60px'}} src={data.headImgUrl} />
					</Col>
					<Col span={21}>
						<ul className="f-clear info-list info-bd-top"  style={{width:'100%'}}>
							<li style={{width:'33%'}}>
								<span className="f-pale f-mr2">学员姓名(学号):</span>
								{data.name}{ data.stuNo ? <i>({data.stuNo})</i> : '' }
							</li>
							<li style={{width:'33%'}}>
								<span className="f-pale f-mr2">家长手机:</span>
								{data.phone}
							</li>
							<li style={{width:'33%'}}>
								<span className="f-pale f-mr2">学员生日:</span>
								{data.birthday}
							</li>
							<li style={{width:'33%'}}>
								<span className="f-pale f-mr2">余额:</span>
								<i className="f-cny f-mr2">¥ {numberFormate(data.balanceAmount || '0.00')}</i>
							</li>
							<li style={{width:'33%'}}>
								<span className="f-pale f-mr2">市场来源:</span>
								{data.sourceStr}
							</li>
							<li style={{width:'33%'}}>
								<span className="f-pale f-mr2">课程顾问:</span>
								{data.salesName}
							</li>
							<li style={{width:'33%'}}>
								<Button 
									className="f-mr2"
									onClick = {
										this._visibleRecharge.bind(this)
									}
								>充值</Button>
								<Button 
									type="primary"
									onClick = {
										this._visibleDrawMoney.bind(this)
									}
								>提现</Button>
							</li>
						</ul>
					</Col>
				</Row>
			</header>

			{
				visibleRecharge ? 
					<RechargeForm
						ref={ form => this.rechargeForm = form }
						visible={ visibleRecharge }
						onSaveOk={this._rechargeOk.bind(this)}
						onCancel={this._rechargeCancel.bind(this)}
						studentId={studentId}
					/>
				: null
			}

			{
				visibleDrawMoney ? 
					<DrawMoney
						ref={ form => this.drawMoneyForm = form }
						visible={ visibleDrawMoney }
						onSaveOk={this._drawMoneyOk.bind(this)}
						onCancel={this._drawMoneyCancel.bind(this)}
						studentId={studentId}
						balanceAmount={data.balanceAmount}
					/>
				: null
			}
		</div>);
	}
	_visibleRecharge() { //显示充值
		this.setState({
			visibleRecharge: true
		});
	}
	_rechargeCancel() { //取消充值
		this.setState({
			visibleRecharge: false
		});
	}
	_rechargeOk(id) { //充值成功,返回充值流水id
		let {
			onRechargeOk
		} = this.props;
		this._rechargeCancel();
		onRechargeOk(id);
		message.success('恭喜您,充值成功!');
	}
	_visibleDrawMoney() { //显示提现
		this.setState({
			visibleDrawMoney: true
		});
	}
	_drawMoneyCancel() { //取消提现
		this.setState({
			visibleDrawMoney: false
		});
	}
	_drawMoneyOk() { //提现成功
		let {
			onDrawMoneyOk
		} = this.props;
		this._drawMoneyCancel();
		onDrawMoneyOk();
		message.success('申请成功，请等待审批！');
	}
}