import React,{Component} from 'react'
import {
	Form,
	Breadcrumb,
	Select,
	Input,
	Button,
	message,
	Row,
	Col,
	Tag
} from 'antd'
import {
	payTypesSub
} from '../../common/staticData.js';
import PayTypes from '../../common/payTypes.js';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import {
	fpost,
	host,
	fpostArray
} from '../../common/io.js';
import {
	getUnicodeParam
} from '../../common/g.js';
import SelectDiscounts from '../../common/selectDiscounts.js';

const FormItem = Form.Item;
const {
	TextArea
} = Input;
const Option = Select.Option;
let registrationCourseId = getUnicodeParam('id');//报课id
let classId = getUnicodeParam('classId');//转入班级id

class Main extends Component {
	constructor(props) {
		super(props)
		this.state={
			data: {},// 转出班级信息
			intoClassInfo:{},//转入班级信息
			cost:{}, //计算结果

			visibleSelectDiscounts: false, //是否显示选择优惠弹窗
			couponData:[],//优惠信息

			courseTimes:'',//转入课次
			offsetBalanceAmount:'', //余额抵用

			tradeList:[],//支付方式
			refundType:'-1'//退款方式
		}
	}
	render() {
		let self = this;
		const {
			getFieldDecorator
		} = this.props.form;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		let {
			data={},
			intoClassInfo={},
			cost={},
			visibleSelectDiscounts,
			couponData,
			refundType
		} = this.state;

		return (<Form>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>转班</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">转班结算</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
				<h3>学员信息</h3>
				<ul className="f-clear mylistUl"  style={{width:'100%'}}>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">学员姓名(学号):</span>
						{data.studentName}({data.stuNo})
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">家长手机:</span>
						{data.phone}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">学员生日:</span>
						{data.birthday}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">市场来源:</span>
						{data.salesFromStr}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">课程顾问:</span>
						{data.salesName}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">余额:</span>
						<i className="f-cny f-mr2">¥ {data.balanceAmount}</i>
					</li>
				</ul>
				<h3>转出班级信息</h3>
				<ul className="f-clear mylistUl"  style={{width:'100%'}}>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">班级名称:</span>
						{data.className}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">校区:</span>
						{data.schoolAreaName}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">原价:</span>
						{data.originalPrice}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">优惠:</span>
						{data.preferentialAmount}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">学费:</span>
						{data.receivableAmount}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">剩余学费:</span>
						{data.remainReceivableAmount}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">剩余课次:</span>
						{data.remainClassTimes}
					</li>
					{
						data.arrearAmount ?
							<li className="mylistLi" style={{width:'33%'}}>
								<span className="title f-pale f-mr2">欠费:</span>
								{data.arrearAmount}
							</li>
						: null
					}
				</ul>
				<h3>转入班级信息</h3>
				<ul className="f-clear mylistUl"  style={{width:'100%'}}>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">班级名称:</span>
						{intoClassInfo.name}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">校区:</span>
						{intoClassInfo.schoolAreaName}
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">课次单价:</span>
						{intoClassInfo.unitPrice} 元
					</li>
					<li className="mylistLi" style={{width:'33%'}}>
						<span className="title f-pale f-mr2">转入课次:</span>
						<FormItem>
						    {
						        getFieldDecorator('courseTimes', { 
						            rules: [{
						                required: true, 
						                message:'请输入转入课次'
						            }],
						            initialValue:cost && cost.courseTimes || '' 
						        })
						        (
						            <Input
						            	type="number" 
						            	size="large" 
						            	style={{width:'100px'}} 
						            	onChange={
						            		(e)=> {
						            			this.setState({
						            				courseTimes:e.target.value
						            			},this._compute.bind(this));
						            		}
						            	}
					            	/>
						        )
						    }
						</FormItem>
					</li>
					<li className="mylistLi" style={{width:'66%'}}>
						<span className="title f-pale f-mr2 ant-form-item-required">课程顾问:</span>
						<FormItem
							style={{
								width:'300px'
							}}
						>
							{
								getFieldDecorator('salesId', {
									rules: [{  
										required:true,
										message:'请选择'
									}],
									initialValue:null
								})(
									<SelectPerson 
										url='/api/hr/staff/listStaffByCondForDropDown'
										data={
											{ 
												type:1
											}
										} 
										onSelect={ 
											(v)=> {
												this.props.form.setFieldsValue({
													salesId: v
												});
											} 
										}
									/>
								)
							}
						</FormItem>
					</li>
					{
						cost.resultType && cost.resultType == '2' ?
							<li className="f-clear mylistLi" style={{display:'block',float:'none',width:'100%'}}>
								<span className="title f-pale f-mr2">优惠信息:</span>
								{
									(couponData && couponData.coupon) ? 
										<span>
											{
												couponData.discount ?
													<Tag 
														color="orange" 
														className="f-lh7 discounts-tag"
													> 折扣: { couponData.discount } </Tag>
												: null
											}
											{
												couponData.cash ?
													<Tag 
														color="orange" 
														className="f-lh7 discounts-tag"
													> 现金优惠: ¥{ couponData.cash } </Tag>
												: null
											}
											{ couponData.coupon.map((d,i)=> {
												return (<Tag 
														key={i}
														color="orange" 
														className="f-lh7 discounts-tag"
													>
														{d.name}：¥{d.amount}
													</Tag>)
											}) }
										</span>
									: null
								}
								<Button 
									className="f-mb3" 
									size="large" 
									type="primary"
									onClick={ ()=> {
										this.setState({
											visibleSelectDiscounts:true
										});
									} }
								>选择优惠</Button>
							</li>
						: null
					}
					{
						cost.resultType && cost.resultType == '2' && data.balanceAmount ?
							<li className="mylistLi" style={{width:'33%'}}>
								<span className="title f-pale f-mr2">余额抵用:</span>
								<FormItem className="f-mr2">
								    {
								        getFieldDecorator('offsetBalanceAmount', { 
								            rules: [{
								                required: false, 
								            }],
								            initialValue:'0'
								        })
								        (
								            <Input 
								            	type="number" 
								            	size="large" 
								            	style={{width:'100px'}} 
								            	onChange={
								            		(e)=> {
								            			this.setState({
								            				offsetBalanceAmount:e.target.value
								            			},this._compute.bind(this));
								            		}
								            	}
							            	/>
								        )
								    }
								</FormItem>
								元
								<span>（最多可抵用{data.balanceAmount}元）</span>
							</li>
						: null
					}
					{
						cost.resultType && cost.resultType == '2' ?
						<li className="mylistLi" style={{width:'33%'}}>
							<span className="title f-pale f-mr2">优惠金额:</span>
							{
								cost.totalPreferentialAmount || 0
							}
							元
						</li>
						: null
					}
					{
						cost.resultType && cost.resultType == '1' ?
							<li className="mylistLi" style={{width:'33%'}}>
								<span className="title f-pale f-mr2">转班应退:</span>
								{cost.totalAmount} 元
							</li>
						: null
					}
					{
						cost.resultType && cost.resultType == '2' && cost.transferArrearAmount ?
							<li className="mylistLi" style={{width:'33%'}}>
								<span className="title f-pale f-mr2">转班应补:</span>
								{cost.totalAmount} 元
							</li>
						: null
					}
					{
						cost.resultType && cost.resultType == '0' ?
							<li className="mylistLi" style={{width:'33%'}}>
								<span className="title f-pale f-mr2">转班应补:</span>
								0 元
							</li>
						: null
					}
					{
						cost.resultType && cost.resultType == '2' ?
							<li className="mylistLi" style={{width:'33%' ,marginBottom:'28px'}}>
								<span className="title f-pale f-mr2">转入班级欠费:</span>
								{cost.transferArrearAmount || 0} 元
							</li>
						: null
					}
					{
						cost.resultType && cost.resultType == '1' ?
							<li className="mylistLi" style={{width:'100%'}}>
								<span className="title f-pale f-mr2 ant-form-item-required">退款方式:</span> 
								<FormItem className="f-mr2">
								    {
								        getFieldDecorator('refundType', { 
								            rules: [{
								                required: true, 
								                message:'请选择退款方式'
								            }],
								            initialValue:'-1'
								        })
								        (
											<Select 
												size="large" 
												placeholder="请选择退款方式" 
												style={{width:'150px',marginRight:'-14px'}}
												onChange={
													(v)=> {
														this.setState({
															refundType:v
														});
													}
												}
											>
											    {
											        payTypesSub.map((d,i)=> {
											            return <Option key={i} value={d.value}>{d.label}</Option>
											        })
											    }
											</Select>
								        )
								    }
								</FormItem>
								<FormItem className="f-mr2">
								    {
								        getFieldDecorator('refundAmount', { 
								            rules: [{
								                required: true, 
								                message:'请输入退款金额'
								            }],
								            initialValue:''
								        })
								        (
											<Input 
												style={{
													width:"100px",
													borderTopLeftRadius:'0',
													borderBottomLeftRadius:'0'
												}} 
												size="large" 
												type="number" 
											/>
								        )
								    }
								</FormItem>
							</li>
						: null
					}
				</ul>
				{
					refundType == '7' ?
					<div>
					    <ul className="f-clear mylistUl"  style={{width:'100%'}}>
					    	<li className="mylistLi"> 
					    		<span className="title f-pale f-mr2 ant-form-item-required">户名:</span>
					    		<FormItem>
					    		    {
					    		        getFieldDecorator('cardName', { 
					    		            rules: [{
					    		                required: true, 
					    		                message: '请输入银行卡户名' 
					    		            }] 
					    		        })
					    		        (
					    		            <Input style={{width:'400px'}} placeholder="请输入银行卡户名" />
					    		        )
					    		    }
					    		</FormItem>
					    	</li>
					    	<li className="mylistLi">
					    		<span className="title f-pale f-mr2 ant-form-item-required">银行:</span>
					    		<FormItem>
				    		        {
				    		            getFieldDecorator('bankName', { 
				    		                rules: [{
				    		                    required: true, 
				    		                    message: '请填写银行支行' 
				    		                }] 
				    		            })
				    		            (
				    		                <Input style={{width:'400px'}} placeholder="请填写银行支行" />
				    		            )
				    		        }
				    		    </FormItem>
					    	</li>
				    	</ul>
		    		    <ul className="f-clear mylistUl"  style={{width:'100%'}}>
		    		    	<li className="mylistLi"> 
		    		    		<span className="title f-pale f-mr2 ant-form-item-required">银行账号:</span>
		    		    		<FormItem>
		    		    		    {
				    		            getFieldDecorator('cardNum', { 
				    		                rules: [{
				    		                	whitespace: true,
				    		                    type:'number',
				    		                    transform(value) {
				    		                      if(value){
				    		                        return Number(value.replace(/ /g,''));
				    		                      }
				    		                    },
				    		                	message: '请输入正确的账号' 
				    		                },{
				    		                    required: true, 
				    		                    message: '请输入账号' 
				    		                }] 
				    		            })
				    		            (
				    		                <Input style={{width:'400px'}} placeholder="请输入提现账号" />
				    		            )
				    		        }
		    		    		</FormItem>
		    		    	</li>
		    		    	<li className="mylistLi">
		    		    		<span className="title f-pale f-mr2 ant-form-item-required">原因:</span>
		    		    		<FormItem>
		    	    		        {
		    				            getFieldDecorator('reason', { 
		    				                rules: [{
		    				                    required: true, 
		    				                    message: '请输入原因' 
		    				                }] 
		    				            })
		    				            (
		    				                <Input style={{width:'400px'}} />
		    				            )
		    				        }
		    	    		    </FormItem>
		    		    	</li>
					    </ul>
				    </div>
					: null
				}
				{
					cost.resultType && cost.resultType == '2' ?
						<div className="f-clear">
							<div className="f-pale f-mr2 f-left f-align-right ant-form-item-required" style={{minWidth:'100px'}}>支付方式:</div> 
							<div className="f-left" style={{paddingLeft:'108px',position:'relative',top:'-20px'}}>
								<PayTypes 
			    					onInput = { 
			    						(v)=>{
			    							let {
												tradeList
											} = this.state;
											tradeList = (
												v.trades &&
												v.trades[0] &&
												v.trades[0].tradeAmount.length
											) ? v.trades : [];
											this.setState({
												tradeList
											}, this._compute.bind(this));
			    						} 
			    					}
			    				/>
		    				</div>
						</div>
					: null
				}
				<ul className="f-clear mylistUl"  style={{width:'100%'}}>
					<li className="mylistLi"> 
						<span className="title f-pale f-mr2">对内备注:</span>
						<FormItem>
						    {
						        getFieldDecorator('internalRemark', { 
						            rules: [{
						                required: false
						            }],
						            initialValue:''
						        })
						        (
						            <TextArea 
						            	style={{width:'400px'}}
						            	rows={4} 
						            />
						        )
						    }
						</FormItem>
					</li>
					<li className="mylistLi">
						<span className="title f-pale f-mr2">对外备注:</span>
						<FormItem>
						    {
						        getFieldDecorator('foreignRemark', { 
						            rules: [{
						                required: false
						            }],
						            initialValue:''
						        })
						        (
						            <TextArea 
						            	style={{width:'400px'}}
						            	rows={4} 
						            />
						        )
						    }
						</FormItem>
					</li>
				</ul>
				<div className="f-align-center">
					<Button 
						size="large" 
						className="f-mr4" 
						onClick={
							()=> {
								window.history.back();
							}
						}
					>取消</Button>
					<Button 
						type="primary"
						size="large" 
						onClick = {this._submit.bind(this)}
					>转班</Button>
				</div>
			</header>

			{/*选择优惠*/}
			{
				visibleSelectDiscounts && this.state.intoClassInfo.courseId ? 
					<SelectDiscounts
						visible={ visibleSelectDiscounts }
						handleOk= { this._selectDiscountsOk.bind(this) }
						handleCancel= { this._selectDiscountsCancel.bind(this) }
						courseId={ this.state.intoClassInfo.courseId }
						defaultData={ this.state.couponData }
						isNoZhekou={ true }
					/>
				: null
			}

		</Form>)
	}
	componentDidMount() {
		this._getInfo();
	}
	_getInfo() {
		fpost('/api/reception/student/course/findRegCourseAndStuInfoById', {
				id:registrationCourseId
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					data:res.result || {}
				},()=> {
					this._getIntoClassInfo();
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_getIntoClassInfo() {
		fpost('/api/educational/findClass', {
				classId:classId
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					intoClassInfo:res.result || {}
				},()=> {
					this._compute();
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_compute() {
		let {
			data,
			intoClassInfo,
			couponData,
			courseTimes,
			offsetBalanceAmount,
			tradeList
		} = this.state;
		let {
			remainReceivableAmount,
			arrearAmount
		} = data;
		let {
			unitPrice
		} = intoClassInfo;

		fpostArray('/rolling/calculateTransferCost', {
				remainReceivableAmount,
				arrearAmount,
				unitPrice,
				preferentialList:couponData && couponData.registrationCourseReduceList,
				courseTimes:parseInt(courseTimes),
				offsetBalanceAmount:Number(offsetBalanceAmount),
				tradeList
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				// res.result.resultType=0;
				this.setState({
					cost:res.result || {}
				});
			})
			.catch((err) => {
				console.log(err);
			});

	}
	_selectDiscountsOk(v) { //确定选择优惠
		this.setState({
			couponData: v,
			visibleSelectDiscounts: false
		},this._compute);
	}
	_selectDiscountsCancel() { //取消选择优惠
		this.setState({
			visibleSelectDiscounts: false
		});
	}
	_submit() {
		let {
			data,
			intoClassInfo,
			couponData,
			courseTimes,
			offsetBalanceAmount,
			tradeList
		} = this.state;
		let {
			studentId,
			remainReceivableAmount,
			arrearAmount
		} = data;
		let {
			schoolAreaId,
			unitPrice,
			courseId
		} = intoClassInfo;
		let param={
			studentId,
			registrationCourseId,
			schoolAreaId,
			classId,
			courseId,
			remainReceivableAmount,
			arrearAmount,
			unitPrice,
			preferentialList:couponData && couponData.registrationCourseReduceList,
			tradeList
		};
		this.props.form.validateFields((err, values) => {
			if (!err) {
				// console.log(values);
				// console.log(this.state);
				let {
					cardNum,
					cardName,
					bankName
				} = values;

				param={
					...param,
					...values,
				}
				if(cardNum) {
					param.accountInfo = cardName+' '+ bankName + ' ' + cardNum.replace(/ /g,'');
				}
				// console.log(param);
				this._confirmSubmit(param);
			}
		});
	}
	_confirmSubmit(param) {
		let {
			resultType //1:退款,2:补缴
		} = this.state;
		fpostArray('/rolling/submitTransfer', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				let {
					id,
					schoolAreaId
				} = res.result;
				// let url =resultType=='1' ? `/front/check/list` : `/print?registrationId=${id}&schoolAreaId=${schoolAreaId}&type=5`;
				// window.location.href = url;
				window.location.href = "/front/registrationCourse";
			})
			.catch((err) => {
				console.log(err);
			});
	}
}

const MyForm = Form.create()(Main);

export default MyForm;