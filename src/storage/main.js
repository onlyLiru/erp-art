import React, {
	Component
} from 'react';
import {
	Route,
	Redirect
} from 'react-router-dom';
import {
	default as Layout
} from '../common/layout.js';

//仓库设置
import {
	default as StockSet
} from './stockSet.js';
//物品设置
import {
	default as GoodsSet
} from './goodsSet.js';
//库存查询
import {
	default as SearchStock
} from './searchStock.js';
//进出库--进货
import {
	default as InOutStock
} from './inOutStock/purchases.js';
//进出库--退货
import {
	default as SalesReturn
} from './inOutStock/salesReturn.js';
//进出库--调拨
import {
	default as Allot
} from './inOutStock/allot.js';
//进出库--报损
import {
	default as Breakage
} from './inOutStock/breakage.js';
//进出库--员工物品领用
import {
	default as EmployeeGoods
} from './inOutStock/employeeGoods.js';
//进出库--报课物品领用
import {
	default as ClassGoods
} from './inOutStock/classGoods.js';
//进出库--员工物品退领
import {
	default as GoodsBack
} from './inOutStock/goodsBack.js';
//进出库--库存调整
import {
	default as StockAdjust
} from './inOutStock/stockAdjust.js';
//库出存查询
import {
	default as InOutStockQuery
} from './inOutStockQuery.js';
//库存变动表
import {
	default as StockChange
} from './stockChange.js';

import {
	default as GetGoods
} from './getGoods/main.js';

import './main.less';

export default ({
	match
}) => {
	window.store.session('tMenu', {
		selectedKeys: 'ckgl'
	})
	return (<div>
		<Layout main={ Main } />
	</div>);
}

class Main extends Component {
	render() {
		return (
			<div>
				<Route exact path={`/storage`} component={StockSet} />
				<Route path={`/storage/stockSet`} component={StockSet} />		
				<Route path={`/storage/goodsSet`} component={GoodsSet} />		
				<Route path={`/storage/stockQuery`} component={SearchStock} />		
				<Route exact path={`/storage/inOutStock`} render={ () => (<Redirect to="/storage/inOutStock/purchases"/>) } />
				<Route path={`/storage/inOutStock/purchases`} component={InOutStock} />
				<Route path={`/storage/inOutStock/salesReturn`} component={SalesReturn} />
				<Route path={`/storage/inOutStock/allot`} component={Allot} />
				<Route path={`/storage/inOutStock/breakage`} component={Breakage} />
				<Route path={`/storage/inOutStock/employeeGoods`} component={EmployeeGoods} />
				<Route path={`/storage/inOutStock/classGoods`} component={ClassGoods} />
				<Route path={`/storage/inOutStock/goodsBack`} component={GoodsBack} />
				<Route path={`/storage/inOutStock/stockAdjust`} component={StockAdjust} />
				<Route exact path={`/storage/inOutStockQuery`} component={InOutStockQuery} />
				<Route exact path={`/storage/stockChange`} component={StockChange} />
				<Route path={`/storage/getGoods`} component={GetGoods} />
			</div>
		);
	}
}