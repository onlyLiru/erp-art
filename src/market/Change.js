import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Tabs,
	Row,
	Col,
	Button,
	Input,
	Select,
	Form,
	Table,
	Checkbox,
	Modal,
	message
} from 'antd';
import {
	fpost
} from '../common/io.js';
import {
	Loading,
	Pagination,
	undefinedToEmpty
} from '../common/g.js';

const TabPane = Tabs.TabPane;
const Option = Select.Option;
const FormItem = Form.Item;

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visibleType: false,
			chooseDataList: [],
			seller: '',
			clientDataList: null,
			initTable: true,
			total: 1,
			isRequest: false,
			unitData: {},
			changeStatus: '',
			personList: [],
			selectedCustomer: [],
			searchPara: {
				currentPage: 1,
				pageSize: 10
			}
		}
		this.getStatus.bind(this);
	}
	showModal() {
		if (this.state.chooseDataList.length == 0) {
			message.warning('请先勾选需要更换课程顾问的客户姓名!');
			return;
		}
		this.setState({ //确定对话框的展示
			visibleType: true,
			initTable: true,
			changeStatus: 'more'
		})
	}
	showOnecModal(v, o) {
		this.setState({ //确定对话框的展示
			visibleType: true,
			initTable: true,
			unitData: o,
			changeStatus: 'unit'
		})
	}
	updatePersonList(obj) {
		this.setState({
			personList: obj
		})
	}
	cancelModal() {
		this.setState({ //确定对话框的展示
			visibleType: false,
			initTable: false
		})
	}
	resetPersonList() {
		let personList = this.state.personList;
		this.setState({
			personList: personList.map((e, i) => {
				e.isChecked = false;
				return e;
			})
		})
	}
	saveData() {
		const {
			chooseDataList,
			seller,
			unitData,
			changeStatus
		} = this.state, arrData = [], {
			currentPage,
			pageSize
		} = this.state.searchPara;
		if (changeStatus == 'unit') {
			arrData.push(unitData.id)
		} else if (changeStatus == 'more') {
			chooseDataList.map((e, i) => {
				arrData.push(e.id)
			})
		}

		this.setState({
			isRequest: true
		})

		if (seller) {
			fpost('/api/marketing/updateSalesBatch', {
				ids: arrData,
				salesId: seller
			}).then(res => res.json()).then(res => {
				if (res.success == true) {
					message.success('更换课程顾问成功!');
					this.setState({
						visibleType: false,
						initTable: false,
						selectedCustomer: [],
						chooseDataList: [],
						seller: '',
						searchPara: {
							currentPage: currentPage,
							pageSize
						}
					}, function() {
						this.getDataList(this.state.searchPara);
						this.resetPersonList();
					})
				} else {
					message.error(res.message);
				}
				this.setState({
					isRequest: false
				})
			})
		} else {
			message.warning('请先选择需要更换的课程顾问!');
		}
	}
	getStatus(e, o, index) {
		let data = this.state.chooseDataList,
			{
				currentPage
			} = this.state.searchPara,
			selectedCustomer = this.state.selectedCustomer;

		if (e.target.checked) {
			data.push(o);
			this.setState({
				chooseDataList: data
			});
			if (currentPage == 1) {
				selectedCustomer[index] = true;
			} else {
				selectedCustomer[(currentPage - 1) * 10 + index] = true;
			}
		} else {
			this.setState({
				chooseDataList: data.filter(obj => {
					return obj.id != o.id
				})
			});
			if (currentPage == 1) {
				selectedCustomer[index] = false;
			} else {
				selectedCustomer[(currentPage - 1) * 10 + index] = false;
			}
		}
	}
	updateSeller(v) {
		if (v) {
			this.setState({
				seller: v
			})
		} else {
			this.setState({
				seller: ''
			})
		}
	}
	setPara(obj) {
		this.setState({
			searchPara: obj
		})
	}
	getDataList(para) {
		fpost('/api/marketing/pagePotentialStudent', para).then(res => res.json()).then(res => {
			if (res.success == true) {
				let arr = [];
				res.result.records.map((e, i) => {
					e.key = i;
					arr.push(e);
				});

				this.setState({
					clientDataList: arr,
					total: res.result.total
				})
			}
		});
	}
	allSelect() {
		const {
			searchPara,
			selectedCustomer,
			chooseDataList,
			clientDataList
		} = this.state;
		for (let i = 0; i < searchPara.pageSize; i++) {
			if (searchPara.currentPage == 1) {
				selectedCustomer[i] = true;
			} else {
				selectedCustomer[(searchPara.currentPage - 1) * searchPara.pageSize + i] = true;
			}
		}
		this.setState({
			selectedCustomer,
			chooseDataList: clientDataList
		})
	}
	cancelAllselect() {
		const {
			searchPara,
			selectedCustomer
		} = this.state;
		for (let i = 0; i < searchPara.pageSize; i++) {
			if (searchPara.currentPage == 1) {
				selectedCustomer[i] = false;
			} else {
				selectedCustomer[(searchPara.currentPage - 1) * searchPara.pageSize + i] = false;
			}
		}
		this.setState({
			selectedCustomer,
			chooseDataList: []
		})
	}
	componentDidMount() {
		this.getDataList();
	}
	render() {
		const selectedCustomer = this.state.selectedCustomer,
			{
				currentPage
			} = this.state.searchPara;

		const columns = [{
			title: '客户姓名',
			dataIndex: 'name',
			key: 'name',
			className: 'f-align-left',
			width: '110px',
			render: (text, record, index) => (<Checkbox className='f-change-check' checked={ currentPage==1? selectedCustomer[index]? true : false : selectedCustomer[(currentPage-1)*10+index]? true: false } onChange={(e)=>{ this.getStatus(e, record, index) }}>{text}</Checkbox>),
			fixed: 'left'
		}, {
			title: '头像',
			key: 'headImgUrl',
			width: '100px',
			className: 'f-align-center f-line1',
			render: (text, record) => (
				record.headImgUrl ? <img src={record.headImgUrl} width={50} height={50} className='f-round'/> : ''
			)
		}, {
			title: '性别',
			dataIndex: 'genderStr',
			key: 'genderStr',
			className: 'f-align-center f-line1',
		}, {
			title: '年龄',
			dataIndex: 'age',
			key: 'age',
			className: 'f-align-center f-line1',
		}, {
			title: '手机号码',
			dataIndex: 'phone',
			key: 'phone',
			className: 'f-align-center f-line1',
		}, {
			title: '是否有效',
			dataIndex: 'isValidStr',
			key: 'isValidStr',
			className: 'f-align-center f-line1',
		}, {
			title: '邀约次数',
			dataIndex: 'connectCount',
			key: 'connectCount',
			className: 'f-align-center f-line1',
		}, {
			title: '诺访率',
			dataIndex: 'confirmRate',
			key: 'confirmRate',
			className: 'f-align-center f-line1',
		}, {
			title: '到访率',
			dataIndex: 'visitRate',
			key: 'visitRate',
			className: 'f-align-center f-line1',
		}, {
			title: '市场来源',
			dataIndex: 'sourceStr',
			key: 'sourceStr',
			className: 'f-align-center f-line1',
		}, {
			title: '课程顾问(工号)',
			dataIndex: 'salesName',
			key: 'salesName',
			className: 'f-align-center f-line1',
		}, {
			title: '市场推广',
			dataIndex: 'marketPromotor',
			key: 'marketPromotor',
			className: 'f-align-center f-line1',
		}, {
			title: '跟进状态',
			key: 'statusStr',
			className: 'f-align-center f-line1',
			render: (text, record) => (
				<span className={ record.statusStr=='待跟进'? 'f-orange' : record.statusStr=='已跟进'? 'f-blue' : '' }><i className='iconfont icon-dian'></i>{record.statusStr}</span>
			)
		}, {
			title: '操作人',
			dataIndex: 'operatorName',
			key: 'operatorName',
			className: 'f-align-center f-line1'
		}, {
			title: '操作时间',
			dataIndex: 'modifyTime',
			key: 'modifyTime',
			className: 'f-align-center f-line1'
		}, {
			title: '操作',
			key: 'action',
			className: 'f-align-center',
			width: '15px',
			render: (text, record) => (
				<Button className='f-radius4' type='primary' onClick={ ()=>{ this.showOnecModal(record.id, record) } }><i className='iconfont icon-icon-2-30 f-vertical-middle f-mr1'></i>更换课程顾问</Button>
			),
			fixed: 'right'
		}];

		return (<div className='classListPage wid100'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>市场运营</Breadcrumb.Item>
					    <Breadcrumb.Item>客户管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>更换课程顾问</span></Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
						<Tabs defaultActiveKey="1" style={{textAlign:'center'}}>
							<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
					    		<ExactSearchForm pageSize={this.state.searchPara.pageSize} setPara={this.setPara.bind(this)} updateMainData={ this.getDataList.bind(this) }/>	
					    	</TabPane>
						    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
						    	<AdvancedSearchForm pageSize={this.state.searchPara.pageSize} setPara={this.setPara.bind(this)} updateMainData={ this.getDataList.bind(this) }/>	
						    </TabPane>
						 </Tabs>
					</div>
					<div className='f-bg-white f-radius1 f-box-shadow1 f-pd4 f-mt5'>
						<div className='f-flex' style={{justifyContent: 'space-between'}}>
							<h3 className="f-title-blue f-inline-block" >客户列表</h3>
							<Button onClick={this.showModal.bind(this)} className='f-mr5' type="primary" style={{borderRadius: '4px', width:'110px', height:'40px'}}>
		        				<i className='iconfont icon-icon-2-30 f-vertical-middle f-mr1'></i>
		        				批量更换
		        			</Button>
						</div>
						{
							this.state.clientDataList != null?
							<div>
								<div className='f-mb5'>
									<Button className='f-mr2' onClick={this.allSelect.bind(this)}>全选</Button><Button onClick={this.cancelAllselect.bind(this)}>取消全选</Button>
								</div>
								<Table columns={columns} pagination={ Pagination(this.state.total, this.state.searchPara, this.getDataList.bind(this), ()=>{
									this.setState({
										selectedCustomer: [],
										chooseDataList: []
									})
								}) } dataSource={this.state.clientDataList} bordered className='f-align-center' scroll={{ x: 1570 }}/>
							</div>
							: <Loading />
						}
					</div>
					<Modal
			          title='更换课程顾问'
			          visible={this.state.visibleType}
			          onOk={this.saveData.bind(this)}
			          onCancel={this.cancelModal.bind(this)}
			          confirmLoading={ this.state.isRequest }
			          width={880}
			          footer={[
			            <Button key="back" size="large" onClick={this.cancelModal.bind(this)}>取消</Button>,
			            <Button key="submit" type="primary" size="large" onClick={this.saveData.bind(this)}>
			              确定
			            </Button>,
			          ]}>
						<ChangeSellerForm personList={this.state.personList} updatePersonList={this.updatePersonList.bind(this)} initTable={ this.state.initTable } seller={ this.state.seller } updateSeller={ this.updateSeller.bind(this) }/>
					</Modal>				
				</div>)
	}
}

class ExactSearch extends Component {
	exactCondition() {
		let obj = this.props.form.getFieldsValue();
		obj.currentPage = 1;
		obj.pageSize = this.props.pageSize;

		obj = undefinedToEmpty(obj);
		this.props.updateMainData(obj);
		this.props.setPara(obj);
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		return (<Form style={{ textAlign:'left' }}>
					<FormItem style={{ width:'25%', marginLeft:'30px' }} className='f-inline-block'>
						{getFieldDecorator('name', {
				            rules: [{
				              required: false,
				              message: '请输入客户姓名',
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入客户姓名" />
				          )}
					</FormItem>
					<FormItem style={{ width:'25%', marginLeft:'30px' }} className='f-inline-block'>
						{getFieldDecorator('phone', {
				            rules: [{
				              pattern: /(^1[3|4|5|8][0-9]\d{4,8}$)|(^(\d{2,4}-?)?\d{7,8}$)/,
				              message: '请输入正确的号码'
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入手机号码" />
				          )}
					</FormItem>
					<FormItem className='f-inline-block f-ml3'>
						<Button type="primary" icon="search" style={{height:'40px'}} onClick={ this.exactCondition.bind(this) }>搜索</Button>
					</FormItem>
				</Form>)
	}
}
class AdvancedSearch extends Component {
	resetForm() {
		let obj = {
			currentPage: 1,
			pageSize: this.props.pageSize,
			isValid: '',
			status: ''
		};
		this.props.form.resetFields();
		this.props.updateMainData(obj);
		this.props.setPara(obj);
	}
	AdvancedCondition() {
		let obj = this.props.form.getFieldsValue();
		obj.currentPage = 1;
		obj.pageSize = this.props.pageSize;
		obj.isValid = obj.isValid ? obj.isValid : '';
		obj.status = obj.status ? obj.status : '';

		obj = undefinedToEmpty(obj);
		this.props.updateMainData(obj);
		this.props.setPara(obj);
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		return (<Form style={{ textAlign:'left' }}>
					<FormItem className='f-inline-block' style={{ marginLeft:'30px' }} label='是否有效' wrapperCol={{ style: { width:'30%', display:'inline-block', verticalAlign:'middle' } }}>
						{getFieldDecorator('isValid', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: undefined
				          })(
	                    	<Select
	            			    className='f-vertical-middle f-ml3'
	            			    style={{ width: 200 }}
	            			    placeholder="请选择"
	            			    optionFilterProp="children"
	            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
	            			  >
	            			    <Option value="1">是</Option>
	            			    <Option value="0">否</Option>
	            		  </Select>
				          )}
					</FormItem>
					<FormItem className='f-inline-block' style={{ marginLeft:'30px' }} label='跟进状态' wrapperCol={{ style: { width:'30%', display:'inline-block', verticalAlign:'middle' } }}>
						{getFieldDecorator('status', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: undefined
				          })(
	                    	<Select
	            			    className='f-vertical-middle f-ml3'
	            			    style={{ width: 200 }}
	            			    placeholder="请选择"
	            			    optionFilterProp="children"
	            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
	            			  >
	            			    <Option value="0">待跟进</Option>
	            			    <Option value="1">已跟进</Option>
	            		  </Select>
				          )}
					</FormItem>
					<FormItem className='f-inline-block' style={{ marginLeft:'30px' }}>
						<Button className='f-mr5' type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={ this.AdvancedCondition.bind(this) }>
	        				<i className='iconfont icon-hricon33 f-mr2'></i>
	        				搜索
	        			</Button>
	        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.resetForm.bind(this)}>
	        				<i className='iconfont icon-reset f-mr2'></i>
	        				重置
	        			</Button>
					</FormItem>
				</Form>)
	}
}
class ChangeForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			schoolList: [],
			departmentList: [],
			total: 1,
			searchPara: {
				currentPage: 1,
				pageSize: 5
			}
		}
	}
	resetForm() {
		this.props.form.resetFields();
		this.getSellerList({
			currentPage: 1,
			pageSize: 5
		});
		this.setState({
			searchPara: {
				currentPage: 1,
				pageSize: 5
			}
		})
	}
	searchCondition() {
		let obj = this.props.form.getFieldsValue();
		obj.schoolAreaId = obj.schoolAreaId ? obj.schoolAreaId : '';
		obj.department = obj.department ? obj.department : '';
		obj.currentPage = 1;
		obj.pageSize = 5;
		obj = undefinedToEmpty(obj);

		this.getSellerList(obj);
		this.setState({
			searchPara: obj
		})
	}
	searchPage(page, pageSize) {
		let searchPara = this.state.searchPara;
		searchPara.currentPage = page;
		this.setState({
			currentPage: page,
			loading: true
		})
		this.getSellerList(searchPara);
	}
	addSeller(record, index, e) {
		let personList = this.props.personList;
		if (e.target.checked) {
			personList.forEach((e, i) => {
				e.isChecked = false;
			});
			this.props.updateSeller(record.id);
		}
		personList[index].isChecked = e.target.checked;
		this.props.updatePersonList(personList);
	}
	getSellerList(para) {
		//获取课程顾问列表
		fpost('/api/hr/staff/pageStaffList', para).then(res => res.json()).then(res => {
			if (res.success == true) {
				this.setState({
					total: res.result.total,
				});
				this.props.updatePersonList(res.result.records.map((e, i) => {
					e.key = i;
					//e.name = e.name.substr(0, 30);
					return e;
				}))
			} else {
				message.error(res.message);
			}
		})
	}
	componentDidMount() {
		this.getSellerList(this.state.searchPara);
		//获取全国校区列表
		fpost('/api/system/schoolarea/listSchoolArea').then(res => res.json()).then(res => {
			if (res.success == true) {
				this.setState({
					schoolList: res.result
				})
			} else {
				message.error(res.message);
			}
		})

		//获取部门列表
		fpost('/api/hr/staff/listDepartment').then(res => res.json()).then(res => {
			if (res.success == true) {
				this.setState({
					departmentList: res.result
				})
			} else {
				message.error(res.message);
			}
		});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const FormLayoutStyle = {
			labelCol: {

			},
			wrapperCol: {
				style: {
					width: '200px',
					display: 'inline-block',
					verticalAlign: 'middle'
				}
			}
		}

		const columns = [{
			title: '姓名',
			key: 'name',
			dataIndex: 'name',
			className: 'f-align-left',
			render: (text, record, index) => (<p style={{width: text.length>30? '310px':''}}><input className='f-vertical-middle f-mr2'  name='changeRadio' checked={ record.isChecked? true : false } type='radio' onChange={ (e)=>{ this.addSeller(record, index, e) } }/><span className='f-vertical-middle f-inline-block f-line1' style={{width: text.length>30? '90%':''}}>{text}</span></p>)
		}, {
			title: '工号',
			key: 'staffNo',
			dataIndex: 'staffNo',
			className: 'f-align-center',
		}, {
			title: '校区',
			key: 'schoolAreas',
			dataIndex: 'schoolAreas',
			className: 'f-align-center',
			render: (text, record) => (
				<span className='f-line1 f-inline-block' style={{width: record.schoolAreas.length>15? '150px' : ''}}>{record.isAllArea==0? record.schoolAreas : '全部校区'}</span>
			)
		}, {
			title: '部门',
			key: 'department',
			dataIndex: 'department',
			className: 'f-align-center',
		}, {
			title: '职位',
			key: 'position',
			dataIndex: 'position',
			className: 'f-align-center',
		}];

		return (<Form>
					<Row>
						<Col span={ 24 }>
							<FormItem {...FormLayoutStyle} label='课程顾问' className='f-inline-block f-mr5'>
								{getFieldDecorator('search', {
						            rules: [{
						              required: false,
						              message: '请输入课程顾问姓名',
						            }],
						            initialValue: ''
						          })(
						            <Input placeholder="请输入课程顾问姓名/工号" />
						          )}
							</FormItem>
							<FormItem label='校区' {...FormLayoutStyle} className='f-inline-block f-mr5'> 
								{getFieldDecorator('schoolAreaId', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: undefined
						          })(
			                    	<Select
			            			    className='f-vertical-middle'
			            			    placeholder="请选择"
			            			    optionFilterProp="children"
			            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
			            			  >
			            			    {
			            			    	this.state.schoolList.map((e, i)=>{
			            			    		return <Option value={e.id} key={i}>{e.name}</Option>
			            			    	})
			            			    }
			            		  </Select>
						          )}
							</FormItem>
							<FormItem label='部门' {...FormLayoutStyle} className='f-inline-block'> 
								{getFieldDecorator('department', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: undefined
						          })(
			                    	<Select
			            			    className='f-vertical-middle'
			            			    placeholder="请选择"
			            			    optionFilterProp="children"
			            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
			            			  >
			            			    {
			            			    	this.state.departmentList.map((e, i)=>{
			            			    		return <Option value={e} key={i}>{e}</Option>
			            			    	})
			            			    }
			            		  </Select>
						          )}
							</FormItem>
							<FormItem className='f-align-center'>
								<Button className='f-mr5 f-inline-block' onClick={ this.searchCondition.bind(this) } type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}}>
			        				<i className='iconfont icon-hricon33 f-mr2'></i>
			        				搜索
			        			</Button>
			        			<Button className='f-inline-block' onClick={ this.resetForm.bind(this) } style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.resetForm.bind(this)}>
			        				<i className='iconfont icon-reset f-mr2'></i>
			        				重置
			        			</Button>
							</FormItem>
						</Col>
					</Row>
					{
						this.props.initTable? <Table columns={ columns } dataSource={ this.props.personList } pagination={{ total: parseInt(this.state.total), pageSize: parseInt(this.state.searchPara.pageSize), onChange: this.searchPage.bind(this) }}/> : ''
					}
				</Form>)
	}
}

const ExactSearchForm = Form.create()(ExactSearch);
const AdvancedSearchForm = Form.create()(AdvancedSearch);
const ChangeSellerForm = Form.create()(ChangeForm);