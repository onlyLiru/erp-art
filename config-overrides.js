const {
	injectBabelPlugin
} = require("react-app-rewired");
const rewireLess = require("react-app-rewire-less");
module.exports = function override(config, env) {
	// do stuff with the webpack config...
	config = injectBabelPlugin(["import", {
		libraryName: "antd",
		style: true
	}], config);
	config = rewireLess.withLoaderOptions({
		modifyVars: {
			"@primary-color": "#4aa236",
			"@font-size-base": "14px",
			"@layout-header-height": "87px",
			"@input-bg": "#f3f3f8",
			"@table-header-bg": "#eaf5d0",
			"@btn-height-lg": "36px",
			"@input-height-lg": "36px"
		},
	})(config, env);
	return config;
};